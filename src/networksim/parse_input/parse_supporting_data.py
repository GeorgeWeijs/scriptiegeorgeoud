"""
    Read supporting data and export this to seperate json data files

    Network included

    datapipeline as written up in setttings.py:
        FROM: INPUT_DATA, folder 0_input in datafolder
        TO: RAW_DATA, folder 1_raw


"""
from src.settings import INPUT_DATA, RAW_DATA
from src.utils import utilities
import os
import pandas as pd


def parse_supporting_data(supporting_data_file, input_folder=INPUT_DATA, output_folder=RAW_DATA, print_results=False):
    """
    Parse excel file and output to raw data
    :return:
    """

    def _save(data, output_file):
        # saves the data dumped in the parsed output folder
        utilities.save_json(data, output_file, output_folder)

    def _return_dict(sheet, primary_col=0, save=True, print_result=False):
        # parses the excel sheet and return a dictionary of dictionary, primary key is first column
        print(' .. working on %s (dictionary)' % sheet)
        dataframe = pd.read_excel(os.path.join(input_folder, supporting_data_file), sheet_name=sheet, skiprows=5)
        dataframe = dataframe.where((pd.notnull(dataframe)), None)
        output_dic = {}
        for number, data in dataframe.iterrows():
            if data[primary_col] is not None:  # not None
                data_to_add = data[primary_col]
                if int(data_to_add) == data_to_add:
                    data_to_add = int(data_to_add)
                output_dic[data_to_add] = data.to_dict()

        if save:
            _save(output_dic, sheet)

        if print_result:
            utilities.print_json(output_dic)

        return output_dic

    def _return_lookup(sheet, primary_col=0, lookup_col=True, save=True):
        # parses the excel sheet and returns a dictionary with the second row as a lookup
        # e.g. which types are suited for each servicegroup. The first column is here not unique
        # lookup_col: boolean, either return a single column (in col 1) or a dictionary
        print(' .. working on %s (lookup)' % sheet)
        dataframe = pd.read_excel(os.path.join(input_folder, supporting_data_file), sheet_name=sheet, skiprows=5)
        dataframe = dataframe.where((pd.notnull(dataframe)), None)
        output_dic = {}
        for number, data in dataframe.iterrows():
            if data[primary_col] is not None:  # not none

                # extract either a single column or a dictionary
                out_data = data[1] if lookup_col else data[1:].to_dict()

                # either append if it is already in dictionary or create new list
                if data[primary_col] in output_dic:
                    output_dic[data[primary_col]].append(out_data)
                else:
                    output_dic[data[primary_col]] = [out_data]

        if save:
            _save(output_dic, sheet)

        return output_dic

    # parse the sheets one by one
    # function to read in excel sheet with dictionary (unique values)
    _return_dict('sections', save=True, print_result=print_results)
    _return_dict('section_distributions', save=True, print_result=print_results)
    _return_dict('custom_locations', save=True, print_result=print_results)
    _return_dict('corridor_distributions', save=True, print_result=print_results)

    # function to read in excel sheet with lookup
    _return_lookup('related_sections', lookup_col=False, save=True)
    _return_lookup('blockages', lookup_col=False, save=True)

    _return_lookup('retrigger_event', lookup_col=True, save=True)
    _return_lookup('subsequent_sections', lookup_col=True, save=True)
    _return_lookup('preceding_sections', lookup_col=True, save=True)
    _return_lookup('section_corridors', lookup_col=True, save=True)
    _return_lookup('GTFS_stop_map', lookup_col=True, save=True)


if __name__ == '__main__':
    input_filename = 'Network Simulator supporting data_all.xlsx'
    parse_supporting_data(input_filename, print_results=False)
