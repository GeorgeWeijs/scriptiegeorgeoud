"""
    Read trips and stop_times from GTFS

    datapipeline as written up in setttings.py:
        FROM: INPUT_DATA, folder 0_input in datafolder
        TO: RAW_DATA, folder 1_raw
"""
import json
from src.settings import INPUT_DATA, RAW_DATA
from src.utils import utilities
import os
import csv
import pandas as pd


def parse_gtfs_trips(gtfs_folder, trips_filename, input_folder=INPUT_DATA, output_folder=RAW_DATA):
    """
    Parse gtfs trips
    """

    gtfs_trips = []

    with open(os.path.join(input_folder, gtfs_folder, trips_filename), newline='') as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            gtfs_trips.append(row)

    utilities.save_json(gtfs_trips, 'trips_{}'.format(gtfs_folder), output_folder)

    # return json if needed
    return gtfs_trips


def parse_gtfs_stop_times(gtfs_folder, stop_times_filename, input_folder=INPUT_DATA, output_folder=RAW_DATA):
    """
    Parse gtfs stop_times
    """

    gtfs_stop_times = []

    with open(os.path.join(input_folder, gtfs_folder, stop_times_filename), newline='') as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            gtfs_stop_times.append(row)

    utilities.save_json(gtfs_stop_times, 'stop_times_{}'.format(gtfs_folder), output_folder)

    # return json if needed
    return gtfs_stop_times


def parse_gtfs(gtfs_folder, stop_times_filename, trips_filename):
    """
    Parse both stop times and trips
    """
    parse_gtfs_trips(gtfs_folder, trips_filename)
    parse_gtfs_stop_times(gtfs_folder, stop_times_filename)


if __name__ == '__main__':
    trips_testfilename = 'trips.txt'
    stop_times_testfilename = 'stop_times.txt'
    gtfs_testfolder = 'GTFS_Parramatta_20180328'
    parse_gtfs(gtfs_testfolder, stop_times_testfilename, trips_testfilename)