import pandas as pd
import os
import openpyxl
from openpyxl import load_workbook
from src.settings import INPUT_DATA
import difflib
import scipy.stats as stats

def create_sections_table_from_bus_stations(df):
    new_sections = pd.DataFrame(columns = ["id", "section_length", "station_ind",'endpoint_ind', "section_name", "timing_station_endpoint_ind","chargingpoint_ind", "schematic_section_length_increase","max_capacity","direction", "network_focus","halte","timing_station_ind"])
    counter = 0
    new_sections = new_sections.set_index("id")
    df.loc[df['direction_id '] == "Naar Bijlmer", 'direction_id '] = "Richtig Haarlem"
    df.loc[df['direction_id '] == "Naar Haarlem", 'direction_id '] = "Richting Bijlmer"
    for station,direction  in zip(df['stop_name '],df['direction_id ']):
        if counter == 0:
            new_sections.loc[counter, "section_name"] = station
            previous_station = station
            new_sections.loc[counter, "max_capacity"] = 1
            new_sections.loc[counter, "direction"] =  df.loc[df['stop_name '] == station,"direction_id "].values[0]
            new_sections.loc[counter,  "direction"] = direction
            new_sections.loc[counter, "halte"] = True
            new_sections.loc[counter, "station_ind"] = True
            new_sections.loc[counter, "timing_station_ind"] = False
            counter += 1

        else:
            if previous_station==station:
                continue
            new_sections.loc[counter, "section_name"] =  previous_station + " - " + station
            new_sections.loc[counter, "direction"] = direction
            new_sections.loc[counter, "max_capacity"] = 100
            new_sections.loc[counter, "halte"] = False
            new_sections.loc[counter, "station_ind"] = False
            new_sections.loc[counter, "timing_station_ind"] = False
            counter += 1
            new_sections.loc[counter, "direction"] =  direction
            new_sections.loc[counter,  "section_name"] = station
            new_sections.loc[counter, "max_capacity"] = 1
            new_sections.loc[counter, "halte"] =True
            new_sections.loc[counter, "station_ind"] = True
            new_sections.loc[counter, "timing_station_ind"] = False
            previous_station = station
            counter += 1
    new_sections = new_sections.fillna(0)
    return(new_sections)

def create_corridors(new_sections):
    section_corridors = pd.DataFrame(columns = ['section_id', 'section_name'])
    section_corridors_distributions = pd.DataFrame(columns = ['id','section_mean', 'section_std','unit_of_distri','distri','minimum','corridor_name'])
    return section_corridors, section_corridors_distributions
    section_corridors['section_id'] = new_sections.index
    section_corridors['section_name'] = new_sections['section_name']
    section_corridors_distributions['id']= new_sections.index
    section_corridors_distributions= section_corridors_distributions.set_index("id")
    section_corridors_distributions['corridor_name'] = new_sections['section_name']
    section_corridors_distributions['section_mean'] = 0
    section_corridors_distributions['section_std'] = 0
    section_corridors_distributions['unit_of_distri'] = 'sec'
    section_corridors_distributions['distri'] = "gamma"


def create_section_distributions(new_sections, section_mean, section_std):
    section_distributions = pd.DataFrame(columns = ['id', 'section_name', 'section_mean','sections_std','unit_of_distri','distri','minimum','notes'])
    section_distributions = section_distributions.set_index('id')



    export_tijden = pd.read_csv(os.path.join(INPUT_DATA,'export_for_nws.csv'),delimiter = ';')

    export_tijden['Hour'] = pd.to_datetime(export_tijden.DR_VertrekTijd, format ="%H:%M:%S").dt.hour

    export_tijden.DR_Gerealiseerde_Rijtijd = \
        pd.to_numeric(
            export_tijden.DR_Gerealiseerde_Rijtijd.str.replace(',', '.')) * 24 * 60 * 60
    # remove irrelevant data points where travel time is larger than 0 and smaller than 1000
    export_tijden = export_tijden.loc[
        (export_tijden["DR_Gerealiseerde_Rijtijd"] > 0) & (
                    export_tijden["DR_Gerealiseerde_Rijtijd"] < 1000)]
    export_tijden.DR_Gerealiseerde_HalteerTijd = pd.to_numeric(
        export_tijden.DR_Gerealiseerde_HalteerTijd.str.replace(',', '.')) * 24 * 60 * 60
    # obtain distributions means, std's and minimums for sections
    export_tijden[(export_tijden.DR_Gerealiseerde_HalteerTijd > 0) & (export_tijden.DR_Gerealiseerde_Rijtijd > 30)]
    station_counter = 0
    distributions = pd.DataFrame(columns=['id', 'section_name', 'section_mean', 'sections_std', 'unit_of_distri',
                                          'distri', 'minimum', 'notes', 'section_std', 'network_focus'])
    distributions = distributions.set_index("id")
    axs = []
    figs = []

    # derive the average number of arrivals

    for station, direction in zip(new_sections.section_name, new_sections.direction):
        if new_sections.loc[new_sections.section_name == station, "halte"].values[0]:
            if new_sections.loc[new_sections.section_name == station, "halte"].values[0]:
                if (station in export_tijden.DR_HalteNaam_Corrected.unique()) == False:
                    print(station)
                    station_alt = difflib.get_close_matches(station, export_tijden.DR_HalteNaam_Corrected.unique())[0]
                    print(station_alt)
                else:
                    station_alt = station
                    print("was",station)
                    print('is',station_alt)

            if station_counter == 0:
                distributions.loc[station_counter, 'section_name'] = station
                distributions.loc[station_counter, 'section_mean'] = 180
                distributions.loc[station_counter, 'section_std'] = 1
                distributions.loc[station_counter, 'minimum'] = 30
                distributions.loc[station_counter, 'distri'] = "gamma"
                previous_station = station_alt
                station_counter += 2
                continue
            for group in export_tijden.loc[
                (export_tijden['DR_HalteNaam_Corrected'] == station_alt) & (
                        export_tijden['DR_Richting_naam'] == direction)].groupby(
                ['Hour']):
                hour = group[0]
                distributions.loc[station_counter-1,"distri"] = 'gamma'
                distributions.loc[station_counter - 1, 'section_name'] = previous_station + ' - ' + station
                distributions.loc[station_counter - 1, 'section_std_'+ str(hour)] = group[1].DR_Gerealiseerde_Rijtijd.std()
                distributions.loc[station_counter - 1, 'section_mean_'+str(hour)] =group[1].DR_Gerealiseerde_Rijtijd.mean()
                distributions.loc[station_counter - 1, 'minimum_'+ str(hour)] = group[1].DR_Gerealiseerde_Rijtijd.mean()*0.6
            previous_station = station
            if (station_counter == 86) or (station_counter == 42):
                for hour in range(4,23):
                    distributions.loc[station_counter, 'section_mean_' + str(hour)] = 340
                    distributions.loc[station_counter, 'section_std_'+ str(hour)] = 1
                    distributions.loc[station_counter, 'minimum_'+ str(hour)] = 40
                    distributions.loc[station_counter,'distri'] = 'gamma'
                distributions.loc[station_counter, 'section_name'] = station
            else:
                distributions.loc[station_counter, 'section_name'] = station
                # create section distributions for haltes
                for group in export_tijden.loc[
                    (export_tijden['DR_HalteNaam_Corrected'] == station_alt) & (
                            export_tijden['DR_Richting_naam'] == direction)].groupby(
                    ['datum', 'Hour']).DR_NrCheckins.sum().groupby('Hour'):
                    hour = group[0]
                    distributions.loc[station_counter, 'section_mean_' + str(hour)] = group[1].mean()
                    distributions.loc[station_counter, 'section_std_' + str(hour)] = group[1].std()
                    distributions.loc[station_counter,'distri'] = 'poisson'
            distributions['network_focus'] = 0
            distributions['unit_of_distri'] = "sec"
            relevant = export_tijden.loc[
                (export_tijden['DR_HalteNaam_Corrected'] == station_alt) & (
                        export_tijden['DR_Richting_naam'] == direction) & (export_tijden["DR_Gerealiseerde_HalteerTijd"]>0) &
            (export_tijden["DR_Gerealiseerde_HalteerTijd"]<600)]
            for nr_arrival in relevant.groupby(pd.cut(relevant['DR_NrCheckins'], [0, 1, 5, 10, 20, 30, 40])):
                arrival_amount = nr_arrival[0].right

                distributions.loc[station_counter, 'dwell_mean_arrival_' + str(arrival_amount)] = nr_arrival[1].DR_Gerealiseerde_HalteerTijd.mean()
                distributions.loc[station_counter, 'dwell_std_arrival_' + str(arrival_amount)] = nr_arrival[1].DR_Gerealiseerde_HalteerTijd.std()
            if station =="Schiphol, Airport":
                print(distributions.loc[station_counter], nr_arrival[1].DR_Gerealiseerde_HalteerTijd.mean())
            station_counter +=2

    return distributions

def append_df_to_excel(filename, df, sheet_name='Sheet1', startrow=None,startcol=None,truncate_sheet=False,
                       **to_excel_kwargs):

    # ignore [engine] parameter if it was passed
    if 'engine' in to_excel_kwargs:
        to_excel_kwargs.pop('engine')

    writer = pd.ExcelWriter(filename, engine='openpyxl')

    # Python 2.x: define [FileNotFoundError] exception if it doesn't exist
    try:
        FileNotFoundError
    except NameError:
        FileNotFoundError = IOError


    try:
        # try to open an existing workbook
        writer.book = load_workbook(filename)

        # get the last row in the existing Excel sheet
        # if it was not specified explicitly
        if startrow is None and sheet_name in writer.book.sheetnames:
            startrow = writer.book[sheet_name].max_row

        # truncate sheet
        if truncate_sheet and sheet_name in writer.book.sheetnames:
            # index of [sheet_name] sheet
            idx = writer.book.sheetnames.index(sheet_name)
            # remove [sheet_name]
            writer.book.remove(writer.book.worksheets[idx])
            # create an empty sheet [sheet_name] using old index
            writer.book.create_sheet(sheet_name, idx)

        # copy existing sheets
        writer.sheets = {ws.title:ws for ws in writer.book.worksheets}
    except FileNotFoundError:
        # file does not exist yet, we will create it
        pass

    if startrow is None:
        startrow = 0
    if startcol is None:
        startcol = "A"

    # write out the new sheet
    df.to_excel(writer, sheet_name, startrow=startrow, **to_excel_kwargs)

    # save the workbook
    writer.save()


if __name__ == '__main__':
    supporting_data_file = "Network Simulator supporting data_all.xlsx"
    sheet = "Sheet1"
    df = pd.read_excel(os.path.join(INPUT_DATA, supporting_data_file), sheet_name=sheet)
    new_sections = create_sections_table_from_bus_stations(df)


    section_distributions = create_section_distributions(new_sections,60,15)
    section_corridors, section_corridor_distributions = create_corridors(new_sections)

    append_df_to_excel(os.path.join(INPUT_DATA, supporting_data_file), new_sections, sheet_name='sections', startrow=5, startcol = "A", truncate_sheet=False)
    append_df_to_excel(os.path.join(INPUT_DATA, supporting_data_file), section_distributions, sheet_name='section_distributions', startrow=5, startcol = "A", truncate_sheet=False)
    append_df_to_excel(os.path.join(INPUT_DATA, supporting_data_file), section_corridors, sheet_name='section_corridors', startrow=5, startcol = "A", truncate_sheet=False)
    append_df_to_excel(os.path.join(INPUT_DATA, supporting_data_file), section_corridor_distributions, sheet_name='corridor_distributions', startrow=5, startcol = "A", truncate_sheet=False)
