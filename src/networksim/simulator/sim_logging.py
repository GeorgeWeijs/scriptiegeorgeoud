import time
import pandas as pd


class SimulationLogger(object):
    """
    Simulation logger object
    This object will be added to all trains, and then each train will log its activities
    in this object
    The object can print intermediate logging (default) or this can be disabled
    The results can be returned in dataframes
    """
    def __init__(self, name, any_printing=True, print_log_train=True, print_log_station=False):
        self.name = name
        self.print_log_train = print_log_train & any_printing
        self.print_log_station = print_log_station & any_printing
        if any_printing:
            print('Simulationlogger {} initialized'.format(name))
        self.train_activities_rows = []
        self.station_activivities_rows = []

    def train_activity_add(self, train_object, message):
        """
        Adds a message and a train_object in the train activity tables
        """
        row_to_add_dict = dict()
        row_to_add_dict['timestamp'] = train_object.env.now
        row_to_add_dict['timestamp_hr'] = time.strftime("%H:%M:%S", time.gmtime(train_object.env.now))
        row_to_add_dict['train_name'] = train_object.name
        row_to_add_dict['behind_schedule'] = train_object.behind_schedule
        if train_object.current_trip:
            row_to_add_dict['current_trip'] = train_object.current_trip
        else:
            row_to_add_dict['current_trip'] = 'NA'
        row_to_add_dict['message'] = message
        self.train_activities_rows.append(row_to_add_dict)
        if self.print_log_train:
            print('{:<6.3f}({:<8}) - {:<13} - ({:6}) - ({:+8.3f}) - {:<50}'.format(row_to_add_dict['timestamp'],
                                                                                row_to_add_dict['timestamp_hr'],
                                                                                row_to_add_dict['train_name'],
                                                                                row_to_add_dict['current_trip'],
                                                                                row_to_add_dict['behind_schedule'],
                                                                                row_to_add_dict['message']))

    def station_activity(self, train_object, arrival=True):
        """
        Adds a message and a train_object in the station activity tables, will be triggered once an
        arrival or departure happens at station
        """
        row_to_add_dict = dict()
        row_to_add_dict['timestamp'] = train_object.env.now
        row_to_add_dict['timestamp_hr'] = time.strftime("%H:%M:%S", time.gmtime(train_object.env.now))
        row_to_add_dict['train_name'] = train_object.name
        row_to_add_dict['behind_schedule'] = train_object.behind_schedule
        if train_object.current_trip:
            row_to_add_dict['current_trip'] = train_object.current_trip
        else:
            row_to_add_dict['current_trip'] = 'NA'

        row_to_add_dict['section_name'] = train_object.nw_sections[train_object.current_sec_id]['section_name']
        row_to_add_dict['section_id'] = train_object.current_sec_id
        row_to_add_dict['planned_timestamp'] = train_object.env.now - train_object.behind_schedule
        row_to_add_dict['is_peak'] = train_object.current_trip_peak

        row_to_add_dict['arrival'] = 'arrival' if arrival else 'departure'
        row_to_add_dict['direction'] = train_object.direction
        row_to_add_dict['wait_bc_early'] = train_object.wait_bc_early
        row_to_add_dict['wait_resource_time'] = train_object.wait_resource_time

        if arrival:
            row_to_add_dict['planned_arrival_headway'] = train_object.planned_arr_headway
            row_to_add_dict['planned_departure_headway'] = None
        else:
            row_to_add_dict['planned_arrival_headway'] = None
            row_to_add_dict['planned_departure_headway'] = train_object.planned_dep_headway

        self.station_activivities_rows.append(row_to_add_dict)
        if self.print_log_station:
            print('{:<6.3f}({:<8}) - {:<13} - ({:6}) - ({:+8.3f}) - {:<10} - {:<3} - {:<20}'
                  .format(row_to_add_dict['timestamp'], row_to_add_dict['timestamp_hr'], row_to_add_dict['train_name'],
                          row_to_add_dict['current_trip'], row_to_add_dict['behind_schedule'],
                          row_to_add_dict['arrival'], row_to_add_dict['section_id'],
                          row_to_add_dict['section_name']))

    def return_train_df(self):
        return pd.DataFrame(self.train_activities_rows)

    def return_station_df(self):
        return pd.DataFrame(self.station_activivities_rows)
