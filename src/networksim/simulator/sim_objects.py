import simpy
from src.utils import stochastic_fns, lrv_schedule_fns
import random
import numpy as np

import  datetime
class Train(object):
    def __init__(self, env, name, current_sec_id, start_time, nw_sections, sec_rec, blockage_mgr,
                 ended_trip_mgr, sim_logger, retrigger_event_mgr, schedule, starting_trains,regelmaat_mgr):
        self.env = env
        self.name = name
        self.starting = True
        self.start_time = start_time
        self.start_delay = 0
        self.meters_travelled = 0
        self.current_sec_id = current_sec_id
        self.current_loc_coords = None
        self.direction = None
        self.behind_schedule = 0
        self.wait_bc_early = None  # used for storing information at departure
        self.wait_resource_time = None  # used for storing information at departure next to logging etc
        self.total_sectional_runtime = 0
        self.total_dwelltime = 0
        self.total_wait_resource_time = 0
        self.total_wait_bc_early = 0
        self.sections_claimed = []
        # use of schedules: (optional)
        self.use_schedule = False if schedule is None else True
        # get the headways from the first in the schedule before removing it
        self.planned_dep_headway = None if schedule is None else schedule[0]['planned_departure_headway']
        self.planned_arr_headway = None if schedule is None else schedule[0]['planned_arrival_headway']
        # we remove the first stop from the roster since that is the departure stop (and time)
        # the train will on that section and find a candidate and then update the departure
        # thus has 'completed the first departure'
        self.schedule = None if schedule is None else schedule[1:]
        self.current_trip = None if schedule is None else self.schedule[0]['trip_id']
        self.current_trip_peak = None if schedule is None else self.schedule[0]['peak']
        self.starting_trains = starting_trains  # contains starting stops and times, needed for wait on start trns
        self.fake_dwell_at_termination = None  # fake dwell time indicator
        self.nw_sections = nw_sections  # network sections
        self.sec_rec = sec_rec  # section_resources
        self.blockage_mgr = blockage_mgr  # blockage manager
        self.ended_trip_mgr = ended_trip_mgr
        self.regelmaat_mgr = regelmaat_mgr
        self.sim_logger = sim_logger  # the simulation logger
        self.retrigger_event_mgr = retrigger_event_mgr
        # Start the run process everytime an instance is created.

        self.action = env.process(self.run())

    def run(self):
        while True:
            # if start_sec_id is not None, train should start
            if self.starting:
                # wait till start

                yield self.env.timeout(self.start_time-self.env.now)
                # wait till start current section claimed
                start_section_resource = yield self.sec_rec.get(lambda t: t['id'] == self.current_sec_id)
                self.sections_claimed = [start_section_resource]
                self.behind_schedule += self.env.now - self.start_time  # delay due to start resource delay
                self.total_wait_resource_time += self.env.now - self.start_time  # delay due to start resource delay

                if self.use_schedule:
                    # check assumption that no spawning train has to wait for claiming of start resource
                    # functionality has been build that for train claiming a section it check whether
                    # a train will spawn before it should depart, if so, it is ordered to wait for the train to spawn
                    # if it arrives at the same time the spawning train is scheduled to spawn and depart
                    # it assumed that the spawning train has first claimed for this time (at initialization)
                    # and thus will get it first, as is correct. See _train_about_to_spawn_on_any_section_claim
                    # it can still be triggered: if a train scheduled to dpearte before the spawning is heavily delaye
                    assert True
                    # assert self.env.now == self.start_time, 'This GTFS apparently != trains to start very separately'
                self.sim_logger.train_activity_add(self, 'COMMENCE, now on section {}, with delay {:+}'.format(
                    self.current_sec_id, self.env.now - self.start_time))

                time_last_bus = self.regelmaat_mgr.determine_if_bus_can_start(self.current_sec_id,self.env.now)
                time_last_bus =None
                if time_last_bus!= None:
                    if (time_last_bus < 9*60) & (time_last_bus > 7*60):
                        yield self.env.timeout(1*60)

                if time_last_bus!= None:
                    if (time_last_bus < 3*60):
                        print("cancelled", self.name)
                        #self.env.timeout(50 * 60)
                        self.ended_trip_mgr.end_trip(self, cancelled=True)
                        #break
                #yield self.env.timeout(extra_wait_before_start)
                #self.current_loc_coords, self.direction = self._get_location_coords_and_direction(loc_type='finish')
                # starting no longer
                self.starting = False

            # find next candidates
            candidates_plus_related_sections = self._find_next_candidates_plus_related_sections()

            if not candidates_plus_related_sections:
                # if no more candidates, end trip. Only used for non schedule case (150 trains spawn and do 1 lap)
                self.ended_trip_mgr.end_trip(self)
                break

            # request the sequence from the sequence resources
            pre_resource_time = self.env.now
            candidate_next_sec, sec_newly_claimed, sec_freed, blk_sec_claimed, blk_sec_freed, wait_on_spawn = \
                yield self.env.process(self._find_candidate_and_claim_free_resources(candidates_plus_related_sections))
            self.wait_resource_time = self.env.now - pre_resource_time
            self.behind_schedule += self.wait_resource_time
            self.total_wait_resource_time += self.wait_resource_time


            # once resource claimed
            # update the logging with an 'departure' activity, since the dwell time has completed and since the next
            # section is now claimed, the train has departed. Note the current_sec_id is artificially added here
            # for starting trains such that a departure of the first station is correctly recorded
            # note current_sec_id here is the previous sec, since it has not been updated after resource claimed
            if self.nw_sections[self.current_sec_id]['station_ind']:
                self.sim_logger.station_activity(self, arrival=False)

            self.wait_bc_early = None  # set to None again since dep activity registered

            log_msg = 'STARTING sequence id {}, resources claimed {} (new:{}) (freed:{}) with delay: {:+}'\
                .format(candidate_next_sec, [x['id'] for x in self.sections_claimed], sec_newly_claimed, sec_freed,
                        self.wait_resource_time)
            if len(blk_sec_claimed) > 0 or len(blk_sec_freed) > 0:
                log_msg += ' and triggered blockage manager to block: {} and free: {}'\
                    .format(blk_sec_claimed, blk_sec_freed)
            if wait_on_spawn > 0:
                log_msg += ' and had to wait {} on spawning train'.format(wait_on_spawn)
            self.sim_logger.train_activity_add(self, log_msg)

            # current sec set since resource claimed and started
            self.regelmaat_mgr.send_message_of_arrival_or_departure(self.current_sec_id, "departure", self.env.now,
                                                                    self.name)
            self.current_sec_id = candidate_next_sec

            # send message of arrival
            self.regelmaat_mgr.send_message_of_arrival_or_departure(self.current_sec_id, "arrival", self.env.now,
                                                                    self.name)
            # add kilometers to trip
            self.meters_travelled += self.nw_sections[self.current_sec_id]['section_length']

            self.wait_resource_time = None  # reset again, since departure has been logged and arrival msg recorded
            layover = None
            # if at a station
            if self.nw_sections[self.current_sec_id]['station_ind']:
                # if at station, determine layover:
                layover = self.nw_sections[self.current_sec_id]['layover']  # can be None
                # if train follows schedule
                if self.use_schedule:
                    # check that arrival matches next stop in the schedule, and update delay accordingly
                    gtfs_maps = self.nw_sections[self.current_sec_id]['gtfs_map']
                    assert self.schedule[0]['stop_id'] in gtfs_maps
                    self.behind_schedule = self.env.now - self.schedule[0]['arrival_time']
                    # update the logging with an 'arrival' activity, since the delay is now processed
                    # first set the headway for the next schedule, since the latest sched has not been removed yet
                    self.planned_dep_headway = self.schedule[0]['planned_departure_headway']
                    self.planned_arr_headway = self.schedule[0]['planned_arrival_headway']
                    self.sim_logger.station_activity(self, arrival=True)
                    arrival_time = self.schedule[0]['arrival_time']
                    if self.schedule[0]['departure_time'] == self.schedule[0]['arrival_time']:
                        # end of trip, trip will be finalized
                        self.sim_logger.train_activity_add(self, 'TRIP ENDING {}'.format(self.current_trip))
                        self.ended_trip_mgr.end_trip(self)
                        self.current_trip = None
                        # trip completed, next starts on next departure
                        # last stop of trip has happened (arrival stop), removed from list
                        # and next departure stop is now next to do (unless no more)
                        self.schedule = self.schedule[1:]
                        if len(self.schedule) == 0:
                            # this means that this is the last stop of the schedule for this LRV
                            # the arrival time is the only thing used and thus the LRV is terminated
                            # fake dwell is set to True, such that we wait equivalent of dwell time before
                            # freeing resources as passengers need to unboard
                            self.fake_dwell_at_termination = True
                            break
                    layover = self.schedule[0]['departure_time'] - arrival_time
                else:
                    # update the logging with an 'arrival' activity, since the dwell time will now commence
                    self.sim_logger.station_activity(self, arrival=True)

            # determine the length of the sequence time (stochastic) for the current id (id to now do)

            sec_time, delay, arriving_passengers = self._get_section_time(layover,self.env.now)


            # do the travelling, for sec_time length
            yield self.env.timeout(sec_time)

            # check time between the bus and the followed bus and back bus
            time_diff_next_bus, time_diff_back_bus = self.regelmaat_mgr.calculate_time_difference_between_busses(self.current_sec_id, self.name, self.env.now)
            self.wait_for_reg = self.regelmaat_mgr.determine_action(time_diff_next_bus, time_diff_back_bus)

            # add the delay to the overall delay (layover taken into account)
            self.behind_schedule += delay

            if self.nw_sections[self.current_sec_id]['halte']:
                self.sim_logger.train_activity_add(self, 'COMPLETED sequence id {}, with {} passengers arrived, took {}(delay affected by {:+.2f}) '
                                                     'time units --_HALTE'.format(self.current_sec_id, arriving_passengers,sec_time, delay))
            else:
                self.sim_logger.train_activity_add(self, 'COMPLETED seqeunce id {}, took {}(delay affected by {:+.2f}) '
                                                     'time units -- ROAD'.format(self.current_sec_id,sec_time, delay))

            # if at a station (completed), wait if the regelmaat manager tells the bus to wait
            if self.nw_sections[self.current_sec_id]['halte']:
                if self.wait_for_reg != None:
                    if sec_time < self.wait_for_reg:
                        # add dwell time
                        self.total_dwelltime += sec_time
                        self.extra_wait_time = self.wait_for_reg - sec_time
                        # if at a timing station, train cannot leave early and should wait accordingly
                        self.sim_logger.train_activity_add(self, 'WAITING at station {} for {} after completing '
                                                                 'section {}, before resuming'
                                                           .format(self.nw_sections[self.current_sec_id]['section_name'],
                                                                   self.extra_wait_time, self.current_sec_id))

                        yield self.env.timeout(self.extra_wait_time)
                        self.regelmaat_mgr.send_message_of_arrival_or_departure(self.current_sec_id, "departure",
                                                                                self.env.now,
                                                                                self.name)
                        # add early wait time
                        self.wait_bc_early = -1 * self.behind_schedule
                        self.total_wait_bc_early += self.wait_bc_early
                        self.behind_schedule = 0
                        self.sim_logger.train_activity_add(self, 'COMPLETED WAITING at station {} for {} after '
                                                                 'completing section {}, now resuming'
                                                           .format(self.nw_sections[self.current_sec_id]['section_name'],
                                                                   self.extra_wait_time, self.current_sec_id))
                # if train follows schedule and is not doing a trip (happens if previous arrival was end of trip)
                # update the new trip to the next scheduled departure and update the trip starting parameters
                if self.use_schedule:
                    if self.current_trip is None:  # this means that a new trip starts
                        self.current_trip = self.schedule[0]['trip_id']
                        self.current_trip_peak = self.schedule[0]['peak']
                        # reset all trip settings
                        self.total_wait_resource_time = 0
                        self.total_sectional_runtime = 0
                        self.total_dwelltime = 0
                        self.total_wait_bc_early = 0
                        self.meters_travelled = 0
                        self.start_time = self.env.now  # - min(self.behind_schedule, 0)# start time corrected for wait
                        # set start delay to delay or 0 if negative, since if negative delay, will wait with starting
                        self.start_delay = self.behind_schedule  # max(self.behind_schedule, 0)
                        self.sim_logger.train_activity_add(self,
                                                           'TRIP STARTING {} at {} with start delay {}'.format(
                                                               self.current_trip, self.start_time,
                                                               self.start_delay))
                    # check that departure matches current next stop in the schedule, and check delay matches
                    # behind_schedule @ arrival incl. delay (layover taken in account) should match delay at departure
                    gtfs_maps = self.nw_sections[self.current_sec_id]['gtfs_map']
                    assert self.schedule[0]['stop_id'] in gtfs_maps
                    assert round(self.behind_schedule, 3) == round(self.env.now - self.schedule[0]['departure_time'], 3)
                    # remove the next stop, this has been completed (will be logged once next resource claimed)
                    # first pick planned headways (might been changed if arrival schedule entry has been removed)
                    self.planned_dep_headway = self.schedule[0]['planned_departure_headway']
                    self.planned_arr_headway = self.schedule[0]['planned_arrival_headway']
                    self.schedule = self.schedule[1:]
                    # check assumption that if trip is always set to trip of next stop
                    assert self.current_trip == self.schedule[0]['trip_id']
            else:
                # add sectional run time
                self.total_sectional_runtime += sec_time

            # update location since section finished, so at 'to' location of section
            # self.current_loc_coords, self.direction = self._get_location_coords_and_direction(loc_type='finish')

        # terminate train and free all resources
        yield self.env.process(self._terminate_and_free_resources())

    def _find_next_candidates_plus_related_sections(self):
        """
        A function that returns the possible candidates as next section including their related sections
        """

        subsequent_sections = self.nw_sections[self.current_sec_id]['subsequent_sections']

        candidates_plus_related_sections = []
        for subseq_sec in subsequent_sections:
            # determine all sections to claim for this candidate:
            all_sections_to_be_claimed = {subseq_sec}  # make it a set
            if self.nw_sections[subseq_sec]['related_sections']:
                # if related section, add these to the section to be claimed: update with a set
                all_sections_to_be_claimed.update(set(self.nw_sections[subseq_sec]['related_sections']))

            # add tuple of candidate and complete list to claim for the candidate
            candidates_plus_related_sections.append((subseq_sec, all_sections_to_be_claimed))

        return candidates_plus_related_sections

    def _get_section_time(self, layover, time):
        """
        Function to generate travel time for sequence (seq_id), based on distribution
        """
        hour = int(time/60/60)
        mu = self.nw_sections[self.current_sec_id]['section_mean_' + str(hour)]
        std = self.nw_sections[self.current_sec_id]['section_std_' + str(hour)]
        distri = self.nw_sections[self.current_sec_id]['distri']
        minimum = self.nw_sections[self.current_sec_id]['minimum_'+str(hour)]
        arriving_passengers = None
        if (self.current_sec_id =="85") or (self.current_sec_id =="0") :
            return 30, 0, None
        if distri == 'normal':
            section_time = stochastic_fns.get_norm_time_cutoff(mu, std, nr_of_std_cutoff=1)
        elif distri == 'gamma':
            if (mu ==None) or (minimum==None):
                mu = self.nw_sections[self.current_sec_id]['section_mean_' + str(hour+1)]
                std = self.nw_sections[self.current_sec_id]['section_std_' + str(hour+1)]
                distri = self.nw_sections[self.current_sec_id]['distri']
                minimum = self.nw_sections[self.current_sec_id]['minimum_' + str(hour+1)]
            section_time = stochastic_fns.get_gamma_time_fixed_minimum(mu, std, minimum)
        elif distri == 'poisson':
            time_last_bus = self.regelmaat_mgr.last_bus_arrived_at_stop(self.current_sec_id)
            time_between_busses = self.env.now - time_last_bus
            if mu ==None:
                mu=5
            arriving_passengers = sum(np.random.poisson(lam=(mu/60), size=[round(time_between_busses/60)]))
            if arriving_passengers ==0:
                mu_dwell = self.nw_sections[self.current_sec_id]['dwell_mean_arrival_1']
                std_dwell = self.nw_sections[self.current_sec_id]['dwell_std_arrival_1']
                if mu_dwell ==None:
                    minimum = 40
                else:
                    minimum = 0.6*mu_dwell
                section_time = stochastic_fns.get_gamma_time_fixed_minimum(mu_dwell, std_dwell, minimum)
            elif (arriving_passengers >0) & (arriving_passengers<5):
                mu_dwell = self.nw_sections[self.current_sec_id]['dwell_mean_arrival_5']
                std_dwell = self.nw_sections[self.current_sec_id]['dwell_std_arrival_5']
                if mu_dwell ==None:
                    minimum = 40
                else:
                    minimum = 0.6*mu_dwell
                section_time = stochastic_fns.get_gamma_time_fixed_minimum(mu_dwell, std_dwell, minimum)
            elif (arriving_passengers >4) & (arriving_passengers<10):
                mu_dwell = self.nw_sections[self.current_sec_id]['dwell_mean_arrival_10']
                std_dwell = self.nw_sections[self.current_sec_id]['dwell_std_arrival_10']
                if mu_dwell ==None:
                    minimum = 40
                else:
                    minimum = 0.6*mu_dwell
                section_time = stochastic_fns.get_gamma_time_fixed_minimum(mu_dwell, std_dwell, minimum)
            elif (arriving_passengers >9) & (arriving_passengers<20):
                mu_dwell = self.nw_sections[self.current_sec_id]['dwell_mean_arrival_20']
                std_dwell = self.nw_sections[self.current_sec_id]['dwell_std_arrival_20']
                if mu_dwell ==None:
                    minimum = 40
                else:
                    minimum = 0.6*mu_dwell
                section_time = stochastic_fns.get_gamma_time_fixed_minimum(mu_dwell, std_dwell, minimum)
            elif (arriving_passengers > 19) & (arriving_passengers < 29):
                mu_dwell = self.nw_sections[self.current_sec_id]['dwell_mean_arrival_30']
                std_dwell = self.nw_sections[self.current_sec_id]['dwell_std_arrival_30']
                if mu_dwell ==None:
                    minimum = 40
                else:
                    minimum = 0.6*mu_dwell

                section_time = stochastic_fns.get_gamma_time_fixed_minimum(mu_dwell, std_dwell, minimum)
            else:
                mu_dwell = self.nw_sections[self.current_sec_id]['dwell_mean_arrival_40']
                std_dwell = self.nw_sections[self.current_sec_id]['dwell_std_arrival_40']
                if mu_dwell ==None:
                    minimum = 20
                else:
                    minimum = 0.6*mu_dwell
                section_time = stochastic_fns.get_gamma_time_fixed_minimum(mu_dwell, std_dwell, minimum)
        else:
            assert False, 'not supported distribution'

        if self.nw_sections[self.current_sec_id]['unit_of_distri'] == 'sec':
            section_time = round(section_time, 3)
            mu_compare = mu
        elif self.nw_sections[self.current_sec_id]['unit_of_distri'] == 'ms':
            section_time = round(self.nw_sections[self.current_sec_id]['section_length'] / section_time, 3)
            mu_compare = round(self.nw_sections[self.current_sec_id]['section_length'] / mu, 3)
        else:
            assert False, 'Unknown unit of distribution'

        # handle charging time (minimum time needed to stop)
        # if mimimum is set in section times, and charging time is less than that, this is obsolete
        if self.nw_sections[self.current_sec_id]['charging_time']:
            charging_time = self.nw_sections[self.current_sec_id]['charging_time']
            section_time = max(section_time, charging_time)

        # calculate delay as difference between section time and mean section time
        delay = section_time - mu_compare

        assert section_time >= 0, print('{} is not positive'.format(section_time))

        # now adjust delay according to layover at this stop
        if layover:
            # if layover is in place, calculate delay compared to this as this is schedule
            delay = section_time - layover
            # a negative delay will trigger a wait, since at station cannot leave with negative delay (early)
        return section_time, delay, arriving_passengers

    def _get_location_coords_and_direction(self, loc_type='middle'):

        """
        Function that returns the location (coordinates) for a certain section id
        given the network sections
        For each section (A-B) there can be two locations
        - while on section (doing the travel time) the location is between A and B
        - once a section is finished the location is B, until the next section is claimed again
        """
        assert loc_type in ['middle', 'finish'], 'type input is incorrect: should be middle or end'

        seq_id = self.current_sec_id

        if loc_type == 'middle':
            # currently on the section
            direction = self.direction  # dont allow direction changes when on the middle of the track
            coords = self.nw_sections[seq_id]['middle_point']
        else:
            # completed the section
            direction = self.nw_sections[seq_id]['direction']
            coords = [self.nw_sections[seq_id]['coordinates_xs'][1], self.nw_sections[seq_id]['coordinates_ys'][1]]

        # direction = self.nw_sections[seq_id]['direction']
        assert direction in ['from_start'], 'unknown direction found'
        return coords, direction

    def _find_candidate_and_claim_free_resources(self, candidates_plus_related_sections):
        """
        candidates_plus_related_sections is a list of tuples:
        First element is the candidate
        Second element is a set of all sections that need to be claimed for this candidate (including candidate self)
        """

        assert len(candidates_plus_related_sections) > 0, 'need at least one candidate tuple'

        # define needed variables
        found_candidate = new_to_claim = all_sections_to_be_claimed = None
        wait_on_spawn = 0  # variable to determine if it was needed to wait on a spawning train

        # find which are already claimed
        already_claimed = set((x['id'] for x in self.sections_claimed))  # make a set

        candidate_new_to_claim_list = []
        # add new_to_claim to tuples in new list, new_to_claim will be check on availability later
        for candi_update, all_sections_to_be_claimed_update in candidates_plus_related_sections:
            new_to_claim_update = all_sections_to_be_claimed_update - already_claimed  # set operation
            candidate_new_to_claim_list.append((candi_update, all_sections_to_be_claimed_update, new_to_claim_update))

        # set time for retrigger (first time it was finished and was ready to claim next resource)
        time_for_retrigger = self.env.now

        # if only one candidate with only one section to claim, and no blockage requests
        # select that candidate since no partly claim problem, it can just wait for its resource to become available
        if len(candidate_new_to_claim_list) == 1 and len(candidate_new_to_claim_list[0][2]) == 1 and \
                self.nw_sections[candidate_new_to_claim_list[0][0]]['blockage'] is None:
            found_candidate, all_sections_to_be_claimed, new_to_claim = candidate_new_to_claim_list[0][0], \
                                                                        candidate_new_to_claim_list[0][1], \
                                                                        candidate_new_to_claim_list[0][2]
            # check if there is a train about to spawn on candidate section, if so, wait till it spawned
            train_about_to_spawn_on_ntc, wait_on_spawn = self._train_about_to_spawn_on_any_section_claim(new_to_claim)
            if wait_on_spawn > 0:
                yield self.env.timeout(wait_on_spawn)

        else:  # if multiple candidates or if more than one resource or at least 1 resource and block request
            # loop through all candidates and see if all needed resources of this candidate are available
            # wait until all desired resources are available to avoid partly claiming (resource or block) and all
            # waiting for everything if not, check next candidate, and if no candidate available
            # wait for a retrigger if specified, else wait till next time, but minimum of 1
            no_candidate_found = True
            while no_candidate_found:
                id_s_in_section_resources = set([x['id'] for x in self.sec_rec.items])
                # first candidate that has all resources available is the going to be the next section
                for candidate_srch, all_sections_to_be_claimed_srch, new_to_claim_srch in candidate_new_to_claim_list:
                    # determine if train is about to spawn on candidate, if so, treat it as occupied
                    train_about_to_spawn_on_ntc, _ = self._train_about_to_spawn_on_any_section_claim(new_to_claim_srch)
                    if new_to_claim_srch.issubset(id_s_in_section_resources) and train_about_to_spawn_on_ntc is False:
                        no_candidate_found = False
                        # use data from found candidate to set parameters (last values from loop)
                        found_candidate, all_sections_to_be_claimed, new_to_claim = \
                            candidate_srch, all_sections_to_be_claimed_srch, new_to_claim_srch
                        break
                else:
                    # if no candidate found for which all section can be claimed now (due to unfree or about to spawn)
                    if self.nw_sections[self.current_sec_id]['need_retrigger_if_no_candidate']:
                        # 'passivate' by making a retrigger event and then wait for the event to be retriggered
                        retrigger_event = self.retrigger_event_mgr.add_event(self.current_sec_id, time_for_retrigger)
                        # time of first addition returned, if still no candidate, event will be re-added with original
                        # time, to maintain order always
                        time_for_retrigger = yield retrigger_event
                    else:
                        # if no candidate is all available and no retrigger event specified wait till next time,
                        # before retrying but minimally 1
                        yield self.env.timeout(max(self.env.peek() - self.env.now - 1, 1))

        # find which to keep and free, use set logic
        keep_claimed_ids = all_sections_to_be_claimed & already_claimed
        sections_to_free_once_next_claimed_ids = already_claimed - all_sections_to_be_claimed

        keep_claimed = [x for x in self.sections_claimed if x['id'] in keep_claimed_ids]
        sections_to_free_once_next_claimed = \
            [x for x in self.sections_claimed if x['id'] in sections_to_free_once_next_claimed_ids]
        # assert that sections to keep and sections to free add to the currently claimed list
        assert set([x['id'] for x in keep_claimed] + [x['id'] for x in sections_to_free_once_next_claimed]) == \
            set([x['id'] for x in self.sections_claimed])

        # start claiming sections and proces blockage/freeage request if present
        # define event for claiming all sections (that are free at this point)
        claim_event = simpy.events.AllOf(self.env, [self.sec_rec.get(lambda t: t['id'] == section_to_claim) for
                                                    section_to_claim in new_to_claim])

        blockage_sec_claimed = []
        blockage_sec_freed = []
        # find if any blockages should be blocked/freed, if so, create event for processing the block/free request:
        if self.nw_sections[found_candidate]['blockage']:
            process_block_free_event_list = []
            for sec_id, block_or_free in self.nw_sections[found_candidate]['blockage'].items():
                # create event for blockage/freeage request
                process_block_free_event = self.env.process(self.blockage_mgr.block_or_free(block_or_free, sec_id))
                process_block_free_event_list.append(process_block_free_event)

            # define event for processing all block/free requests
            blockfree_event = simpy.events.AllOf(self.env, process_block_free_event_list)
            nr_of_block_free_events = len(process_block_free_event_list)

            # wait for all resources to be claimed AND block/free request to be processed
            ret = yield claim_event & blockfree_event
            # the last one will be the blockage/freeing request, all before are claimage requests
            ret_values = [event for event in ret.values()]
            sections_newly_claimed = ret_values[:-nr_of_block_free_events]
            blockage_sec_claimed, blockage_sec_freed = list(zip(*ret_values[-nr_of_block_free_events:]))
        else:
            # if no blockage request, wait for all the resources to be claimed
            ret = yield claim_event
            sections_newly_claimed = [event for event in ret.values()]

        # once claimed, can free the rest
        yield simpy.events.AllOf(self.env, [self.sec_rec.put(section_to_free) for section_to_free in
                                            sections_to_free_once_next_claimed])

        retrigger_list = set()
        for section_thats_freed in [x['id'] for x in sections_to_free_once_next_claimed]:
            if self.nw_sections[section_thats_freed]['retrigger_events_if_freed']:
                retrigger_list.update(self.nw_sections[section_thats_freed]['retrigger_events_if_freed'])
        self.retrigger_event_mgr.retrigger_event(retrigger_list)

        # total list that is currently claimed
        self.sections_claimed = keep_claimed + sections_newly_claimed

        sections_freed_ids = [x['id'] for x in sections_to_free_once_next_claimed]
        sections_newly_claimed_ids = [x['id'] for x in sections_newly_claimed]

        return found_candidate, sections_newly_claimed_ids, sections_freed_ids, blockage_sec_claimed, \
               blockage_sec_freed, wait_on_spawn

    def _train_about_to_spawn_on_any_section_claim(self, new_to_claim):
        """
        This function takes candidate new_to_claim sections and finds out if there is a train about to spawn
        on any starting station section before the current train is scheduled to depart here. Note that if this train
        is scheduled to have departed before a new spawn, but incurs delay, it will still delay the spawn, but this
        is as intended, since the schedule expected this train to pass the starting stop before the spawning
        """

        train_about_to_spawn_on_ntc, wait_on_spawn = False, 0

        # before claiming a next section, check departure time, and check whether nr of starting trains
        # have already started. If not, wait because you might block the spawning of a new starting train
        # and we assume a driver to wait if it knows a train is about to arrive from the depot
        if self.starting_trains is None:
            # if no starting trains defined (in no schedule case), never wait as all trains spawn at same location
            # in order and no schedule to mess up
            return train_about_to_spawn_on_ntc, wait_on_spawn
        else:
            # find new to claim starting stop (section that is station within the new to claim secs where trains start)
            new_to_claim_start_stop = None
            for section in new_to_claim:
                if section in self.starting_trains:
                    # check assumption that a train never has more than 1 stop new to claim per candidate
                    # and thus that no previous has been found before in the new_to_claim list
                    assert new_to_claim_start_stop is None
                    new_to_claim_start_stop = section

            if new_to_claim_start_stop is None:
                # this means that the new to claim does not contain a stop where trains start, so no train to spawn
                return train_about_to_spawn_on_ntc, wait_on_spawn
            else:
                # in the new to claim sections, one of the section is a station where trains spawn. Find out if
                # a train is about to spawn there before this trains departure time, if so, we have to wait for this

                # get our departure time from the next stop in our schedule
                if self.schedule[0]['departure_time'] == self.schedule[0]['arrival_time'] and len(self.schedule) > 1:
                    # if the next scheduled activity has same dep and arr, its a arrival activity,
                    # and we should look at the next activity to find the dep time for the candidate section
                    sched_dep_time = self.schedule[1]['departure_time']
                else:
                    # the next activity has a different departure time (thus not a trip transition stop) or is the
                    # last activity, where there is no departure. We assume here that the last activity is never
                    # on a starting stop (it will be one before to go to depot) so the simplification that the arrival
                    # here is the departure will not cause problems for spawning trains
                    sched_dep_time = self.schedule[0]['departure_time']
                # get spawn times from this spawning station
                list_of_spawn_times = self.starting_trains[new_to_claim_start_stop]

                # now we have scheduled dep time for our candidate and the list of spawn times on the start stop section
                # find out if between now and the next departure, there is a train spawning
                for time in list_of_spawn_times:
                    if self.env.now <= time <= sched_dep_time:
                        # this means there is a train to spawn at the stop between now and our scheduled dep time
                        # which means this train should wait for that, as it does not want to block the spawning
                        # if it is scheduled for the spawning to happen first
                        train_about_to_spawn_on_ntc = True
                        # time to wait before claiming section and let the train spawn is the difference
                        # betwen the planned spawn train and the current time.
                        # we add 1 second to enforce that this train claims the section 1 second after the train
                        # spawns and not steals it 'on' the second
                        wait_on_spawn = (time - self.env.now)

        return train_about_to_spawn_on_ntc, wait_on_spawn

    def _terminate_and_free_resources(self):
        """
        Once a train does not have any more candidates or is terminated for any other reason
        This function triggers which does the cleanup: make a log entry, set coordinates to None
        and add the train to the terminated_train_manager to register all trains that are terminated
        Furthermore it frees all claimed resources
        """
        # if you terminate a train as a result of finishing the last trip, it has just arrived.
        # in reality it should claim the resources while it 'dwells' and let the people off
        # so it should wait before freeing all resources, so it prevent the next trains from claiming it too soon
        if self.fake_dwell_at_termination:
            fake_dwell_wait, _ = self._get_section_time(None)
            self.sim_logger.train_activity_add(self, 'WAITING BEFORE TERMINATING for {}'.format(fake_dwell_wait))
            yield self.env.timeout(fake_dwell_wait)

        # if train ended (break was used)
        self.sim_logger.train_activity_add(self, 'TERMINATED')
        # set the location to the 'termination' loc
        self.current_loc_coords = None

        # if the train was terminated while the schedule was not complete, it means that
        # the current trip has been ended mid trip (this is handled by the endtrip function)
        # at the same time it means there might be trips scheduled for this train
        # that do not even start and are cancelled
        # these trips are next ended
        if self.use_schedule:
            # schedule countains part of the trip that was active during ending/cancel and the stops of future trips
            while len(self.schedule) > 0:
                if self.schedule[0]['trip_id'] != self.current_trip:
                    # found a trip id that has not been ended yet
                    # set variables accordingly and end
                    self.current_trip = self.schedule[0]['trip_id']
                    self.current_trip_peak = self.schedule[0]['peak']
                    self.total_wait_resource_time = 0 #'NA'
                    self.behind_schedule = 0 #'NA'
                    self.start_delay = 0 #'NA'
                    self.meters_travelled = 0
                    self.start_time = self.env.now
                    # needed for the calculation of the planned kms during ending of trip
                    self.current_sec_id = lrv_schedule_fns.find_start_section(self.schedule)
                    self.ended_trip_mgr.end_trip(self, cancelled=True)
                else:
                    # this entry was the current trip or an already ended trip during termination
                    self.schedule = self.schedule[1:]

        # free all resources
        yield simpy.events.AllOf(self.env, [self.sec_rec.put(section_to_free) for section_to_free in
                                            self.sections_claimed])

    def where_are_you(self):
        return self.current_loc_coords, self.direction


class RetriggerEventMgr(object):
    def __init__(self, env, name):
        self.env = env
        self.name = name
        self.retrigger_event_chain = []  # list that keeps the events tuples in order of addition
        self.section1066_retrigger_event = None

    def add_section1066_retrigger(self):
        self.section1066_retrigger_event = self.env.event()

    def retrigger_section1066(self):
        if self.section1066_retrigger_event is not None and self.section1066_retrigger_event.triggered is False:
            self.section1066_retrigger_event.succeed()

    def add_event(self, section_that_needs_retrigger, time_of_addition):
        retrigger_event = self.env.event()
        self.retrigger_event_chain.append((section_that_needs_retrigger, retrigger_event, time_of_addition))
        self.retrigger_event_chain = sorted(self.retrigger_event_chain, key=lambda x: x[2])
        return retrigger_event

    def retrigger_event(self, retrigger_list):
        """
        Go through the ordere retrigger event chain and retrigger event if they are in the retrigger list
        This makes sure the order is maintained
        """
        new_retrigger_event_chain = []
        events_that_will_be_triggered = []
        for event_tuple in self.retrigger_event_chain:
            if event_tuple[0] in retrigger_list:
                # if a retrigger event is in the current retrigger list, add to list that will be triggered
                events_that_will_be_triggered.append((event_tuple[1], event_tuple[2]))
            else:
                # if not, it means this retrigger list does not retrigger this event, goes back in chain (in order)
                new_retrigger_event_chain.append(event_tuple)

        # once all processed, first set new retrigger_event_chain
        self.retrigger_event_chain = new_retrigger_event_chain

        # now trigger all events in order
        for retrigger_event, event_time in events_that_will_be_triggered:
            # return time when event was added, such that in case of re-addition, same time can be used
            retrigger_event.succeed(value=event_time)


class EndedTripMgr(object):
    def __init__(self, name):
        self.name = name
        self.trip_ended_details = {'train_names': [], 'current_trips': [], 'trips_wait_resource_time': [],
                                   'trips_behind_schedule': [], 'trips_total_run_time': [], 'trip_starting_delay': [],
                                   'plot_id': [], 'trip_service_meters': [], 'trip_planned_meters': [],
                                   'is_cancelled': [], 'is_peak': [], 'trips_planned_total_run_time': [],
                                   'trip_total_dwelltime': [], 'trip_total_sectional_runtim': [],
                                   'trip_wait_bc_early': []}

    def end_trip(self, train_object, cancelled=False):
        total_trip_time = train_object.env.now - train_object.start_time

        # here the planned kilometers are calculated for the case that the train was cancelled and the trip
        # was not completed, and a schedule is present
        planned_meters = 'NA'
        if train_object.use_schedule:
            missed_meters = lrv_schedule_fns.calculated_missed_meters_and_remove_trip_stops(train_object)
            planned_meters = train_object.meters_travelled + missed_meters

        is_cancelled = 'No'
        if cancelled:
            is_cancelled = 'Yes'

        # add information from the train at termination to a collection dict
        self.trip_ended_details['train_names'].append(train_object.name)
        if train_object.current_trip:
            self.trip_ended_details['current_trips'].append(train_object.current_trip)
            self.trip_ended_details['is_peak'].append('Yes' if train_object.current_trip_peak else 'No')
            self.trip_ended_details['plot_id'].append('{}({})'.format(train_object.current_trip, train_object.name))
        else:
            self.trip_ended_details['current_trips'].append('NA')
            self.trip_ended_details['is_peak'].append('NA')
            self.trip_ended_details['plot_id'].append('{}'.format(train_object.name))
        self.trip_ended_details['trips_wait_resource_time'].append(train_object.total_wait_resource_time)
        self.trip_ended_details['trips_behind_schedule'].append(train_object.behind_schedule)
        self.trip_ended_details['trips_total_run_time'].append(total_trip_time)
        self.trip_ended_details['trip_starting_delay'].append(train_object.start_delay)
        self.trip_ended_details['trips_planned_total_run_time'].append(
            total_trip_time - train_object.behind_schedule + train_object.start_delay)
        self.trip_ended_details['trip_total_dwelltime'].append(train_object.total_dwelltime)
        self.trip_ended_details['trip_total_sectional_runtim'].append(train_object.total_sectional_runtime)
        self.trip_ended_details['trip_wait_bc_early'].append(train_object.total_wait_bc_early)
        self.trip_ended_details['trip_service_meters'].append(train_object.meters_travelled)
        self.trip_ended_details['trip_planned_meters'].append(planned_meters)
        self.trip_ended_details['is_cancelled'].append(is_cancelled)


class BlockageMgr(object):
    """Single track set blockage manager object
    This object can be triggered by trains to block a set of sections
    This object keeps track of how many trains want it blocked, and block it if this is at least 1
    It unblocks the sections once no train wants it blocked anymore
    This enables the blocking to be done 'outside' the train which enables multiple trains
    to let their blocking known, but only free once all trains are off the single track set"""
    def __init__(self, env, name, sec_rec):
        self.env = env
        self.name = name
        self.sec_rec = sec_rec  # section_resources
        self.sec_block_count = {}  # dict that keeps the block count for each section

    def block_or_free(self, block_or_free, sec_id):
        if sec_id not in self.sec_block_count:
            self.sec_block_count[sec_id] = {'count': 0, 'claimed_sections': None}

        old_count = self.sec_block_count[sec_id]['count']
        if block_or_free == 'block':
            self.sec_block_count[sec_id]['count'] += 1
        elif block_or_free == 'free':
            # claims cannot become negative
            self.sec_block_count[sec_id]['count'] -= 1 if self.sec_block_count[sec_id]['count'] > 0 else 0
        else:
            assert False, 'error'

        sections_to_now_claim = []
        sections_freed_ids = []

        if old_count == 0 and self.sec_block_count[sec_id]['count'] > 0:
            # the section was not claimed before and should now be actually claimed
            sections_to_now_claim = [sec_id]
            new_sections_claimed_dict = yield simpy.events.AllOf(self.env, [
                self.sec_rec.get(lambda t: t['id'] == section_to_claim) for section_to_claim in sections_to_now_claim])
            self.sec_block_count[sec_id]['claimed_sections'] = [x for x in new_sections_claimed_dict.values()]
        elif old_count > 0 and self.sec_block_count[sec_id]['count'] == 0:
            # the section has been freed by all claiming trains and should now be actually freed
            sections_to_now_free = self.sec_block_count[sec_id]['claimed_sections']
            yield simpy.events.AllOf(self.env, [self.sec_rec.put(section_to_free) for section_to_free in
                                                sections_to_now_free])
            sections_freed_ids = [x['id'] for x in sections_to_now_free]
            self.sec_block_count[sec_id]['claimed_sections'] = None
        else:
            # nothing happens
            # still more than 1 claimy or still 0 claimy and all is free for sec_id
            assert (old_count > 0 and self.sec_block_count[sec_id]['count'] > 0) or \
                   (old_count == self.sec_block_count[sec_id]['count'] == 0)

        return sections_to_now_claim, sections_freed_ids

class RegelmaatMgr(object):
    """
    The Regelmaat manager:
    This object manages the regelmaat with different possible "rules" which saves the regelmaat in the system.
    The livelocator is the system to which the live location of the busses can be send to which is updated during
    simulation when busses arrive and departure bus stops. The other functions determine the actions for managing Regelmaat

    """
    def __init__(self, env: object, name: str, sections: str, regelmaat_mgr_settings: str, desired_interval: int):
        self.env = env
        self.name = name
        self.desired_interval = desired_interval
        self.regelmaat_mgr_settings = regelmaat_mgr_settings
        self.sections = sections
        self.max_waiting_time = 120
        self.live_locator =dict(zip(list(sections.keys()),["None"]*len(sections)))
        for id in self.live_locator:
            self.live_locator[id] = {}
            self.live_locator[id]['arrival'] = {}
            self.live_locator[id]['arrival']['time'] = 0
            self.live_locator[id]['arrival']['vehicle']=None

            self.live_locator[id]['departure'] = {}
            self.live_locator[id]['departure']['time'] = 0
            self.live_locator[id]['departure']['vehicle']=None
            self.live_locator[id]['departure']['time_last_vehicle'] = None

    def last_bus_arrived_at_stop(self, section):

        """
        check when the last bus arrived at a section
        """
        return self.live_locator[section]["departure"]['time']

    def send_message_of_arrival_or_departure(self, message_id: str, type: str, timestamp: int, vehicle: str):

        """
        :param message_id: the relevant section for which the update is send
        :param type: the type of message; arrival or departure
        :param timestamp: the time of when the message has been send
        :param vehicle: the vehicle name that arrives at the relevant segment
        :return:
        """
        self.live_locator[message_id][type]['time'] = timestamp
        self.live_locator[message_id][type]["vehicle"] = vehicle

        # update the departure time of the last vehicle at the message section
        self.live_locator[message_id]['departure']['time_last_vehicle'] = self.live_locator[message_id][type]['time']

    def _look_forward(self, time_diff_ahead: int) -> int:
        """
        :param time_diff_ahead: the difference between the vehicle riding ahead of the relevant vehicle
        :return: the amount of time the vehicle has to wait at a bus stop for regelemaat
        """
        # obtain the deviation of the interval
        dev_desired_inter = self.desired_interval - time_diff_ahead

        # determine amount of waiting time for regelmaat
        if dev_desired_inter > 60:
            if dev_desired_inter >120:
                if dev_desired_inter > 180:
                    wait_for_reg = 3*60
                else:
                    wait_for_reg = 2.5*60
            else:
                wait_for_reg = 1.5 * 60
        else:
            wait_for_reg = 0

        return wait_for_reg

    def _look_back_and_forward(self, time_diff_ahead: int, time_diff_follower: int)-> int:
        """
        :param time_diff_ahead: difference between vehicle riding ahead of the relevant vehicle
        :param time_diff_follower: difference between the vehicle following the relevant vehicle
        :return: the amount of time the vehicle has to wait at a bus stop for regelemaat
        """
        # when no vehicle is riding behind the relevant vehicle, look forward
        if time_diff_follower == None:
            wait_for_reg = self._look_forward(time_diff_ahead)
            return wait_for_reg

        # when no vehicle is riding ahead of the relevant vehicle, wait for the vehicle riding at the back
        elif time_diff_ahead == None:
            dev_desired_inter = time_diff_follower - self.desired_interval
            wait_for_reg = dev_desired_inter / 3
            return wait_for_reg

        # waiting time for regelmaat if the bus wants to drive in the middle of its follower and the bus ahead
        wait_for_reg = (time_diff_follower + time_diff_ahead)/2

        dev_desired_inter_ahead = self.desired_interval - time_diff_ahead
        dev_desired_inter_back = time_diff_follower - self.desired_interval

        # if either the follower is bunching or you are bunching us look backward or look forward  respectively

        if dev_desired_inter_back < - 2*60:
            wait_for_reg = self._look_forward(time_diff_ahead)/1.5
            return  wait_for_reg
        elif dev_desired_inter_ahead < - 2*60:
            wait_for_reg = dev_desired_inter_back/4
            return  wait_for_reg
        if wait_for_reg < 0:
            wait_for_reg = 0
            return wait_for_reg

        # when the bus following is more than 2 minutes out of the regelmaat interval wait for 60 seconds
        if dev_desired_inter_back > 2*60:
            wait_for_reg = 60
            return wait_for_reg

        # when the bus ahead is more than 2 minutes out of the regelmaat intercal wait for 60 seconds
        if dev_desired_inter_ahead > 2*60:
            wait_for_reg = 60
            return wait_for_reg



        # when waiting time exceeds maximum waiting time, wait maximum time
        if wait_for_reg > self.max_waiting_time:
            wait_for_reg = self.max_waiting_time
            return wait_for_reg
        return wait_for_reg

    def determine_action(self, time_diff_ahead, time_diff_follower):
        """
        :param time_diff_ahead: time between relevant vehicle and bus ahead
        :param time_diff_follower: time between relevant vehicle and bus following
        :return: waiting to to wait
        """

        if time_diff_ahead != None:
            dev_desired_inter = self.desired_interval - time_diff_ahead
        else:
            wait_for_reg = 0
            return wait_for_reg

        # use the "rule" manager based on the settings
        if self.regelmaat_mgr_settings == "F":
            wait_for_reg = self._look_forward(time_diff_ahead)
        elif self.regelmaat_mgr_settings == "BAF":
            if dev_desired_inter > 2*60:
                wait_for_reg = self._look_forward(time_diff_ahead)
            else:
                wait_for_reg = self._look_back_and_forward(time_diff_ahead=time_diff_ahead, time_diff_follower= time_diff_follower)
        else:
            wait_for_reg = 0

        return wait_for_reg

    def determine_if_bus_can_start(self, starting_point, time):
        """
        :param starting_point: starting section of the bus for which need to determined when it can start
        :param time: time of the starting bus
        :return: amount of time the bus needs to wait before the bus can start its trip
        """

        stop_behind =starting_point
        hour = int(time/60/60)
        print(hour)
        bus_behind_found = False
        exp_time_to_drive = 0

        #stop behind is the stop for which the functions iteratively checks where the bus behind is
        stop_behind = self.sections[stop_behind]['preceding_sections'][0]
        if stop_behind == 0:
            stop_behind = 1

        # find the bus which arrives first at the starting points and determine the expected travel time to that point
        while not bus_behind_found:
            if self.live_locator[self.sections[stop_behind]['preceding_sections'][0]]['arrival']['vehicle'] is not None:
                time_message_back = None
                if stop_behind == starting_point:
                    bus_behind_found = True
                stop_behind = self.sections[stop_behind]['preceding_sections'][0]
                if self.sections[stop_behind]['halte']:
                    time_segment = self.sections[stop_behind]['dwell_mean_arrival_5']
                else:

                    time_segment = self.sections[stop_behind]['section_mean_' + str(hour)]
                if time_segment is None:
                    time_segment = 30
                exp_time_to_drive += time_segment
                continue
            else:
                if self.live_locator[self.sections[stop_behind]['preceding_sections'][0]]['departure'][
                    'vehicle'] is not None:
                    time_message_back = self.live_locator[self.sections[stop_behind]['preceding_sections'][0]]['departure'][
                        'time']
                    print('section_mean_' + str(hour))
                    print(stop_behind)
                    time_segment = self.sections[str(int(stop_behind) + 1)]['section_mean_' + str(hour)]
                    if time_segment is not None:
                        exp_time_to_drive += time_segment
                    else:
                        exp_time_to_drive += 60
                    bus_behind_found = True
                else:
                    time_message_back = self.live_locator[self.sections[stop_behind]['preceding_sections'][0]]['arrival'][
                        'time']
                    exp_time_to_drive += 30
                    bus_behind_found = True

        if time_message_back is None:
            time_diff_back_bus = None
        else:
            time_diff_last_message_now = time - time_message_back
            time_diff_back_bus = exp_time_to_drive - time_diff_last_message_now

        return time_diff_back_bus


    def calculate_time_difference_between_busses(self, arrival_id, vehicle, arrival_time):

        """
        :param arrival_id: section id where the relevant vehicle arrives
        :param vehicle: relevant vehicle that for which the difference between its follower and aheading bus is
        :param arrival_time: the moment the relevant vehicle arrives at the arrival_id
        :return: time_diff_next_bus, time_diff_back_bus: time difference between next bus and back bus
        """

        departure_bus_ahead = self.live_locator[arrival_id]['departure']['time']
        bus_ahead = self.live_locator[arrival_id]['departure']['vehicle']
        bus_behind_found = False
        stop_behind = arrival_id
        time_message_back = None

        # find the last message with location and time of the bus on the back
        while bus_behind_found == False:
            #print(self.live_locator[self.sections[stop_behind]['preceding_sections'][0]]['arrival']['vehicle'])
            if self.live_locator[self.sections[stop_behind]['preceding_sections'][0]]['arrival']['vehicle'] == None:
                time_message_back = None
                bus_behind_found = True
                continue
            if vehicle != self.live_locator[self.sections[stop_behind]['preceding_sections'][0]]['arrival']['vehicle']:
                if vehicle != self.live_locator[self.sections[stop_behind]['preceding_sections'][0]]['departure']['vehicle']:
                    time_message_back = self.live_locator[self.sections[stop_behind]['preceding_sections'][0]]['departure']['time']
                else:
                    time_message_back = self.live_locator[self.sections[stop_behind]['preceding_sections'][0]]['arrival']['time']
                bus_behind_found = True
            else:
                stop_behind = self.sections[stop_behind]['preceding_sections'][0]

        last_location_bus_ahead_found = False
        bus_stop = arrival_id
        time_message_forward = None

        # find the last message with location and time of the bus ahead
        while not last_location_bus_ahead_found:
            if bus_ahead is None:
                # if next bus is None no bus is in front of you or the bus ahead is at bus stop variable
                last_location_bus_ahead_found = True
            else:
                #check if the bus ahead arrived at bus_top
                if bus_ahead == self.live_locator[self.sections[bus_stop]['subsequent_sections'][0]]['arrival']['vehicle']:
                    #check if bus ahead departure at bus stop
                    if bus_ahead == self.live_locator[self.sections[bus_stop]['subsequent_sections'][0]]['departure']['vehicle']:
                        time_message_forward = self.live_locator[self.sections[bus_stop]['subsequent_sections'][0]]['departure']['time']
                        bus_stop = self.sections[bus_stop]['subsequent_sections'][0]
                    else:
                        #if bus ahead has not yet left bus stop we found the last location
                        time_message_forward = self.live_locator[self.sections[bus_stop]['subsequent_sections'][0]]['arrival']['time']
                        last_location_bus_ahead_found = True
                else:
                    # if bus ahead has not arrived at bus stop we found the last location
                    last_location_bus_ahead_found = True

        if time_message_forward is None:
            time_diff_next_bus = None
            # if the bus ahead has not yet arrived at a subsequent bus stop
        else:

            time_diff_last_message_now = arrival_time - time_message_forward
            exp_trav_time_ahead = time_message_forward - departure_bus_ahead
            time_diff_next_bus = exp_trav_time_ahead + time_diff_last_message_now

        if time_message_back is None:
            time_diff_back_bus = None
        else:
            time_stop_to_you = self.live_locator[stop_behind]['departure']['time_last_vehicle']
            time_diff_last_message_now = arrival_time - time_message_back
            exp_trav_time_back = arrival_time - time_stop_to_you
            time_diff_back_bus = exp_trav_time_back - time_diff_last_message_now
        return time_diff_next_bus, time_diff_back_bus








