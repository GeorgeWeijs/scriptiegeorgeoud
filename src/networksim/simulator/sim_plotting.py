import matplotlib.pyplot as plt
import os
import time
import collections
import imageio
import textwrap
from src.utils import utilities


def plot_simu(env, start_plot, until, trains, nw_sections, sec_rec, ended_trip_mgr, blockage_mnr,
              retrigger_event_mgr, plot_folder, network_focus, giffify=False):

    plot_sub_path = os.path.join(plot_folder, 'simu_plots')
    utilities.create_if_needed(plot_sub_path)

    # set some variables based on network_focus
    shift_parameter = 0
    if network_focus == 'CLF':
        # if CLF only, all text should be shifted to still be visile
        shift_parameter = 31

    # determine the wrap parameter for the terminated train list as maximum x value minus the start point of the text
    term_wrap = 240
    if network_focus == 'all':
        term_wrap = 590

    prev_timeunit = -1
    iterator = 0
    timestamps = []  # a list of times between frames
    plot_names = []  # a list of plot names
    pre_trains_locs = []
    while env.peek() < until:
        if env.now < start_plot:
            env.step()
            continue
        if env.now > prev_timeunit:
            trains_locs = [(train.where_are_you()) for train in trains]
            if trains_locs != pre_trains_locs:
                plt_skeleton = plot_sections(nw_sections, plot_folder, sec_rec, network_focus=network_focus,
                                             save=False, open_plot=False, return_plt=True)
                # add train locations
                for train in trains:
                    coord_to_plot, direction = train.where_are_you()
                    if coord_to_plot:
                        # train text
                        if train.direction == 'from_westmead':
                            plt.plot(coord_to_plot[0], coord_to_plot[1], 'k>', markersize=2)
                        else:
                            plt.plot(coord_to_plot[0], coord_to_plot[1], 'k<', markersize=2)

                        if coord_to_plot[1] >= 2:  # plot train details above it if on the top part of the track
                            plt.text(coord_to_plot[0], coord_to_plot[1]+1,
                                     '{}({:+.2f})'.format(train.name, train.behind_schedule), fontsize=4, ha='center',
                                     va='center')
                            plt.text(coord_to_plot[0], coord_to_plot[1] + 1.8,
                                     '{}'.format([x['id'] for x in train.sections_claimed]), fontsize=3, ha='center',
                                     va='center')
                            if train.current_trip:
                                plt.text(coord_to_plot[0], coord_to_plot[1] + 2.3,
                                         'trip: {}'.format(train.current_trip), fontsize=4, ha='center',
                                         va='center')
                        else:
                            plt.text(coord_to_plot[0], coord_to_plot[1]-1.3,
                                     '{}({:+.2f})'.format(train.name, train.behind_schedule), fontsize=4, ha='center',
                                     va='center')
                            plt.text(coord_to_plot[0], coord_to_plot[1] - 1.8,
                                     '{}'.format([x['id'] for x in train.sections_claimed]), fontsize=3, ha='center',
                                     va='center')
                            if train.current_trip:
                                plt.text(coord_to_plot[0], coord_to_plot[1] - 2.3,
                                         'trip: {}'.format(train.current_trip), fontsize=4, ha='center',
                                         va='center')

                # add timing information
                plt.text(-1 + shift_parameter, -5, '{}'.format(env.now))
                plt.text(-1 + shift_parameter, -7, '{}'.format(time.strftime("%H:%M:%S", time.gmtime(env.now))))

                # add terminated train information
                endedtrips_text = 'terminated:'
                for prnt_pce in textwrap.wrap('{}'.format(ended_trip_mgr.trip_ended_details['plot_id']),
                                              term_wrap):
                    endedtrips_text += '\n' + prnt_pce
                plt.text(1 + shift_parameter, -5, endedtrips_text, fontsize=3, va='top')

                # add blocked info in rows of 9
                blck_text = 'blocked information:'
                shift_count = 1
                counter = 0
                for prnt_id, print_data in blockage_mnr.sec_block_count.items():
                    if counter % 9 == 0 and counter > 0:
                        plt.text(shift_count + shift_parameter, -10, blck_text, fontsize=3, va='top')
                        shift_count += 5
                        blck_text = ''
                    blck_text += '\n{}: {}'.format(prnt_id, print_data)
                    counter += 1
                plt.text(shift_count + shift_parameter, -10, blck_text, fontsize=3, va='top')

                # add retrigger info
                retrig_txt = 'retrigger information: \nWaiting for retrigger'
                for print_data in retrigger_event_mgr.retrigger_event_chain:
                    retrig_txt += '\n{}'.format(print_data[0])
                retrig_txt += '\nfirst_train retrigger: '
                if retrigger_event_mgr.section1066_retrigger_event is not None:
                    retrig_txt += '\n1066'
                plt.text(0 + shift_parameter, -5, retrig_txt, fontsize=3, va='top')

                # name, save, cleanup and set variables for next iteration
                plot_name = "{0:0>7}.png".format(iterator)
                plt_skeleton.savefig(os.path.join(plot_sub_path, plot_name))
                plt.close()
                iterator += 1
                pre_trains_locs = trains_locs
                timestamps.append(env.now - prev_timeunit)
                plot_names.append(plot_name)
                prev_timeunit = env.now
        env.step()

    if giffify:
        giffify_fn(plot_folder, plot_names, plot_sub_path, timestamps)


def giffify_fn(output_folder, plot_names, load_plot_folder, timestamps):
    """
    Function that converts the produced single images into a gif with time intervals between the images
    equal to timestamps
    """
    gif_folder = os.path.join(output_folder, 'gifs')
    utilities.create_if_needed(gif_folder)
    now_epoch = int(time.time())
    gif_name = '{}.gif'.format(now_epoch)
    gif_name_fast = '{}_fast.gif'.format(now_epoch)

    with imageio.get_writer(os.path.join(gif_folder, gif_name), mode='I',
                            duration=[x/10 for x in timestamps[1:]]) as writer:
        with imageio.get_writer(os.path.join(gif_folder, gif_name_fast), mode='I',
                                duration=[x/100 for x in timestamps[1:]]) as writer_fast:
            for plotname in plot_names:
                filepath = os.path.join(load_plot_folder, plotname)
                image = imageio.imread(filepath)
                writer.append_data(image)
                writer_fast.append_data(image)
                print('Added {}'.format(plotname))


def plot_sections(nw_sections, output_path, sec_rec, network_focus, save=True, open_plot=True, return_plt=False):

    def _start_plot():
        # helper function to open a new figure
        # plt.figure(figsize=[800 / WIDTH_FACTOR, HEIGHT_FACTOR]).patch.set_facecolor('white')
        if network_focus == 'all':
            start_fig = plt.figure(figsize=(40, 4), dpi=320)
        else:
            start_fig = plt.figure(figsize=(10, 4), dpi=320)  # Westmead or Carlingford only setting
        return start_fig

    def _close_plot():
        # helper function to save, possibly open, and close figure
        plt.margins(0)
        plt.ylim(-15, 15)
        plt.xlim(min([min(x['coordinates_xs'][0], x['coordinates_xs'][1]) for x in nw_sections.values()]),
                 max([max(x['coordinates_xs'][0], x['coordinates_xs'][1]) for x in nw_sections.values()]))
        for spine in plt.gca().spines.values():
            spine.set_visible(False)
        plt.tick_params(top='off', bottom='off', left='off', right='off', labelleft='off', labelbottom='off')
        plt.xlabel('')
        plt.ylabel(' ')

        if save:
            print('sections plot saved')
            utilities.create_if_needed(output_path)
            plt.savefig(os.path.join(output_path, 'sections_plot'))
        if open_plot:
            plt.show()
        if not return_plt:
            plt.close()

    # init plot
    fig_handle = _start_plot()

    # determine not fully claimed sections
    resource_id_available_count = collections.Counter([x['id'] for x in sec_rec.items])
    partly_claimed_resources = [y[0] for y in resource_id_available_count.items() if
                                y[1] < nw_sections[y[0]]['max_capacity']]
    # note fully claimed resources are not present in the sec_rec

    # add stations to plot
    for section_id, section_data in nw_sections.items():
        if section_data['station_ind']:
            if section_id not in [x['id'] for x in sec_rec.items]:
                # fully claimed since not anymore in sec_rec
                color, linestyle, alpha = 'r', '-', 1
            elif section_id in partly_claimed_resources:
                # partly claimed
                color, linestyle, alpha = 'r', '--', 1
            else:
                # not claimed
                color, linestyle, alpha = '#FAA460', '-', 1

            plt.plot(section_data['coordinates_xs'], section_data['coordinates_ys'], linestyle=linestyle, color=color,
                     linewidth=1, alpha=alpha)
            # also plot the 'platform'
            plt.plot(section_data['coordinates_xs'], [y + 0.75 for y in section_data['coordinates_ys']], linestyle='-',
                     color='#999999', linewidth=1)
            plt.plot(section_data['coordinates_xs'], [y - 0.75 for y in section_data['coordinates_ys']], linestyle='-',
                     color='#999999', linewidth=1)
            if section_data['coordinates_ys'] == [2, 2]:  # only plot station names once (on y=2 row)
                plt.text(section_data['middle_point'][0], section_data['middle_point'][1] + 4,
                         section_data['section_name'].split('__')[-1], fontsize=4, rotation=30, ha='left', va='bottom')

    # add trafficlights to plot
    # for section_id, section_data in nw_sections.items():
    #     if section_data['trafficlight_ind']:
    #         if section_id not in [x['id'] for x in sec_rec.items]:
    #             # fully claimed since not anymore in sec_rec
    #             color, linestyle, alpha = 'r', '-', 1
    #         elif section_id in partly_claimed_resources:
    #             # partly claimed
    #             color, linestyle, alpha = 'r', '--', 1
    #         else:
    #             # not claimed
    #             color, linestyle, alpha = '#0F8903', '-', 1
    #         plt.plot(section_data['coordinates_xs'], section_data['coordinates_ys'], linestyle=linestyle, color=color,
    #                  linewidth=1, alpha=alpha)

    # add other sections to plot
    for section_id, section_data in nw_sections.items():
        # other sections
        if not section_data['station_ind']:
            if section_id not in [x['id'] for x in sec_rec.items]:
                # fully claimed since not anymore in sec_rec
                color, linestyle, marker, alpha = 'r', '-', '|', 1
            elif section_id in partly_claimed_resources:
                # partly claimed
                color, linestyle, marker, alpha = 'r', '--', '|', 1
            else:
                # not claimed
                color, linestyle, marker, alpha = '#1E90FF', '-', '|', 1
            plt.plot(section_data['coordinates_xs'], section_data['coordinates_ys'], marker='|', linestyle=linestyle,
                     color=color, linewidth=1, markersize=2, alpha=alpha)

    # add section ID and speed distribution to plot
    for section_id, section_data in nw_sections.items():
        if section_data['speed_text'] is not None:
            speed_text = section_data['speed_text']
            if section_data['layover'] is not None:
                speed_text += ' + layover of {}'.format(section_data['layover'])
            plt.text(section_data['middle_point'][0], section_data['middle_point'][1] - 0.5,
                     speed_text, fontsize=2, ha='center')
        # else:
        plt.text(section_data['middle_point'][0], section_data['middle_point'][1] + 0.5,
                 section_id, fontsize=2, ha='center', va='top')

    # finalize and save plot or return
    _close_plot()

    return fig_handle


if __name__ == '__main__':
    print('test')
    # sections = utilities.load_json('sections', PREPROCESSED_DATA)
    #
    # env = simpy.Environment()
    # # setup the sequences such that each section of the network is a unique resource for the simulation
    # sections_resource = simpy.FilterStore(env, len(sections.keys()) + 3)
    #
    # for sequence_data in sections.values():
    #     # generate all routes iteratively, each connection between two stations is a route, 1 for each direction
    #     for _ in range(sequence_data['max_capacity']):
    #         sections_resource.put({'id': str(sequence_data['id']), 'section_name': sequence_data['section_name'],
    #                                'max_capacity': sequence_data['max_capacity']})
    #
    # plot_sections(sections, sec_rec=sections_resource, output_path=os.path.join(OUTPUT_DATA, 'sections'))
