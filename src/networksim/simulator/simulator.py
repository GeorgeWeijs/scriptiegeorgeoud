import simpy
import random
import copy
import os
import numpy as np
import pandas as pd
from datetime import datetime
from src.utils import utilities, stochastic_fns, lrv_schedule_fns, post_process_sim
from src.networksim.simulator.sim_plotting import plot_simu
from src.networksim.simulator.sim_logging import SimulationLogger
from src.networksim.simulator.sim_objects import Train, BlockageMgr, EndedTripMgr, RetriggerEventMgr, RegelmaatMgr
from src.utils import kpi_fns


def add_endpoint_layover(sections, endpoint_layover):

    for section_id, section_data in sections.items():
        if section_data['endpoint_ind']:
            sections[section_id]['layover'] = endpoint_layover


def add_chargingpoint_time(sections, charging_time):

    for section_id, section_data in sections.items():
        if section_data['chargingpoint_ind']:
            sections[section_id]['charging_time'] = charging_time


def update_multiple_single_track_settings(sections, multiple_single_track):

    if multiple_single_track:
        scenarios_lst = ['all']
    else:
        scenarios_lst = ['all', 'one_on_single_track']

    for section_id, section_data in sections.items():
        if section_data['related_sections']:
            sections[section_id]['related_sections'] = [str(sec) for sec, scenario in
                                                        section_data['related_sections'].items()
                                                        if scenario in scenarios_lst]


def filter_and_setup_sections_for_network_focus(sections, network_focus):
    # if all, no filter
    if network_focus == 'all':
        return sections
    else:
        sections_filtered = {}
        for section_id, section_data in sections.items():
            # if int(re.findall(r'[-\d]+', section_id)[0]) < 857 or int(re.findall(r'[-\d]+', section_id)[0]) > 1274:
            if section_data['route_focus'] == network_focus:
                sections_filtered[section_id] = section_data

        # now that a subset of sections has been selected, set all subsequent and preceding section references
        # that do not exist anymore to empty lists
        for f_section_id, f_section_data in sections_filtered.items():
            new_subseq = []
            for subsequent_section in f_section_data['subsequent_sections']:
                if subsequent_section in sections_filtered.keys():
                    new_subseq.append(subsequent_section)
            sections_filtered[f_section_id]['subsequent_sections'] = new_subseq

        for f_section_id, f_section_data in sections_filtered.items():
            new_presed = []
            for preceding_section in f_section_data['preceding_sections']:
                if preceding_section in sections_filtered.keys():
                    new_presed.append(preceding_section)
            sections_filtered[f_section_id]['preceding_sections'] = new_presed

    return sections_filtered


def generate_trains(nr_of_trains, env, sections, sec_rec, blockage_mnr, ended_trip_mgr,
                    simulation_logger, tr_avg_interval, train_start_time, retrigger_event_mgr, start_sec_id,
                    expo_variance_at_spawn,regelmaat_mgr):
    trains = []
    train_names = []

    for i in range(nr_of_trains):

        if i%2 ==0:
            #Start at Haarlem
            start_sec_id_specific_train = start_sec_id[0]
        else:
            #Start at Bijlmer
            start_sec_id_specific_train = start_sec_id[1]

        train_name = 'BUS_{}'.format(i)
        train = Train(env=env, name=train_name, current_sec_id=start_sec_id_specific_train, start_time=train_start_time,
                      nw_sections=sections, sec_rec=sec_rec, blockage_mgr=blockage_mnr,
                      ended_trip_mgr=ended_trip_mgr, sim_logger=simulation_logger,
                      retrigger_event_mgr=retrigger_event_mgr,
                      schedule=None, starting_trains=None, regelmaat_mgr=regelmaat_mgr)
        if expo_variance_at_spawn:
            train_start_time += stochastic_fns.get_exp_wait_time_rounded(tr_avg_interval)
        else:
            train_start_time += round(tr_avg_interval)
        trains.append(train)
        train_names.append(train_name)

    return trains, train_names

def generate_trains_scheduled(schedules, env, sections, sec_rec, blockage_mnr, ended_trip_mgr, simulation_logger,
                              retrigger_event_mgr, crossover_after_cfd):
    # determine starting train dict
    starting_trains = {}
    for _, schedule_data in schedules.items():
        train_start_time = schedule_data['lrv_sched'][0]['departure_time']
        departure_sec_id = lrv_schedule_fns.find_start_section(schedule_data['lrv_sched'])
        if departure_sec_id not in starting_trains:
            starting_trains[departure_sec_id] = []
        starting_trains[departure_sec_id].append(train_start_time)
        starting_trains[departure_sec_id].sort()

    trains_scheduled = []
    train_scheduled_names = []
    for train_id, train_schedule_data in schedules.items():
        train_name = 'LRV_{}_{}'.format(train_schedule_data['start_nr'], train_id)
        # use first trip from schedule to find start time (departure time)
        train_start_time = train_schedule_data['lrv_sched'][0]['departure_time']
        departure_sec_id = lrv_schedule_fns.find_start_section(train_schedule_data['lrv_sched'])
        schedule_todo = train_schedule_data['lrv_sched']

        train = Train(env=env, name=train_name, current_sec_id=departure_sec_id, start_time=train_start_time,
                      nw_sections=sections, sec_rec=sec_rec, blockage_mgr=blockage_mnr,
                      ended_trip_mgr=ended_trip_mgr, sim_logger=simulation_logger,
                      retrigger_event_mgr=retrigger_event_mgr, crossover_after_cfd=crossover_after_cfd,
                      schedule=schedule_todo, starting_trains=starting_trains)

        trains_scheduled.append(train)
        train_scheduled_names.append(train_name)

    return trains_scheduled, train_scheduled_names


def create_section_resources(sections, env):

    # setup the sequences such that each section of the network is a unique resource for the simulation
    sections_resource = simpy.FilterStore(env)

    for sequence_data in sections.values():
        # generate all routes iteratively, each connection between two stations is a route, 1 for each direction
        for _ in range(sequence_data['max_capacity']):
            sections_resource.put({'id': str(sequence_data['id']), 'section_name': sequence_data['section_name'],
                                   'max_capacity': sequence_data['max_capacity']})
    return sections_resource


def simulation(sections, random_seed, nr_of_trains, tr_avg_interval, sim_printing,
               plot_simulation, giffify, plot_start_time, plot_folder,
               network_focus, expo_variance_at_spawn, schedules, sim_end_time=23*60*60):
    random.seed(random_seed)
    np.random.seed(random_seed)

    # start with an environment for the simulation
    env = simpy.Environment()

    # create section resources based on sections
    sections_resource = create_section_resources(sections, env)
    # generate a logger object
    simulation_logger = SimulationLogger('sim_logger', any_printing=sim_printing)

    # generate retrigger event manager
    retrigger_event_mgr = RetriggerEventMgr(env, 'retrigger_event_mgr') #niet gebruikt

    # generate a blocker manager and a terminated train manager
    blockage_mnr = BlockageMgr(env, 'blockage_mnr', sec_rec=sections_resource) #niet gebruikt
    ended_trip_mgr = EndedTripMgr('ended_trip_mgr')
    regelmaat_mgr = RegelmaatMgr( env, 'Regelmaat_mgr', sections,regelmaat_mgr_settings = "BAF", desired_interval = 7*60) #"Look Forward"
    # add the permanent blockages
    # def toggle_permanent_block():
    #     for permanent_block in permanent_blocked_tpl:
    #         yield env.process(blockage_mnr.block_or_free('block', permanent_block))
    # env.process(toggle_permanent_block())

    if schedules is None:
        # starting point of trains
        start_sec_id = ['0' , "42"]
        # # generate trains
        trains, train_names = generate_trains(nr_of_trains=nr_of_trains, env=env, sections=sections,
                                              sec_rec=sections_resource, blockage_mnr=blockage_mnr,
                                              ended_trip_mgr=ended_trip_mgr,
                                              simulation_logger=simulation_logger, tr_avg_interval=tr_avg_interval,
                                              train_start_time=6*60*60, retrigger_event_mgr=retrigger_event_mgr,
                                              start_sec_id=start_sec_id,
                                              expo_variance_at_spawn=expo_variance_at_spawn, regelmaat_mgr =regelmaat_mgr)
    else:
        # if schedules provided, use those to generate trains, let them define nr of trains, start times and headway
        trains, train_names = generate_trains_scheduled(schedules=schedules, env=env, sections=sections,
                                                        sec_rec=sections_resource, blockage_mnr=blockage_mnr,
                                                        ended_trip_mgr=ended_trip_mgr,
                                                        simulation_logger=simulation_logger,
                                                        retrigger_event_mgr=retrigger_event_mgr)

    if plot_simulation:
        # call plotting function and fire step by step to create plots
        plot_simu(env=env, start_plot=plot_start_time, until=sim_end_time, trains=trains, nw_sections=sections,
                  sec_rec=sections_resource, ended_trip_mgr=ended_trip_mgr, blockage_mnr=blockage_mnr,
                  retrigger_event_mgr=retrigger_event_mgr, plot_folder=plot_folder, network_focus=network_focus,
                  giffify=giffify)
    else:
        env.run(until=sim_end_time)

    # collect termination information of trains
    ended_trip_details = ended_trip_mgr.trip_ended_details
    train_acts = simulation_logger.train_activities_rows
    station_acts = simulation_logger.station_activivities_rows

    return train_acts, station_acts, ended_trip_details


def simulator(basis_sections, storage_folder, nr_of_simulations=10, nr_of_trains=150, tr_avg_interval=60*7,
              endpoint_layover=60*4, sim_printing=False, plot_simulation=False,
              giffify=False, plot_start_time=0, export_detailed_result=True,
              network_focus='all', expo_variance_at_spawn=True, schedules=None):

    # validate input
    assert network_focus in ['all'], 'Unsupported network focus selected'
    # there are two options: schedules provided for LRVs, or meta parameters like headway and layover
    # check that they are mutually exclusive provided
    if schedules is None:
        assert nr_of_trains is not None and endpoint_layover is not None and tr_avg_interval is not None and \
               expo_variance_at_spawn is not None
    else:
        print('GTFS schedule used, note that headway, layover, nr_of_trains and expo_variance are masked and not used'
              ' also note that the network can only be "all"')
        assert endpoint_layover is None and tr_avg_interval is None and expo_variance_at_spawn is None and \
            network_focus == 'all'

    # call simulation a lot
    start = datetime.now()
    print('Simulating {} scenarios'.format(nr_of_simulations))
    #utilities.print_train()

    # add layover (None in case of provided schedules) and charging times
    add_endpoint_layover(basis_sections, endpoint_layover)
    # add_chargingpoint_time(basis_sections, charging_time)

    # incorporate the multiple_single_track preferences in the sections (at related sections)
    # update_multiple_single_track_settings(basis_sections, multiple_single_track)

    # filter and setup based on network focus and multiple_single_track preference
    sections_in_mem = filter_and_setup_sections_for_network_focus(basis_sections, network_focus)

    # result catcher
    simulator_result_df = pd.DataFrame()

    for i in range(nr_of_simulations):
        output_folder = os.path.join(storage_folder, '{}'.format(i))
        utilities.progress_bar(i / nr_of_simulations)

        # make a new copy for each simulation for safety: make sure sections are not changed during simulation
        sections = copy.deepcopy(sections_in_mem)

        trn_acts, stat_acts, ended_trip_details = simulation(sections, random_seed=i+10, nr_of_trains=nr_of_trains,
                                                             tr_avg_interval=tr_avg_interval, sim_printing=sim_printing,
                                                             plot_simulation=plot_simulation, giffify=giffify,
                                                             plot_folder=output_folder, plot_start_time=plot_start_time,
                                                             network_focus=network_focus,
                                                             expo_variance_at_spawn=expo_variance_at_spawn,
                                                             schedules=schedules)
        # kpi_fns is used to calculate all the perfomance kpi's
        kpi_fns.plot_bus_travelling(sections=sections,trn_acts=trn_acts,plot_sub_folder=output_folder)
        kpi_busses, travel_time = kpi_fns.calculate_bus_performance_kpis(sections,trn_acts)

        # # add the seed
        # kpi_dict['random_seed'] = i
        # # append the kpis to the simulator result df
        # simulator_result_df = simulator_result_df.append(kpi_dict, ignore_index=True)

        # store the activities for each individual simulation
        df_stat_acts = pd.DataFrame(stat_acts)
        df_trip_end = pd.DataFrame(ended_trip_details)
        utilities.save_df_to_csv(df_stat_acts, 'stat_acts', output_folder)
        utilities.save_df_to_csv(df_trip_end, 'trip_end_details', output_folder)
        utilities.save_df_to_csv(kpi_busses, 'kpi_busses', output_folder)
        utilities.save_df_to_csv(travel_time, 'travel_time_busses', output_folder)
        if export_detailed_result:
            # logging of everything, can be enabled for debug reasoning
            df_trn_acts = pd.DataFrame(trn_acts)
            utilities.save_df_to_csv(df_trn_acts, 'trn_acts', output_folder)

    # store the sections json as used for the simulation
    utilities.save_json(sections_in_mem, 'sections_used', storage_folder)

    # store results of simulations combined (nr_of_simulations)
    utilities.save_df_to_csv(simulator_result_df, 'combined_result', storage_folder)

    utilities.progress_bar(1)

    # combine all stat_acts from simulations into run times
    print('\nPost processing results')
    #CODE COMMENTED
    #run_dwell_times = post_process_sim.aggr_section_runtime(input_folder=storage_folder, input_file='stat_acts.csv',
    #                                                        nr_sims=nr_of_simulations)

    #utilities.save_df_to_csv(run_dwell_times, 'run_dwell_times', storage_folder)

    print('--- Simulation ended. (this one) ---')
    print('Simulating {} scenarios took {}'.format(nr_of_simulations, datetime.now() - start))

    return simulator_result_df


if __name__ == '__main__':
    print('simulator called')
