
import pandas as pd
import time
import copy
from src.utils import utilities
from src.settings import OUTPUT_DATA
from src.networksim.parse_input import parse_supporting_data, parse_gtfs_timetable
from src.utils.folder_helper import build_storage_folder
from src.networksim.process import setup_sections, setup_gtfs_schedules
from src.networksim.simulator.simulator import simulator
from src.utils import kpi_fns


def run_script(single=True, use_schedule=False):

    def run_sim_aggr(sections, schedules, gtfs_ext, nr_of_simulations, output_folder=OUTPUT_DATA):

        # charging_time_list = [int(150), 180]
        # charging_time_list = [int(150)]
        # # permanent_blocked_list = [['none', tuple(), False], ['10', ('10',), False]]
        # # permanent_blocked_list = [['none_with_crossover', tuple(), True], ['none_no_crossover', tuple(), False]]
        #                           # ['1066_1', ('1066',), False]]
        # # permanent_blocked_list = [['none_no_crossover', tuple(), False]]
        # permanent_blocked_list = [['no_3rd_wm_no_crossover', ('10',), False]]
        # # multiple_single_track_list = [[True, 'multiple_ok'], [False, 'multiple_notok']]
        # multiple_single_track_list = [[False, 'multiple_notok']]

        if schedules is None:
            assert gtfs_ext is None
            # headway_list = [7.5 * 60, 8 * 60, 10 * 60, 12 * 60]
            headway_list = [3.5* 60]
            layover_list = [5 * 60]
            expo_variance_at_spawn_list = [[False, 'variance_off']]
            # network_focus = 'CLF'
            # network_focus = 'WMD'
            network_focus = 'all'
        else:
            assert gtfs_ext is not None
            # if schedules are given, headway and layover are given, variance is irrelevant. Use all network pieces
            headway_list = [None]
            layover_list = [None]
            expo_variance_at_spawn_list = [[None, 'NA']]
            network_focus = 'all'

        df_results = pd.DataFrame()
        i = 1
        result_fldr = None
        epoch_time = int(time.time())
        print('Starting the simulator aggregator')
        for headway in headway_list:
            for layover in layover_list:
                for expo_variance_at_spawn in expo_variance_at_spawn_list:

                    # make a new copy for each simulator, each simulator will adjust its sections according
                    # to provided parameters, basis set will not change this way
                    sections_copy = copy.deepcopy(sections)

                    # print('---Running simulation {}/{}: headway: {} layover: {}: '
                    #       'blocked: {} {} {} schedules: {}---'
                    #       .format(i, len(headway_list) * len(layover_list) *
                    #               len(expo_variance_at_spawn_list), headway, layover,
                    #               expo_variance_at_spawn[1], gtfs_ext))

                    storage_folder, result_fldr = build_storage_folder(output_folder, 'aggr',
                                                                       epoch_time, network_focus,
                                                                       gtfs_ext, headway, layover,
                                                                       expo_variance_at_spawn[1])

                    simulator_result_df = simulator(basis_sections=sections_copy,
                                                    storage_folder=storage_folder,
                                                    nr_of_simulations=nr_of_simulations,
                                                    nr_of_trains=nr_of_train_in_each_simulation,
                                                    tr_avg_interval=headway, endpoint_layover=layover,
                                                    sim_printing=False, plot_simulation=False,
                                                    giffify=False, export_detailed_result=True,
                                                    network_focus=network_focus,
                                                    expo_variance_at_spawn=expo_variance_at_spawn[0],
                                                    schedules=schedules)

                    # can take average of averages since all simulations have equal amount of trains
                    # kpi_fns aggr is used to calculate all the perfomance kpi's
                    # aggr_kpi_dict = kpi_fns.aggr_performance_kpis(simulator_result_df)
                    # # add scenario variables
                    # aggr_kpi_dict['headway'] = headway
                    # aggr_kpi_dict['layover'] = layover
                    # aggr_kpi_dict['expo_variance_at_spawn'] = expo_variance_at_spawn[1]
                    #
                    # i += 1
                    # df_results = df_results.append(aggr_kpi_dict, ignore_index=True)
                    # # add money paymech calc results
                    # df_results = kpi_fns.add_paymech_results(df_results)

                    # intermediate (and final) results print
                    # print(df_results)

        # store results of sim aggregation including money
        utilities.save_df_to_csv(df_results, 'results', result_fldr)

        print('Finished simulator aggregator')

    def run_sim_single(sections, schedules, gtfs_ext, output_folder=OUTPUT_DATA):

        if schedules is None:
            assert gtfs_ext is None
            headway = int(3.5 * 60)
            layover = int(5 * 60)
            # expo_variance_at_spawn = [True, 'variance_on']
            expo_variance_at_spawn = [False, 'variance_off']
            # network_focus = 'CLF'
            # network_focus = 'WMD'
            network_focus = 'all'
        else:
            assert gtfs_ext is not None
            # if schedules are given, headway and layover are given, variance is irrelevant. Use all network pieces
            headway = None
            layover = None
            expo_variance_at_spawn = [None, 'NA']
            network_focus = 'all'

        epoch_time = int(time.time())

        storage_folder, result_fldr = build_storage_folder(output_folder, 'indi', epoch_time, network_focus,
                                                           gtfs_ext, headway, layover, expo_variance_at_spawn[1])

        simulator_result_ad_hoc = simulator(basis_sections=sections, storage_folder=storage_folder,
                                            nr_of_simulations=1, nr_of_trains=nr_of_train_in_each_simulation,
                                            tr_avg_interval=headway, endpoint_layover=layover, sim_printing=True,
                                            plot_simulation=False, giffify=False, plot_start_time=0,
                                            export_detailed_result=True,
                                            network_focus=network_focus,
                                            expo_variance_at_spawn=expo_variance_at_spawn[0],
                                            schedules=schedules)

    # variable initiationx
    nr_of_train_in_each_simulation = 22
    # start by parsing the supporting data, which loads the data
    input_filename = 'Network Simulator supporting data_all.xlsx'
    parse_supporting_data.parse_supporting_data(input_filename, print_results=False)
    # now setup the basic set of sections (this will use all supporting data)
    basis_sections = setup_sections.setup_sections(input_filename='sections', output_filename='basis_sections')

    lrv_schedules = None
    gtfs_folder = None
    if use_schedule:
        print('skip')
    else:
        # for now, when no schedule planned, the trains do one run each, no continuous running
        # to establish this, section 29 is not connected to section 1
        basis_sections['86']['subsequent_sections'] = ['0']
        basis_sections['0']['preceding_sections'] = ['86']

    # the running of simulations will load the basic sections and lrv_schedules(can be none)
    if single:
        run_sim_single(basis_sections, lrv_schedules, gtfs_ext=gtfs_folder)
    else:
        run_sim_aggr(basis_sections, lrv_schedules, gtfs_ext=gtfs_folder, nr_of_simulations=70)  # weekend

if __name__ == '__main__':
    run_script(single=True, use_schedule=False)



