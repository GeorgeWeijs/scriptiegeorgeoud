import pandas as pd
from src.settings import RAW_DATA, PREPROCESSED_DATA
from src.utils import utilities


def setup_stop_times(stop_times_file, input_folder):

    # load raw network json
    stop_times = utilities.load_json(stop_times_file, input_folder)

    stop_times_new = []
    # logic to add if needed
    for stop_times_dict in stop_times:
        # skip all junction stops
        if 'J' not in stop_times_dict['stop_id'].split("_")[1]:
            stop_times_dict.pop('pickup_type')
            stop_times_dict.pop('drop_off_type')
            stop_times_dict['arrival_time_hr'] = stop_times_dict['arrival_time']
            stop_times_dict['arrival_time'] = sum(x * int(t) for x, t in zip([3600, 60, 1],
                                                                             stop_times_dict['arrival_time'].split(":")))
            stop_times_dict['departure_time_hr'] = stop_times_dict['departure_time']
            stop_times_dict['departure_time'] = sum(x * int(t) for x, t in zip([3600, 60, 1],
                                                                               stop_times_dict['departure_time'].split(":")))
            stop_times_new.append(stop_times_dict)

    return stop_times_new


def setup_trips(trips_file, input_folder):

    # load raw network json
    trips = utilities.load_json(trips_file, input_folder)
    # logic to add if needed
    for trip_dict in trips:
        trip_dict.pop('shape_id')
        trip_dict.pop('route_id')  # bit cleaner, only 1 route anyway

    return trips


def add_planned_headway(stop_times_df):
    """
    Based on stop times, planned headway is calculated for each stop
    For each stop, filter on its departures, and then look at the previous one
    """
    def _transform_termini_track_stop_ids_in_one(stop_id_str):
        if stop_id_str in ['WMS_1', 'WMS_2']:
            return 'WMS_all'
        if stop_id_str in ['CAR_1', 'CAR_2']:
            return 'CAR_all'
        return stop_id_str

    stop_times_df['stop_id_headway'] = stop_times_df['stop_id'].apply(_transform_termini_track_stop_ids_in_one)
    # add planned_headway placeholder
    stop_times_df['planned_departure_headway'] = None
    stop_times_df['planned_arrival_headway'] = None

    # add type of row indicator (can be either arrival, departure or both)
    stop_times_df['type_of_activity'] = 'both'
    stop_times_df.loc[(stop_times_df['arrival_time'] == stop_times_df['departure_time']) &
                  (stop_times_df['stop_sequence'] == '1'), 'type_of_activity'] = 'departure'
    stop_times_df.loc[(stop_times_df['arrival_time'] == stop_times_df['departure_time']) &
                  (stop_times_df['stop_sequence'] != '1'), 'type_of_activity'] = 'arrival'

    # add copy of dep and arr times for 'playing with'
    stop_times_df['arrival_time_copy'] = stop_times_df['arrival_time']
    stop_times_df['departure_time_copy'] = stop_times_df['departure_time']
    stop_times_df.loc[stop_times_df['type_of_activity'] == 'arrival', 'departure_time_copy'] = None
    stop_times_df.loc[stop_times_df['type_of_activity'] == 'departure', 'arrival_time_copy'] = None

    stops_headway_unique = stop_times_df['stop_id_headway'].unique()

    stop_times_incl_planned_headway = pd.DataFrame()

    for stop_headway in stops_headway_unique:
        # filter on this stop_headway
        stop_filtered = stop_times_df[stop_times_df['stop_id_headway'] == stop_headway]

        # sort on time
        stop_filtered_sort = stop_filtered.sort_values(['departure_time'], ascending=True)
        stop_filtered_sort.fillna(method='ffill', inplace=True)  # this will cause all the Nones be overwritten from
        # the previous row, thus last arrival or departure respectively. Essentially it means that for
        # each arrival_time, the row above it will be the previous arrival time, since all departure rows were
        # none and filled with above rows

        # now calculate the departure and arrival planned headway by looking at previous row (which might be values
        # from row far above that
        stop_filtered_sort['planned_departure_headway'] = (stop_filtered_sort['departure_time_copy'] -
                                                           stop_filtered_sort['departure_time_copy'].shift(1))
        stop_filtered_sort['planned_arrival_headway'] = (stop_filtered_sort['arrival_time_copy'] -
                                                         stop_filtered_sort['arrival_time_copy'].shift(1))

        # make sure that arrival headway is not given for departure row, and vice versa
        stop_filtered_sort.loc[stop_filtered_sort['type_of_activity'] == 'arrival', 'planned_departure_headway'] = None
        stop_filtered_sort.loc[stop_filtered_sort['type_of_activity'] == 'departure', 'planned_arrival_headway'] = None

        stop_times_incl_planned_headway = stop_times_incl_planned_headway.append(stop_filtered_sort)

    stop_times_incl_planned_headway = stop_times_incl_planned_headway.drop(['stop_id_headway', 'departure_time_copy',
                                                                            'arrival_time_copy'], axis=1)
    return stop_times_incl_planned_headway


def setup_gtfs_schedules(gtfs_extension, filter_s_id='PAR_VEH-Weekday-11-PARRAMATTA-1111100-',
                         input_folder=RAW_DATA, output_folder=PREPROCESSED_DATA):

    print('Setting up schedules based on {}'.format(gtfs_extension))

    # load_stop times and trips
    stop_times = setup_stop_times('stop_times_{}'.format(gtfs_extension), input_folder)
    trips = setup_trips('trips_{}'.format(gtfs_extension), input_folder)

    stop_times_df_pre = pd.DataFrame(stop_times)
    trips_df = pd.DataFrame(trips)

    # # filter on weekday or weekends
    trips_df = trips_df[trips_df['service_id'] == filter_s_id]
    uniquetrips = trips_df['trip_id'].unique()
    stop_times_df_pre = stop_times_df_pre[stop_times_df_pre['trip_id'].isin(uniquetrips)]

    # add planned headway figures to each departure based on timetable
    stop_times_df = add_planned_headway(stop_times_df_pre)

    # combine
    stop_times_trips = stop_times_df.merge(trips_df)

    # create an empty dict to hold the final structure (which will be a schedule per lrv)
    lrv_schedules = {}

    # for each block_id (lrv) add all stops and trips
    for row_number, data in stop_times_trips.iterrows():
        row_data = data.to_dict()
        if row_data['block_id'] not in lrv_schedules.keys():
            lrv_schedules[row_data['block_id']] = {'start_nr': None, 'lrv_sched': []}
        lrv_schedules[row_data['block_id']]['lrv_sched'].append(row_data)

    # sort them on time
    for block_id, schedule_data in lrv_schedules.items():
        schedule_data['lrv_sched'].sort(key=lambda x: x['arrival_time'])

    # add start number to all LRV's based on first departure time
    b_id_dep_time_lst = [(block_id, sched['lrv_sched'][0]['departure_time']) for block_id, sched in lrv_schedules.items()]
    b_id_dep_time_lst.sort(key=lambda x: x[1])
    i = 0
    for block_id, dep_time in b_id_dep_time_lst:
        lrv_schedules[block_id]['start_nr'] = i
        i += 1

    # find peak trips (trips that start between 07:00-09:00 or between 16:00 - 19:00)
    for block_id, schedule_data in lrv_schedules.items():
        last_check_trip = ''
        peak = False
        for stop in schedule_data['lrv_sched']:
            # add the details to the dict based on first trip occurance (sorted)
            if stop['trip_id'] != last_check_trip:
                peak = False
                if (25200 <= stop['departure_time'] < 32400) or (57600 <= stop['departure_time'] < 68400):
                    peak = True
                last_check_trip = stop['trip_id']
            stop['peak'] = peak

    utilities.save_json(lrv_schedules, 'lrv_schedules_{}'.format(gtfs_extension), output_folder)
    return lrv_schedules


if __name__ == '__main__':
    # gtfs_folder = 'GTFS_Parramatta_20180328'
    gtfs_folder = 'GTFS_Parramatta_20180406_10LRV_draft1'
    filter_s_id = '10LVHblk-Weekday-0-PARRAMATTA-1111100-'
    setup_gtfs_schedules(gtfs_folder, filter_s_id)
