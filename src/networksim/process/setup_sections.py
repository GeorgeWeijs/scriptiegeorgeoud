import numpy as np
from src.settings import RAW_DATA, PREPROCESSED_DATA
from src.utils import utilities


def _add_section_coordinates(sections):
    # construct middle points of sections
    for section_id, section_data in sections.items():
        sections[section_id]['middle_point'] = [sum(sections[section_id]['coordinates_xs'])/2,
                                                sum(sections[section_id]['coordinates_ys'])/2]


def _add_section_speed_text(sections):
    # add speed text to plot for sections (can be nothing)
    for section_id, section_data in sections.items():

        speed_text = '{}:{},{} {}'.format(section_data['distri'][0], round(section_data['section_mean'], 3),
                                          round(section_data['section_std'], 3), section_data['unit_of_distri'])

        # show nothing only if only 1 subsequent section, 1 predecessor and have the same speed
        if len(sections[section_id]['subsequent_sections']) == 1 and \
                len(sections[section_id]['preceding_sections']) == 1:
                subseqsec = sections[sections[section_id]['subsequent_sections'][0]]
                precsec = sections[sections[section_id]['preceding_sections'][0]]

                if section_data['section_mean'] == subseqsec['section_mean'] == precsec['section_mean'] and \
                        section_data['section_std'] == subseqsec['section_std'] == precsec['section_std'] and \
                        section_data['unit_of_distri'] == subseqsec['unit_of_distri'] == precsec['unit_of_distri']:
                    speed_text = None

        sections[section_id]['speed_text'] = speed_text


def construct_corridor_section_distributions(corridor_distributions, section_corridors):

    corridor_section_distribution = {}

    for corridor_id, corridor_section_list in section_corridors.items():
        # nr of sections in this corridor
        nr_sec_cor = len(corridor_section_list)

        # get distribution of whole corridor
        sec_cor_dist = corridor_distributions[corridor_id]
        # calculate associated distribution for each subsection (divide mean by n and std by sqrt(n))
        # https://en.wikipedia.org/wiki/Variance look for Bienaym formula
        # note minimum is also spread
        if sec_cor_dist['unit_of_distri'] == 'sec':
            sec_cor_dist['section_mean'] = round(sec_cor_dist['section_mean']/nr_sec_cor, 3)
            sec_cor_dist['section_std'] = round(sec_cor_dist['section_std']/np.sqrt(nr_sec_cor), 3)
            sec_cor_dist['minimum'] = round(sec_cor_dist['minimum']/nr_sec_cor, 3)
        # for now, if distri provided in ms, no changes for now.

        # add for each section in the corridor, an entry with the new distribution to the final dict
        for section in corridor_section_list:
            corridor_section_distribution[section] = sec_cor_dist

    return corridor_section_distribution


def setup_related_sections(rel_sections_file, input_folder=RAW_DATA):

    # load raw network json
    rel_sections = utilities.load_json(rel_sections_file, input_folder)
    # logic to later add if needed
    for rel_section_id, rel_section_data in rel_sections.items():
        rel_sections[rel_section_id] = {rel_sec_data_item['related_id']: rel_sec_data_item['scenario']
                                        for rel_sec_data_item in rel_section_data}
        # rel_sections[rel_section_id] = [str(x) for x in rel_section_data]

    return rel_sections


def setup_blockages(blockages_file, input_folder=RAW_DATA):

    # load raw network json
    blockages = utilities.load_json(blockages_file, input_folder)
    # logic to add if needed
    for blockage_id, blockage_data in blockages.items():
        blockages[blockage_id] = {section_blockorfree['id_to_block_or_free']: section_blockorfree['block_or_free']
                                  for section_blockorfree in blockage_data}

    return blockages


def setup_section_distribution(section_distribution_file, input_folder=RAW_DATA):

    # load raw network json
    section_distribution = utilities.load_json(section_distribution_file, input_folder)
    # logic to add if needed
    for section_distribution_id, section_distribution_data in section_distribution.items():
        section_distribution[section_distribution_id].pop("id")
        section_distribution[section_distribution_id].pop("section_name")

    return section_distribution


def setup_custom_locations(custom_location_file, input_folder=RAW_DATA):

    # load raw network json
    custom_locations = utilities.load_json(custom_location_file, input_folder)
    # logic to add if needed
    for custom_location_id, custom_loc_data in custom_locations.items():
        custom_locations[custom_location_id]['coordinates_ys'] = [custom_loc_data['y_from'], custom_loc_data['y_to']]
        custom_locations[custom_location_id]['coordinates_xs'] = [custom_loc_data['x_from'], custom_loc_data['x_to']]
        custom_locations[custom_location_id].pop('x_from')
        custom_locations[custom_location_id].pop('x_to')
        custom_locations[custom_location_id].pop('y_from')
        custom_locations[custom_location_id].pop('y_to')
        custom_locations[custom_location_id].pop('section_id')
    return custom_locations


def setup_subsequent_sections(subsequent_section_file, input_folder=RAW_DATA):

    # load raw network json
    subsequent_sections = utilities.load_json(subsequent_section_file, input_folder)
    # logic to add if needed
    for subsequent_sections_id, subsequent_sections_data in subsequent_sections.items():
        subsequent_sections[subsequent_sections_id] = [str(x) for x in subsequent_sections_data]

    return subsequent_sections


def setup_preceding_sections(preceding_section_file, input_folder=RAW_DATA):

    # load raw network json
    preceding_sections = utilities.load_json(preceding_section_file, input_folder)
    # logic to add if needed
    for preceding_sections_id, preceding_sections_data in preceding_sections.items():
        preceding_sections[preceding_sections_id] = [str(x) for x in preceding_sections_data]

    return preceding_sections


def setup_retrigger_events(retrigger_events_file, input_folder=RAW_DATA):

    # load raw network json
    retrigger_events = utilities.load_json(retrigger_events_file, input_folder)
    # logic to add if needed
    for retrigger_event_id, retrigger_events_data in retrigger_events.items():
        retrigger_events[retrigger_event_id] = [str(x) for x in retrigger_events_data]

    return retrigger_events


def setup_section_corridors(section_corridors_file, input_folder=RAW_DATA):

    # load raw network json
    section_corridors = utilities.load_json(section_corridors_file, input_folder)
    # logic to add if needed
    for section_corridor_id, section_corridor_data in section_corridors.items():
        section_corridors[section_corridor_id] = [str(x) for x in section_corridor_data]

    return section_corridors


def setup_corridor_distributions(corridor_distributions_file, input_folder=RAW_DATA):

    # load raw network json
    corridor_distributions = utilities.load_json(corridor_distributions_file, input_folder)
    # logic to add if needed
    for corridor_distribution_id, corridor_distribution_data in corridor_distributions.items():
        corridor_distributions[corridor_distribution_id].pop("id")

    return corridor_distributions


def setup_gtfs_stop_map(gtfs_stop_map_file, input_folder=RAW_DATA):

    # load raw network json
    gtfs_stop_map = utilities.load_json(gtfs_stop_map_file, input_folder)
    # logic to add if needed

    return gtfs_stop_map


def inverse_gtfs_stop_map(gtfs_stop_map):
    """
    The section to gtfs stop map has been added to the section,
    This function also creates the inverse
    """

    inverted_gtfs_stop_map_dict = {}
    for section_id, list_of_gtfs_stops in gtfs_stop_map.items():
        for gtfs_stop in list_of_gtfs_stops:
            if gtfs_stop in inverted_gtfs_stop_map_dict:
                inverted_gtfs_stop_map_dict[gtfs_stop].append(section_id)
            else:
                inverted_gtfs_stop_map_dict[gtfs_stop] = []
                inverted_gtfs_stop_map_dict[gtfs_stop].append(section_id)

    return inverted_gtfs_stop_map_dict


def setup_sections(input_filename, output_filename, input_folder=RAW_DATA, output_folder=PREPROCESSED_DATA):

    print('Setting up basis sections')

    # load raw network json
    sections = utilities.load_json(input_filename, input_folder)

    # add section distribution
    section_distribution = setup_section_distribution('section_distributions', input_folder)  # the non corridor ones
    section_corridors = setup_section_corridors('section_corridors', input_folder)
    corridor_distributions = setup_corridor_distributions('corridor_distributions', input_folder)

    # use the corridor distributions and the corridor settings to calculate the distribution for each corridor section
    corridor_section_distribution = construct_corridor_section_distributions(corridor_distributions, section_corridors)

    assert len(set(section_distribution.keys()).intersection(set(corridor_section_distribution.keys()))) == 0, \
        'sections found in both the section distribution & the corridor distribution list, should be mutually exclusive'

    # add all other information to add to the sections: related, blockage, locs, retrigger, prec and sub information
    related_sections = setup_related_sections('related_sections', input_folder)
    blockages = setup_blockages('blockages', input_folder)
    custom_locations = setup_custom_locations('custom_locations', input_folder)
    retrigger_events = setup_retrigger_events('retrigger_event', input_folder)
    preceding_sections = setup_preceding_sections('preceding_sections', input_folder)
    subsequent_sections = setup_subsequent_sections('subsequent_sections', input_folder)
    gtfs_stop_map = setup_gtfs_stop_map('gtfs_stop_map', input_folder)

    # determine all unique sections that wait for a retrigger
    sections_that_wait_for_retrigger = set([re for re_s in retrigger_events.values() for re in re_s])

    for section_id, section_data in sections.items():
        # =========== SUBSEQUENT =========
        if section_id in subsequent_sections:
            # if subsequent given, take it, otherwise construct with +1 rule
            sections[section_id]['subsequent_sections'] = subsequent_sections[section_id]
        else:
            # add the subsequent sections to each section (for now its only 1)
            sections[section_id]['subsequent_sections'] = []
            # if the next ID number exists, add that to the possible next section list
            if str(section_data['id']+1) in sections.keys():
                sections[section_id]['subsequent_sections'].append(str(section_data['id']+1))

        # =========== PRECEDING  ==========
        if section_id in preceding_sections:
            # if previous given, take it, otherwise construct with -1 rule
            sections[section_id]['preceding_sections'] = preceding_sections[section_id]
        else:
            # add the previous sections to each section (for now its only 1)
            sections[section_id]['preceding_sections'] = []
            # if the next ID number exists, add that to the possible next section list
            if str(section_data['id']-1) in sections.keys():
                sections[section_id]['preceding_sections'].append(str(section_data['id']-1))

        # make all ids now strings
        sections[section_id]['id'] = str(sections[section_id]['id'])

        # add section distributions
        if section_id in section_distribution:
            sections[section_id].update(section_distribution[section_id])
            sections[section_id]['corridor_name'] = None
            assert section_id not in corridor_section_distribution, \
                'corridor and non corridor distribution mutually exclusive '

        if section_id in corridor_section_distribution:
            sections[section_id].update(corridor_section_distribution[section_id])
            assert section_id not in section_distribution, \
                'corridor and non corridor distribution mutually exclusive '

        # add custom locations
        #sections[section_id].update(custom_locations[section_id])

        # add related section
        sections[section_id]['related_sections'] = {}
        if section_id in related_sections.keys():
            sections[section_id]['related_sections'] = related_sections[section_id]

        # add blockage info
        blockage = None
        if section_id in blockages.keys():
            blockage = {}
            blockage.update(blockages[section_id])
        sections[section_id]['blockage'] = blockage

        # retrigger information
        if section_id in sections_that_wait_for_retrigger:
            sections[section_id]['need_retrigger_if_no_candidate'] = True
        else:
            sections[section_id]['need_retrigger_if_no_candidate'] = False
        if section_id in retrigger_events:
            sections[section_id]['retrigger_events_if_freed'] = retrigger_events[section_id]
        else:
            sections[section_id]['retrigger_events_if_freed'] = []

        # add layover placeholder
        sections[section_id]['layover'] = None

        # add charging time placeholder
        sections[section_id]['charging_time'] = None

        # add gtfs_stop_mapping information to relevant section
        gtfs_map = None
        if section_id in gtfs_stop_map.keys():
            gtfs_map = gtfs_stop_map[section_id]
        sections[section_id]['gtfs_map'] = gtfs_map

    # determine the coordinates of each section (add this to the section dict)
    #_add_section_coordinates(sections)
    #_add_section_speed_text(sections)

    # also convert the gtfs_stop_map into its inverse, needed if gtfs schedules are used later
    #inverted_gtfs_stop_map = inverse_gtfs_stop_map(gtfs_stop_map)

    utilities.save_json(sections, output_filename, output_folder)
    #utilities.save_json(inverted_gtfs_stop_map, 'inverted_gtfs_stop_map', output_folder)
    return sections


if __name__ == '__main__':
    setup_sections('sections', 'basis_sections')
    # setup_single_track_sets('single_track_sets')
