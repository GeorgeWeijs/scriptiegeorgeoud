import os
import pandas as pd
import numpy as np
from src.settings import INPUT_DATA,RAW_DATA, PREPROCESSED_DATA
from src.travel_time_from_kv6.RD_to_WGS import RDWGSConverter
import math


### -- settings -- ###
pd.set_option('display.max_rows', 500)
pd.set_option('display.max_columns', 500)
pd.set_option('display.width', 1000)


'''
Script where the data is processed


'''

def kv6_with_traveltime(file):

    KV6_file = os.path.join(RAW_DATA, 'kv6', 'kv6_subset_20181001.csv')
    #KV6_file = os.path.join(RAW_DATA, 'kv6', '{}'.format(file))
    df_kv6 = pd.read_csv(KV6_file, sep=',')

    # sort for shift
    # update 20190528: NTIMESTAMP is not enough! NMSGSEQNR is also needed!
    df_kv6.sort_values(['DDAG', 'NTRIPNR', 'NTIMESTAMP', 'NMSGSEQNR'], ascending=True, inplace=True)

    # get the timestamp from the row above to calculate the traveltime
    df_kv6['travel_time'] = df_kv6['NTIMESTAMP'] - df_kv6['NTIMESTAMP'].shift(periods=1)

    # set the travel time to zero when the day OR trip_id changes
    condition = (df_kv6['DDAG'].shift(periods=1) != df_kv6['DDAG']) | \
                (df_kv6['NTRIPNR'].shift(periods=1) != df_kv6['NTRIPNR']) # new trip condition
    df_kv6.loc[condition, 'travel_time'] = np.NaN

    # ## store and check ##
    # filename = os.path.join(PREPROCESSED_DATA, 'dump', 'kv6_sorted_with_travel_time.csv')
    # KV6_with_coords.to_csv(filename)

    return df_kv6


def load_gtfs_table(table):
    '''
    input: table name
    output: df_table

    770655 = start Haarlem
    772483 = start Bijlmer Arena

    '''
    table_name = os.path.join(RAW_DATA, 'gtfs', '2_gtfs_lijn_300', '{}.txt'.format(table))
    df_table = pd.read_csv(table_name, sep=',')

    if table == 'shapes':
        # check start
        condition = (df_table['shape_id'] == 772483) & (df_table['shape_pt_sequence'] == 1)
        print('First location of the shape_id 772483')
        print(df_table.loc[condition])

    return df_table

def map_the_stops_to_shapes(shape_ids):
    '''
    The GTFS data is connected: shapes <-(shape_id)-> trips <-(trip_id)-> stoptimes <-(shape_dist_traveled)-> shapes

    The goal is to map the stops to the shapes.txt data.
    1. Filter the trips based on the shape_id. This results in multiple trips, select 1 (doesnt matter which)
    2. collect all the shape_sit_traveled in the trips table that are used for the selected trip
    3. map these shape_dist_traveled to shapes.txt to original table


    Shape ID of interest
    770655 = start Haarlem
    772483 = start Bijlmer Arena

    '''

    df_trips = load_gtfs_table('trips')
    df_stop_times = load_gtfs_table('stop_times')
    df_shapes = load_gtfs_table('shapes')

    df_shapes_enriched = pd.DataFrame()

    # >> hier loop shape id
    for shape_id in shape_ids:

        # select 1 trip_id that matches the shape_id (doesnt matter which)
        trip_id = df_trips.loc[df_trips['shape_id'] == shape_id]['trip_id'].iloc[0]

        # select all the shape_dist_traveled from the stop_times (these are the distances traveld at each stop)
        drop_columns = ['trip_id', 'stop_headsign', 'departure_time', 'pickup_type',
         'drop_off_type', 'fare_units_traveled']
        df_stop_info = df_stop_times.loc[df_stop_times['trip_id'] == trip_id].drop(drop_columns, axis=1).add_prefix('trip_')
        df_stop_info.rename({'trip_shape_dist_traveled': 'shape_dist_traveled'}, axis='columns', inplace=True)

        # filter shape_id
        df_shapes_filt = df_shapes.loc[df_shapes['shape_id'] == shape_id].copy()

        df_shapes_enriched_wstops = df_shapes_filt.merge(df_stop_info, how='left', on='shape_dist_traveled')
        df_shapes_enriched = df_shapes_enriched.append(df_shapes_enriched_wstops, sort=False)

    # print(df_shapes_enriched.head())
    # print(df_shapes_enriched.nunique())

    return df_shapes_enriched



def kv6_enrich_segments(file, store=True):

    '''
    Segment assignment with travel time:
    kv6 data is enriched with segments and travel time is assigned to the specific segments. The results look like:
    segment:                                        travel_time
    Dwell - Haarlem, Station H                      17
    0.0 - 500.0 - Haarlem, Station H                28
    0.0 - 500.0 - Haarlem, Station H                43.31
    500.0 - 1000.0 - Haarlem, Station H             12.68
    500.0 - 1000.0 - Haarlem, Station H             41.63
    1000.0 - Haarlem, Centrum/Verwulft              15.36
    1000.0 - Haarlem, Centrum/Verwulft              15

    Process the data:
    In the process of assigning segments many irregularites occur. To overcome the following steps are taken:

    - The last few rows before a stop all fall within the last segment. Iteration is done to check and change
    - The messages span a larger area than the segment length. Duplication of rows to overcome. Also multiple segments
        can be skipped.
    - For the duplicated rows, the travel time is divided by the newly created relative distances per segment
    - After the duplication the travel time is distributed when distances cover two segments proportionally
    - A correction is applied when no distance is covered but travel time has passed
    - A correction is applied when double arrival messages occur

    etc
    :return:
    shape_with_stops dataframe
    kv6 dataframe enriched

    '''

    # load kv6
    df_kv6 = kv6_with_traveltime(file)

    # Get the enriched shape files (with the stops included)
    shape_ids = [770655, 772483]
    df_shapes_enriched = map_the_stops_to_shapes(shape_ids)

    ### Enrich both files with stop_code/stop_id and stopname ###
    mapping = os.path.join(RAW_DATA, 'gtfs', 'stopid_stopcode_stopname_lijn300.csv')
    df_mapping = pd.read_csv(mapping, sep=',')

    # add stopcode and stop_name to shapes (convert to float for matching)
    df_mapping['stop_id'] = df_mapping['stop_id'].astype(np.float64, errors='ignore')
        # cannot convert to int64 as there are nan values...not possible!!
        # df_shapes_enriched['trip_stop_id'] = df_shapes_enriched['trip_stop_id'].astype(np.int64, errors='ignore')
    df_shapes_with_stopnames = df_shapes_enriched.merge(df_mapping, how='left', left_on='trip_stop_id', right_on='stop_id')
    # add flag for stop or not:
    def flag_stop(row):
        if np.isnan(row['trip_timepoint']) == True:
            return 0
        return 1
    df_shapes_with_stopnames['flag_stop'] = df_shapes_with_stopnames.apply(lambda row: flag_stop(row), axis=1)

    # add stop_id and stop_name to kv6 (convert to float for matching)
    df_mapping['stop_code'] = df_mapping['stop_code'].astype(np.float64, errors='ignore')

    df_kv6_enriched = df_kv6.merge(df_mapping, how='left', left_on='NBUSSTOPCODE',
                                                        right_on='stop_code')
    # add direction to KV6 (even is 1, uneven is 0)
    df_kv6_enriched['direction'] = np.where(df_kv6_enriched['NTRIPNR'] % 2 == 1, 0, 1)
    kv6 = df_kv6_enriched.copy()

    # define dwelltime / on route travel time ( see excel example )
    condition = (kv6['MessageType'].isin(['DEPARTURE','ONSTOP']))
    kv6['travel_or_dwell'] = np.where(condition == True, 'Dwell', 'Traveltime')
    kv6['travel_time_org'] = kv6['travel_time']
    kv6['lower_bound'] = kv6['NDISTANCESINCELASTSTOP'].apply(lambda x: math.floor(x / 500.0) * 500.0)
    kv6.sort_values(by=['DDAG', 'NTRIPNR', 'NMSGSEQNR'], inplace=True)  # The order is not sorted correctly before..


    '''
    ##########################################################################
    ##                                                                      ##
    ##                          Add segments                                ##
    ##                                                                      ##
    ##########################################################################
     
    A:
    Add the segments, for example: 0-500; 500-1000; 1000-stop
    To find when 1000-stop should be applied:
    
    1. if the current row value != 0.0   AND
    2. if the next value == 0.0
    ------------------------------------
    then: current row value - stop
    else: current row value - current value + 500
    
    shift(periods=-1) is the next row (periods=1 is the previous)
    '''

    c1 = kv6['travel_or_dwell'] == 'Dwell'
    kv6.loc[c1, 'segment'] = kv6['travel_or_dwell']
    kv6.loc[c1, 'segment_long'] = kv6['travel_or_dwell'] + ' - ' + kv6['stop_name']

    # 26/05/2019 update: some cases occur when there is onroute departure message without arrival, lowerbound can be 0..
    c2 = (kv6['travel_or_dwell'] == 'Traveltime') & (kv6['NDISTANCESINCELASTSTOP'] > 0) & (kv6['lower_bound'].shift(periods=-1) == 0.0)
    # c2 = (kv6['travel_or_dwell'] == 'Traveltime') & (kv6['lower_bound'] != 0.0) & (kv6['lower_bound'].shift(periods=-1) == 0.0)
    kv6.loc[c2, 'segment'] = kv6['lower_bound'].astype(str) + ' - stop'
    kv6.loc[c2, 'segment_long'] = kv6['lower_bound'].astype(str) + ' - ' +  kv6['stop_name'].shift(-1)


    c3 = (kv6['travel_or_dwell'] == 'Traveltime') & (kv6['NDISTANCESINCELASTSTOP']>0) & (kv6['NDISTANCESINCELASTSTOP'].shift(periods=-1) != 0)
    kv6.loc[c3, 'segment'] = kv6['lower_bound'].astype(str) + ' - ' + (kv6['lower_bound'] + 500).astype(str)
    kv6.loc[c3, 'segment_long'] = kv6['lower_bound'].astype(str) + ' - ' + (kv6['lower_bound'] + 500).astype(str) + ' - ' + kv6['stop_name']


    ### AANNAME ###
    # The arrival row has NDISTANCESINCELASTSTOP=0. The travel time should be allocated to: 1000 - stop. This should be added to the segment logic.
    # When arrival message occurs, the previous segment should be allocated
    c4 = (kv6['MessageType'] == 'ARRIVAL')
    kv6.loc[c4, 'segment'] = kv6['segment'].shift(1)
    kv6.loc[c4, 'segment_long'] = kv6['segment_long'].shift(1)

    '''
    ##########################################################################
    ##                                                                      ##
    ##              Segment to stop has improper assignment                 ##
    ##                                                                      ##
    ##########################################################################
    
    500 - 1000 AND 500 to stop shouldn't be both segments...
    
    NDISTANCESINCELASTSTOP  MessageType     travel_time     distance_covered   stop_name                    lower_bound     segment
    620	                    ONROUTE	        28	            405	               Amsterdam, Bijlmer ArenA		500	            500.0 - 1000.0
    877	                    ONROUTE	        29	            257	               Amsterdam, Bijlmer ArenA		500	            500.0 - stop
    0	                    ARRIVAL	        0	            -877	           Amsterdam, Holterbergweg		0	            500.0 - stop

    Solution:
    
    condition 1: next segment has "stop" in the name
    condition 2: lowerbound == lowerbound next row
    condition 3: lowerbound is not 0
    condition 4: the segment does not contain the word stop (thus, has a distance to distance segment)
    
    then: apply segment of the next row to the current row
    
    >>> Multiple instances can occur that need to be changed
    This means iterations are needed to 'update' the row above
    The iterations are done until there are no more cases where: c1 & c2 & c3 & c4 are valid

    '''

    ## Initialize ##
    c1 = kv6['segment'].shift(-1).str.contains('stop')
    c2 = kv6['lower_bound'] == kv6['lower_bound'].shift(-1)
    # c3 = kv6['lower_bound'] != 0
    c3 = kv6['segment'] != 'Dwell'
    c4 = kv6['segment'].str.contains('stop') == False

    i = 0
    print(len(kv6[c1 & c2 & c3 & c4]))
    while len(kv6[c1 & c2 & c3 & c4]) > 0:

        i += 1
        c1 = kv6['segment'].shift(-1).str.contains('stop')
        c2 = kv6['lower_bound'] == kv6['lower_bound'].shift(-1)
        # c3 = kv6['lower_bound'] != 0
        c3 = kv6['segment'] != 'Dwell'
        c4 = kv6['segment'].str.contains('stop') == False
        print(kv6[c1 & c2 & c3 & c4].head(10))

        kv6.loc[c1 & c2 & c3 & c4, 'segment'] = kv6['segment'].shift(-1)
        kv6.loc[c1 & c2 & c3 & c4, 'segment_long'] = kv6['segment_long'].shift(-1)

        print(len(kv6[c1 & c2 & c3 & c4]))

        if i > 200:
            print('unreasonable number of iterations')
            break

    print('all segments fixed')

    '''
    ##########################################################################
    ##                                                                      ##
    ##              Duplication of rows for extra segments                  ##
    ##                                                                      ##
    ##########################################################################    
        
    The data is not clean in structure. The following section tackles irregularities in the data and other challenges

    A. Between two messages more than 2 segments are crossed (i.e. 957m to 1510m: 500-1000; 1000-1500; 1500 - 2000) now
    all the travel time is assigned to 500-1000 and 1500-2000)
    B. Double arrival message
    
    A: Meerdere segmenten
    Het kan voorkomen dat er meer dan 2 segmenten worden aangedaan tussen de onroute messages.
            Distance                                                Segment             Calc traveltime     Traveltime                            
    3:21	957	    ONROUTE	    De Hoek, De Hoek	Traveltime	    500.0 - 1000.0	            	        32
    3:22	1504	ONROUTE	    De Hoek, De Hoek	Traveltime	    1500.0 - 2000.0	    	        

    Voor de rijtijd van 32 seconden moet als volgt worden onderverdeeld:
    
            Distance                                                Segment             Calc traveltime     Traveltime                            
    3:21	957	    ONROUTE	    De Hoek, De Hoek	Traveltime	    500.0 - 1000.0	    2.515539305	        32
    3:21	957	    ONROUTE	    De Hoek, De Hoek	Traveltime	    1000.0 - 1500.0	    29.25045704	        
    3:22	1504	ONROUTE	    De Hoek, De Hoek	Traveltime	    1500.0 - 2000.0	    0.234003656	        

    '''
    # preparation for duplication of rows
    kv6['flag_duplicated_row'] = 0
    print('length of KV6: {}'.format(len(kv6)))

    def _copy_rows_based_on_condition(df, condition):

        df['distance_covered'] = df['NDISTANCESINCELASTSTOP'] - df['NDISTANCESINCELASTSTOP'].shift(1)
        duplicate_rows = df.loc[condition].copy()

        duplicate_rows['lower_bound'] = df['lower_bound'] + 500
        duplicate_rows['segment'] = (df['lower_bound'] + 500).astype(str) + ' - ' + (df['lower_bound'] + 1000).astype(str)
        duplicate_rows['segment_long'] = (df['lower_bound'] + 500).astype(str) + ' - ' + (df['lower_bound'] + 1000).astype(str) + ' - ' + df['stop_name']

        # Set to NaN for now!
        duplicate_rows[['travel_time', 'NDISTANCESINCELASTSTOP', 'travel_time_org']] = np.NaN
        duplicate_rows['NDISTANCESINCELASTSTOP'] = df['lower_bound'] + 1000
        duplicate_rows['flag_duplicated_row'] = 1

        print('length of duplicate_rows: {}'.format(len(duplicate_rows)))
        kv6 = df.append(duplicate_rows, ignore_index=True)
        print('lenght of kv6 appended: {}'.format(len(kv6)))
        kv6.sort_values(by=['DDAG', 'NTRIPNR', 'NMSGSEQNR'], inplace=True) # Of NTIMESTAMP
        # print(kv6[['NKV6ID', 'NDISTANCESINCELASTSTOP', 'MessageType', 'travel_time', 'stop_name', 'travel_or_dwell',
        #            'lower_bound', 'segment', 'segment_long', 'flag_duplicated_row', 'distance_covered']].head(50))


        kv6['travel_time_new'] = kv6['travel_time']
        # A - Current row: split the travel time between the current row and the duplicated row
        c_current = kv6['flag_duplicated_row'].shift(1) == 1 # the previous row is duplicated
        # (afstand / totale afstand ) * originele reistijd
        kv6.loc[c_current, 'travel_time_new'] =  ((kv6['NDISTANCESINCELASTSTOP'] - kv6['NDISTANCESINCELASTSTOP'].shift(1)) / kv6['distance_covered']) * kv6['travel_time']

        # B - Duplicated row: split the travel time between the current row and the duplicated row
        c_duplicated = kv6['flag_duplicated_row'] == 1 # the row is duplicated
        # (afstand / totale afstand ) * originele reistijd

        kv6.loc[c_duplicated, 'travel_time_new'] =  ((kv6['NDISTANCESINCELASTSTOP'] - kv6['NDISTANCESINCELASTSTOP'].shift(1)) / kv6['distance_covered'].shift(-1)) * kv6['travel_time'].shift(-1)

        # print(kv6[['NKV6ID', 'NDISTANCESINCELASTSTOP', 'MessageType', 'travel_time', 'travel_time_new', 'distance_covered', 'stop_name', 'travel_or_dwell',
        #            'lower_bound', 'segment', 'flag_duplicated_row']].head(50))

        kv6['travel_time'] = kv6['travel_time_new']
        kv6 = kv6.drop(['travel_time_new'], axis=1)

        return kv6

    # condition: The difference between the two rows is between 501 and 1000 (skipping 1 segment)
    c1 = (kv6['lower_bound'].shift(-1) - kv6['lower_bound'] > 500) & (kv6['lower_bound'].shift(-1) - kv6['lower_bound'] < 1001)
    # print(kv6[c1].head(10))


    kv6 = _copy_rows_based_on_condition(kv6, c1)
    kv6.reset_index(drop=True, inplace=True)


    def _double_copy_rows_based_on_condition(df, condition):
        df['distance_covered'] = df['NDISTANCESINCELASTSTOP'] - df['NDISTANCESINCELASTSTOP'].shift(1)

        duplicate_rows_part1 = df.loc[condition].copy()
        duplicate_rows_part1['NDISTANCESINCELASTSTOP'] = duplicate_rows_part1['lower_bound'] + 1000

        duplicate_rows_part1['lower_bound'] = duplicate_rows_part1['lower_bound'] + 500
        duplicate_rows_part1['segment'] = (duplicate_rows_part1['lower_bound']).astype(str) + ' - ' + (duplicate_rows_part1['lower_bound'] + 500).astype(str)
        duplicate_rows_part1['segment_long'] = (duplicate_rows_part1['lower_bound']).astype(str) + ' - ' + (duplicate_rows_part1['lower_bound'] + 500).astype(str) + ' - ' + duplicate_rows_part1['stop_name']


        duplicate_rows_part2 = df.loc[condition].copy()
        duplicate_rows_part2['NDISTANCESINCELASTSTOP'] = duplicate_rows_part2['lower_bound'] + 1500

        duplicate_rows_part2['lower_bound'] = duplicate_rows_part2['lower_bound'] + 1000
        duplicate_rows_part2['segment'] = (duplicate_rows_part2['lower_bound']).astype(str) + ' - ' + (duplicate_rows_part2['lower_bound'] + 500).astype(str)
        duplicate_rows_part2['segment_long'] = (duplicate_rows_part2['lower_bound']).astype(str) + ' - ' + (duplicate_rows_part2['lower_bound'] + 500).astype(str) + ' - ' + duplicate_rows_part2['stop_name']


        duplicate_rows = duplicate_rows_part1.append(duplicate_rows_part2)
        duplicate_rows[['travel_time','travel_time_org']] = np.NaN
        duplicate_rows['flag_duplicated_row'] = 1
        duplicate_rows['flag_skipped_2'] = 1

        duplicate_rows.sort_values(by=['DDAG', 'NTRIPNR', 'NMSGSEQNR'], inplace=True)  # Of NTIMESTAMP

        print('length of duplicate_rows: {}'.format(len(duplicate_rows)))
        kv6 = df.append(duplicate_rows, ignore_index=True, sort=False)
        print('lenght of kv6 appended: {}'.format(len(kv6)))
        kv6.sort_values(by=['DDAG', 'NTRIPNR', 'NMSGSEQNR'], inplace=True)  # Of NTIMESTAMP
        # print(kv6[['NKV6ID', 'NDISTANCESINCELASTSTOP', 'MessageType', 'travel_time', 'stop_name', 'travel_or_dwell',
        #            'lower_bound', 'segment', 'segment_long', 'flag_duplicated_row', 'distance_covered']][5855:5865])


        '''
        Assign the travel time to the new rows and existing
        (afstand / totale afstand ) * originele reistijd
        '''

        kv6['travel_time_new'] = kv6['travel_time']
        # A - Current row: split the travel time between three rows and assign to current
        c_current = (kv6['flag_duplicated_row'].shift(1) == 1) & (kv6['flag_duplicated_row'].shift(2) == 1) # the previous 2 rows are duplicated
        kv6.loc[c_current, 'travel_time_new'] = ((kv6['NDISTANCESINCELASTSTOP'] - kv6['NDISTANCESINCELASTSTOP'].shift(1)) / kv6['distance_covered']) * kv6['travel_time']

        # B - First duplicated row: split the travel time between three rows and assign to first duplicated
        c_duplicated_first = (kv6['flag_duplicated_row'] == 1) & (kv6['flag_duplicated_row'].shift(1) == 1) # the current row and previous row are duplicated
        kv6.loc[c_duplicated_first, 'travel_time_new'] =  ((kv6['NDISTANCESINCELASTSTOP'] - kv6['NDISTANCESINCELASTSTOP'].shift(1)) / kv6['distance_covered'].shift(-1)) * kv6['travel_time'].shift(-1)

        # C - Second duplicated row: split the travel time between three rows and assign to second duplicated
        c_duplicated_second = (kv6['flag_duplicated_row'] == 1) & (kv6['flag_duplicated_row'].shift(-1) == 1) # the current row and next row are duplicated
        kv6.loc[c_duplicated_second, 'travel_time_new'] =  ((kv6['NDISTANCESINCELASTSTOP'] - kv6['NDISTANCESINCELASTSTOP'].shift(1)) / kv6['distance_covered'].shift(-2)) * kv6['travel_time'].shift(-2)

        # print(kv6[['NKV6ID', 'NDISTANCESINCELASTSTOP', 'MessageType', 'travel_time', 'travel_time_new', 'stop_name', 'travel_or_dwell',
        #            'lower_bound', 'segment', 'segment_long', 'flag_duplicated_row', 'distance_covered']][5855:5865])

        kv6['travel_time'] = kv6['travel_time_new']
        kv6 = kv6.drop(['travel_time_new'], axis=1)

        return kv6

    # condition: The difference between the two rows is between 1001 and 1500 (skipping 2 segment)
    c2 = (kv6['lower_bound'].shift(-1) - kv6['lower_bound'] > 1000) & (kv6['lower_bound'].shift(-1) - kv6['lower_bound'] < 1501)
    kv6 = _double_copy_rows_based_on_condition(kv6, c2)
    kv6.reset_index(drop=True, inplace=True)


    # ''' 4 gevallen met 3 segmenten geskipped --> 3 ervan travel time gevallen nu ignore..'''
    # c3 = (kv6['lower_bound'].shift(-1) - kv6['lower_bound'] > 1500)
    # kv6.loc[c3, 'flag_skip_segment'] = 3
    #
    # # De gevallen
    # print(kv6[kv6['flag_skip_segment'] == 3][:10])
    #
    # # Dwell time geval
    # print(kv6[32350:32354][['NKV6ID', 'NDISTANCESINCELASTSTOP', 'MessageType', 'travel_time', 'distance_covered', 'stop_name', 'travel_or_dwell',
    #                'lower_bound', 'segment', 'flag_duplicated_row', 'flag_skip_segment']])
    # # Travel time example
    # print(kv6[33818:33822][['NKV6ID', 'NDISTANCESINCELASTSTOP', 'MessageType', 'travel_time', 'distance_covered', 'stop_name', 'travel_or_dwell',
    #                'lower_bound', 'segment', 'flag_duplicated_row', 'flag_skip_segment']])


    '''
    ##########################################################################
    ##                                                                      ##
    ##         Travel time distribution for cross-segment cases             ##
    ##                                                                      ##
    ##########################################################################
    
    Logica toepassen om de reistijd te verdelen over over twee segmenten:
    
    - toepassen wanneer het over "travel time" gaat (c1)
    - toepassen wanneer er een verandering in segmenten komt (c2)
    - de vorige rij mag geen dwell regel zijn.. (c3)
    - de tijd moet verhoudings gewijs (linear) toeworden gedeeld aan de segmenten
    
    NDISTANCES      Traveltime      MessageType     Segment
    395             29              ONROUTE         0 - 500
    593             27              ONROUTE         500 - 1000
    
    '''

    c1 = kv6['travel_or_dwell'] == 'Traveltime'
    c2 = kv6['segment'] != kv6['segment'].shift(1)
    c3 = kv6['travel_or_dwell'].shift(1) != 'Dwell'
    kv6['flag_verdeel_rijtijd'] = 0
    kv6.loc[c1 & c2 & c3, 'flag_verdeel_rijtijd'] = 1

    kv6.loc[kv6['flag_verdeel_rijtijd'] == 1, 'distance_current'] = kv6['NDISTANCESINCELASTSTOP'] - kv6['lower_bound']
    kv6.loc[kv6['flag_verdeel_rijtijd'] == 1, 'distance_previous'] = kv6['lower_bound'] - kv6['NDISTANCESINCELASTSTOP'].shift(1)

    kv6['%distance_current'] = kv6['distance_current'] / (kv6['distance_current'] + kv6['distance_previous'])
    kv6['%distance_previous'] = kv6['distance_previous'] / (kv6['distance_current'] + kv6['distance_previous'])

    '''
    ##########################################################################
    ##                                                                      ##
    ##      Fix for buses that stand still and cover no distance            ##
    ##                                                                      ##
    ##########################################################################
    '''

    # print(kv6[kv6['%distance_current'] == np.inf].head(10))
    c1 = kv6['%distance_current'] == -np.inf
    c2 = kv6['%distance_current'] == np.inf
    kv6.loc[c1 | c2, 'flag_inf'] = 1
    # print(kv6[kv6['flag_inf']==1].head(50))

    # replace the infinite values --> becomes 0, so assignemnt of traveltime will be zero
    kv6['%distance_current'] = kv6['%distance_current'].replace(-np.inf, 0).replace(np.inf, 0)
    kv6['%distance_previous'] = kv6['%distance_previous'].replace(-np.inf, 0).replace(np.inf, 0)

    # divide the travel time in (a) base and (b) division column:
    # The extra condition for flag_inf makes sure that when no distance is covered AND there is a segment change the travel time gets assigned properly
    kv6.loc[(kv6['flag_verdeel_rijtijd'] == 0) | (kv6['flag_inf']==1), 'traveltime_base'] = kv6['travel_time']
    kv6.loc[kv6['flag_verdeel_rijtijd'] == 1, 'travel_time_previous!'] = kv6['travel_time'] * kv6['%distance_previous']
    kv6.loc[kv6['flag_verdeel_rijtijd'] == 1, 'traveltime_division'] = kv6['travel_time'] * kv6['%distance_current']
    kv6['total'] = kv6['traveltime_base'].fillna(0) + kv6['traveltime_division'].fillna(0) + kv6['travel_time_previous!'].shift(-1).fillna(0)
    kv6['identifier'] = kv6['travel_or_dwell'] + kv6['stop_id'].astype('str')

    # #cases where there is no distance covered on route
    # c1 = kv6['NDISTANCESINCELASTSTOP'] == kv6['NDISTANCESINCELASTSTOP'].shift(1)
    # c2 = kv6['travel_or_dwell'] == 'Traveltime'
    # print(kv6[c1 & c2].head(10))

    ''' ----- B: double arrival message: update segment, segment_long, total -----'''
    # condition 1: both the current as the row above should be ARRIVAL
    c1 = (kv6['MessageType'] == 'ARRIVAL') & (kv6['MessageType'].shift(1) == 'ARRIVAL')
    kv6.loc[c1, 'segment'] = kv6['segment'].shift(1)
    kv6.loc[c1, 'segment_long'] = kv6['segment_long'].shift(1)
    kv6.loc[c1, 'total'] = kv6['travel_time']

    # Final check dataframe:
    # print(kv6[['NKV6ID', 'NDISTANCESINCELASTSTOP',  'MessageType', 'travel_time',
    #            'distance_covered', 'stop_name', 'travel_or_dwell', 'lower_bound', 'segment',
    #            'flag_duplicated_row', 'flag_verdeel_rijtijd', 'total', 'traveltime_base', 'traveltime_division',
    #            'travel_time_previous!', 'distance_current', 'distance_previous', '%distance_current', '%distance_previous']][395:405])

    # kv6 = kv6.drop(['%distance_current', '%distance_previous', 'traveltime_base', 'travel_time_previous!', 'traveltime_division'], axis=1)

    '''
    ##########################################################################
    ##                                                                      ##
    ##              Post filter: sometimes wrong Ndistances.                ##
    ##              Segments with negative values get removed               ##
    ##                                                                      ##
    ##########################################################################   
    '''

    c1 = kv6['total'] < 0
    remove = pd.concat([kv6[c1]['DDAG'], kv6[c1]['NTRIPNR'], kv6[c1]['segment_long']], axis=1)

    print('{} instances of negative values - remove the segments of those specific trips'.format(len(remove)))

    #option1:
    remove['flag_remove'] = 1
    combined = pd.merge(kv6, remove, on=['DDAG', 'NTRIPNR', 'segment_long'], how='left')
    # keep where there is no flag
    kv6_filtered = combined[pd.isnull(combined['flag_remove'])][kv6.columns]
    print(kv6_filtered[2505:2520])






    if store:
        # ''' ----- store and check ----- '''
        # filename = os.path.join(PREPROCESSED_DATA, 'shapes', 'shape_with_stopnames.csv')
        # df_shapes_with_stopnames.to_csv(filename)

        filename = os.path.join(PREPROCESSED_DATA, 'kv6_enriched', file.strip('.csv')+'_enriched.csv')
        kv6_filtered.to_csv(filename, index=False)

        # # validate specific trip numbers
        # ntripnr = [17]
        # validation = kv6_filtered.loc[(kv6_filtered['NTRIPNR'].isin(ntripnr)) & (kv6_filtered['DDAG'] == '2018-10-04')]
        # filename = os.path.join(PREPROCESSED_DATA, 'kv6_validation', 'kv6_trip_{}.csv'.format(ntripnr[0]))
        # validation.to_csv(filename)

    return df_shapes_with_stopnames, kv6_filtered


def calculate_travel_time_per_segment():
    pick_up_csv = False
    file = "kv6_20190426_171040.csv"

    if pick_up_csv:
        KV6_file = os.path.join(PREPROCESSED_DATA, 'kv6_enriched', file.strip('.csv')+'_enriched.csv')
        kv6 = pd.read_csv(KV6_file, sep=',')
    else:
        df_shapes_with_stopnames, kv6 = kv6_enrich_segments(file, store=True)



    # calculate travel time per trip:
    # print(kv6[(kv6['NTRIPNR'] == 1) & (kv6['DDAG'] == '2018-10-02')].head())
    kv6_per_trip = kv6.groupby(['NTRIPNR', 'DDAG'])['travel_time_org', 'total'].sum().reset_index()
    kv6_per_trip['compare'] = kv6_per_trip['travel_time_org'].round(2) == kv6_per_trip['total'].round(2)
    filename = os.path.join(PREPROCESSED_DATA, 'kv6_validation', file.strip('.csv')+'_per_trip.csv')
    kv6_per_trip.to_csv(filename)
    print('Trips with different total travel time')
    print(kv6_per_trip[~kv6_per_trip["compare"]]['NTRIPNR'])


    # group by per segment
    kv6_per_segment_per_trip = kv6.groupby(['DDAG', 'direction', 'NTRIPNR', 'segment_long', 'segment'])['total'].sum().reset_index()
    kv6_per_segment = kv6_per_segment_per_trip.groupby(['DDAG', 'direction', 'segment_long', 'segment'])['total'].agg(['mean', 'count']).reset_index()

    kv6_per_segment_per_trip['stopname'] = kv6_per_segment_per_trip['segment_long'].str.split('-').str[-1].str.lstrip()
    kv6_per_segment_per_trip['lowerbound'] = kv6_per_segment_per_trip['segment_long'].str.split('-').str[0]
    kv6_per_segment_per_trip['sort_column'] = kv6_per_segment_per_trip['lowerbound'].str.strip()
    kv6_per_segment_per_trip.loc[kv6_per_segment_per_trip['sort_column']=='Dwell', 'sort_column'] = -1
    kv6_per_segment_per_trip['sort_column'] = kv6_per_segment_per_trip['sort_column'].astype(float)
    kv6_per_segment_per_trip['segment2'] = kv6_per_segment_per_trip['segment_long'].str.split('-').str[0:2].str.join(' - ')
    kv6_per_segment_per_trip['key'] = kv6_per_segment_per_trip['direction'].astype(str) + '_' + kv6_per_segment_per_trip['stopname'].astype(str)


    kv6_per_segment['stopname'] = kv6_per_segment['segment_long'].str.split('-').str[-1].str.lstrip()
    kv6_per_segment['lowerbound'] = kv6_per_segment['segment_long'].str.split('-').str[0]
    kv6_per_segment['sort_column'] = kv6_per_segment['lowerbound'].str.strip()
    kv6_per_segment.loc[kv6_per_segment['sort_column'] == 'Dwell', 'sort_column'] = -1
    kv6_per_segment['sort_column']=kv6_per_segment['sort_column'].astype(float)
    kv6_per_segment['segment2'] = kv6_per_segment['segment_long'].str.split('-').str[0:2].str.join(' - ')
    kv6_per_segment['key'] = kv6_per_segment['direction'].astype(str) + '_' + kv6_per_segment['stopname'].astype(str)

    df_map_file = os.path.join(RAW_DATA, 'kv6', 'sequence.csv')
    df_map = pd.read_csv(df_map_file, sep=',')
    df_map = df_map[['key', 'sequence']]

    # join the map on the stations
    kv6_segment_trip = pd.merge(kv6_per_segment_per_trip, df_map, on=['key'], how='left').sort_values(by=['direction', 'sequence', 'sort_column'])
    kv6_segment = pd.merge(kv6_per_segment, df_map, on=['key'], how='left').sort_values(by=['direction', 'sequence', 'sort_column'])

    # drop all segments that have less than 40 count
    kv6_segment_result = kv6_segment[kv6_segment['count']>=40]

    filename = os.path.join(PREPROCESSED_DATA, 'kv6_validation', file.strip('.csv')+'_per_segment_per_trip.csv')
    kv6_segment_trip.to_csv(filename)
    filename = os.path.join(PREPROCESSED_DATA, 'kv6_validation', file.strip('.csv')+'_kv6_per_segment.csv')
    kv6_segment_result.to_csv(filename)


if __name__ == "__main__":
    # kv6_enrich_segments()
    calculate_travel_time_per_segment()

    # kv6_with_coords()
    # kv6_points_to_lines()
    # load_shapes()

    # shape_ids = [770655, 772483]
    # map_the_stops_to_shapes(shape_ids)


    #
    # #option2:
    # keys = list(remove.columns.values)
    # i1 = kv6.set_index(keys).index
    # i2 = remove.set_index(keys).index
    # filtered = kv6[~i1.isin(i2)]

