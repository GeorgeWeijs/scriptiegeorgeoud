import os
from src.settings import INPUT_DATA,RAW_DATA

'''
Your fabulous script
'''


# Import necessary tools
import zmq
import gzip
import xml.etree.ElementTree as ET

# ZeroMQ Context
context = zmq.Context()

# Define the socket using the "Context"
sock = context.socket(zmq.SUB)

# Define subscription and messages with prefix to accept.
sock.setsockopt_string(zmq.SUBSCRIBE, "/CXX/KV6posinfo")
sock.connect("tcp://pubsub.besteffort.ndovloket.nl:7658")

while True:
    message = sock.recv()
    try:
        message = gzip.decompress(message)
    except OSError:
        pass

    # convert the message
    message = str(message, 'utf-8-sig')

    # filter the useless messages
    if message == '/CXX/KV6posinfo':
        continue

    # define a root from the XML message
    root = ET.fromstring(message)

    # define a mapping with parents for each child
    parent_map = {c: p for p in root.iter() for c in p}

    # loop through all lineplanningnumber elements (inside of a KV6 message)
    for route_field in root.iter('{http://bison.connekt.nl/tmi8/kv6/msg}lineplanningnumber'):
        # only the route of interest
        if route_field.text in ['M300']:
            # obtain all the fields of the KV6 message
            kv6_message = parent_map[route_field]
            print('\n' + kv6_message.tag)
            for child in kv6_message:
                print(child.tag, child.text)

    # file = open('message_{}.txt'.format(index), "w")
    # file.write(message)
    # file.close()