import os
from src.settings import INPUT_DATA, RAW_DATA
from src.settings import SCHIPHOL_LOCATION_RD

# Import necessary tools
import zmq
import gzip
import xml.etree.ElementTree as ET
import pandas as pd
from src.utils.utilities import create_if_needed
from datetime import datetime

def connect_to_socket():
    # ZeroMQ Context
    context = zmq.Context()

    # Define the socket using the "Context"
    sock = context.socket(zmq.SUB)

    # Define subscription and messages with prefix to accept.
    sock.setsockopt_string(zmq.SUBSCRIBE, "/CXX/KV6posinfo")
    sock.connect("tcp://pubsub.besteffort.ndovloket.nl:7658")

    return sock


def scrape_with_route_filter(routes_list=['M300']):
    """

    :param routes_list:
    :return:
    """

    # connect to the socket
    sock = connect_to_socket()

    while True:
        message = sock.recv()
        try:
            message = gzip.decompress(message)
        except OSError:
            pass

        # convert the message
        message = str(message, 'utf-8-sig')

        # filter the useless messages
        if message == '/CXX/KV6posinfo':
            continue

        # define a root from the XML message
        root = ET.fromstring(message)

        # define a mapping with parents for each child
        parent_map = {c: p for p in root.iter() for c in p}

        # loop through all lineplanningnumber elements (inside of a KV6 message)
        for route_field in root.iter('{http://bison.connekt.nl/tmi8/kv6/msg}lineplanningnumber'):
            # only the route of interest
            if route_field.text in routes_list:
                # obtain all the fields of the KV6 message
                kv6_message = parent_map[route_field]
                print('\n' + kv6_message.tag)
                for child in kv6_message:
                    print(child.tag, child.text)
                return parent_map

        # file = open('message_{}.txt'.format(index), "w")
        # file.write(message)
        # file.close()

def scrape_with_location_filter(location, buffer_km):
    """

    :param location: [X,Y]: location coordinates in Rijksdriehoek projection
    :param buffer_km:
    :return:
    """

    buffer_m = 1000*buffer_km

    # find the bounds
    x_min = location[0] - buffer_m
    x_max = location[0] + buffer_m
    y_min = location[1] - buffer_m
    y_max = location[1] + buffer_m

    # connect to the socket
    sock = connect_to_socket()

    # initialize an empty data frame
    kv6_df = pd.DataFrame()

    # output folder
    output_folder = os.path.join(RAW_DATA, 'kv6')
    create_if_needed(output_folder)

    while True:
        # if there a threshold number of messages in the kv6 data frame, store it
        if len(kv6_df) > 100:
            now = datetime.now()
            output_file_path = os.path.join(output_folder, 'kv6_{}.csv'.format(now.strftime('%Y%m%d_%H%M%S')))
            kv6_df.to_csv(output_file_path, index=False)
            print('..stored data into {}'.format(output_file_path))

            # initialize new data frame
            kv6_df = pd.DataFrame()

        # get the kv6 feed from the web socket
        message = sock.recv()
        try:
            message = gzip.decompress(message)
        except OSError:
            pass

        # convert the message
        message = str(message, 'utf-8-sig')

        # filter the useless messages
        if message == '/CXX/KV6posinfo':
            continue

        # define a root from the XML message
        root = ET.fromstring(message)

        # define a mapping with parents for each child
        parent_map = {c: p for p in root.iter() for c in p}

        # loop through all rd-x elements (inside of a KV6 message)
        for rdx_field in root.iter('{http://bison.connekt.nl/tmi8/kv6/msg}rd-x'):
            rdx_value = int(rdx_field.text)

            # check x-coordinate near specified location
            if x_min <= rdx_value <= x_max:
                # obtain all the fields of the KV6 message
                kv6_message = parent_map[rdx_field]

                # find the corresponding y-coordinate (todo: is this efficient?)
                for rdy_field in kv6_message.iter('{http://bison.connekt.nl/tmi8/kv6/msg}rd-y'):
                    rdy_value = int(rdy_field.text)

                    # check y-coordinate near specified location
                    if y_min <= rdy_value <= y_max:

                        # store the message in a dictionary
                        kv6_dict = {str(child.tag).replace('{http://bison.connekt.nl/tmi8/kv6/msg}', ''): child.text for child in kv6_message}
                        kv6_dict['message_type'] = str(kv6_message.tag).replace('{http://bison.connekt.nl/tmi8/kv6/msg}', '')

                        # append the information to the kv6 data frame
                        kv6_df = kv6_df.append(pd.DataFrame(kv6_dict, index=[len(kv6_df)]), sort=False)


if __name__ == '__main__':
    # scrape_with_route_filter()
    scrape_with_location_filter(location=SCHIPHOL_LOCATION_RD, buffer_km=1.5)