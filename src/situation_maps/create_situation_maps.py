import os
from src.utils.map_fns import create_basemap, add_map_tiles, add_map_functionalities, add_shape_to_map
from src.settings import SCHIPHOL_LOCATION, RAW_GTFS_DATA, OUTPUT_DATA


def create_map_with_shape(shape_id):
    print('\nCreating map with shape {}'.format(shape_id))

    # create a base map with functionalities
    map, attribution = create_basemap(centre=SCHIPHOL_LOCATION, zoomstart=10)
    map = add_map_tiles(map=map, attribution=attribution)
    map = add_map_functionalities(map=map, drawing=False)

    # add the shape of the Oude Lijn to the map
    map = add_shape_to_map(map=map, gtfs_folder=os.path.join(RAW_GTFS_DATA, '2_gtfs_lijn_300'), shape_id='770655')

    # save (file_information contains information about router 2)
    plot_filename = 'overzicht_lijn_300_geheel.html'
    map.save(os.path.join(OUTPUT_DATA, 'maps', plot_filename))

    return map


if __name__ == "__main__":
    create_map_with_shape(shape_id='772483')