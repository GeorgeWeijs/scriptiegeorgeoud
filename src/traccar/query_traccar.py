from sqlalchemy import create_engine
import pandas as pd
import os
from src.settings import MYSQL_SERVER, MYSQL_PORT, MYSQL_DATABASE_USER, MYSQL_DATABASE_PASSWORD
from src.settings import RAW_DATA


def create_connection(database):
    """
    Establish connection to a given database on the traccar server
    :param database:
    :return:
    """
    engine = create_engine('mysql://{}:{}@{}:{}/{}'.format(MYSQL_DATABASE_USER, MYSQL_DATABASE_PASSWORD, MYSQL_SERVER, MYSQL_PORT, database))

    return engine


def query_full_table(table_name, database='traccar', save=False):
    """
    Query an entire table and return in a Pandas dataframe
    :param table_name:
    :param database
    :return:
    """

    # establish connection first
    engine = create_connection(database=database)
    table_df = pd.read_sql_table(table_name=table_name, con=engine)

    if save:
        output_filepath = os.path.join(RAW_DATA, database, '{}.csv'.format(table_name))
        table_df.to_csv(output_filepath, index=False)

    return table_df


def query_table_sql(sql, database='traccar'):
    """

    :param sql:
    :param database
    :return:
    """

    # establish connection first
    engine = create_connection(database=database)
    sql_result_df = pd.read_sql(sql=sql, con=engine)

    return sql_result_df


if __name__ == "__main__":
    print('Running')

    query_full_table(table_name='positions', database='traccar', save=True)