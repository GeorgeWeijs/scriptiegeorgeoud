import os
from src.settings import INPUT_DATA, RAW_GTFS_DATA
from src.utils.gtfs_utilities import load_gtfs_files, store_gtfs_files


def filter_gtfs_on_date(date):
    """

    :param date:
    :return:
    """

    # specify the input folder
    gtfs_folder = os.path.join(INPUT_DATA, 'gtfs-nl')

    # load the raw files
    agency, calendar_dates, feed_info, routes, shapes, stop_times, stops, transfers, trips = load_gtfs_files(gtfs_folder)

    # create filtered gtfs files
    print('\n.. filtering the gtfs files')
    calendar_dates = calendar_dates[(calendar_dates['date'] == date) & (calendar_dates['exception_type'] == '1')]
    trips = trips[trips['service_id'].isin(calendar_dates['service_id'])]
    shapes = shapes[shapes['shape_id'].isin(trips['shape_id'])]
    stop_times = stop_times[stop_times['trip_id'].isin(trips['trip_id'])]
    stops = stops[stops['stop_id'].isin(stop_times['stop_id'])]    
    routes = routes[routes['route_id'].isin(trips['route_id'])]
    agency = agency[agency['agency_id'].isin(routes['agency_id'])]
    transfers = transfers[
        transfers['from_stop_id'].isin(stops['stop_id']) &
        transfers['to_stop_id'].isin(stops['stop_id']) &
        transfers['from_trip_id'].isin(trips['trip_id']) &
        transfers['to_trip_id'].isin(trips['trip_id'])]

    # specify the output folder
    output_folder = os.path.join(RAW_GTFS_DATA, '1_gtfs-nl_one_day')

    # save the files
    store_gtfs_files(output_folder, agency, calendar_dates, feed_info, routes, shapes, stop_times, stops, transfers, trips)


if __name__ == "__main__":
    filter_gtfs_on_date(date='20190219')