import os
from src.settings import RAW_GTFS_DATA
from src.utils.gtfs_utilities import load_gtfs_files, store_gtfs_files

def filter_gtfs_routes(route_ids):
    """

    :param date:
    :return:
    """

    # specify the input folder
    gtfs_folder = os.path.join(RAW_GTFS_DATA, '1_gtfs-nl_one_day')

    # load the raw files
    agency, calendar_dates, feed_info, routes, shapes, stop_times, stops, transfers, trips = load_gtfs_files(gtfs_folder)

    # create filtered gtfs files
    print('\n.. filtering the gtfs files')
    routes = routes[routes['route_id'].isin(route_ids)]
    agency = agency[agency['agency_id'].isin(routes['agency_id'])]
    trips = trips[trips['route_id'].isin(routes['route_id'])]
    shapes = shapes[shapes['shape_id'].isin(trips['shape_id'])]
    calendar_dates = calendar_dates[calendar_dates['service_id'].isin(trips['service_id'])]
    stop_times = stop_times[stop_times['trip_id'].isin(trips['trip_id'])]
    stops = stops[stops['stop_id'].isin(stop_times['stop_id'])]
    transfers = transfers[
        transfers['from_stop_id'].isin(stops['stop_id']) &
        transfers['to_stop_id'].isin(stops['stop_id']) &
        transfers['from_trip_id'].isin(trips['trip_id']) &
        transfers['to_trip_id'].isin(trips['trip_id'])]

    # specify the output folder
    output_folder = os.path.join(RAW_GTFS_DATA, '2_gtfs_lijn_300')

    # save the files
    store_gtfs_files(output_folder, agency, calendar_dates, feed_info, routes, shapes, stop_times, stops, transfers, trips)


if __name__ == "__main__":
    # 62525, CXX, 300, Haarlem Station - Adam Bijlmer Arena via Airport,,3,77216F,ffffff,http://www.connexxion.nl/dienstregeling/lijn?ID=M300
    route_ids = ['62525']
    filter_gtfs_routes(route_ids=route_ids)