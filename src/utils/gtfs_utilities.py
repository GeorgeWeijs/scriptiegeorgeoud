"""
gtfs utilities
"""

import pandas as pd
import os
from src.utils.utilities import create_if_needed


def load_gtfs_files(gtfs_folder):
    """

    :param gtfs_folder:
    :return:
    """
    print('\n.. loading input files')

    # load the gtfs files from the gtfs folder
    agency = pd.read_csv(os.path.join(gtfs_folder, 'agency.txt'), dtype=str)
    calendar_dates = pd.read_csv(os.path.join(gtfs_folder, 'calendar_dates.txt'), dtype=str)
    feed_info = pd.read_csv(os.path.join(gtfs_folder, 'feed_info.txt'), dtype=str)
    routes = pd.read_csv(os.path.join(gtfs_folder, 'routes.txt'), dtype=str)
    shapes = pd.read_csv(os.path.join(gtfs_folder, 'shapes.txt'), dtype=str)
    stop_times = pd.read_csv(os.path.join(gtfs_folder, 'stop_times.txt'), dtype=str)
    stops = pd.read_csv(os.path.join(gtfs_folder, 'stops.txt'), dtype=str)
    transfers = pd.read_csv(os.path.join(gtfs_folder, 'transfers.txt'), dtype=str)
    trips = pd.read_csv(os.path.join(gtfs_folder, 'trips.txt'), dtype=str)

    return agency, calendar_dates, feed_info, routes, shapes, stop_times, stops, transfers, trips


def store_gtfs_files(output_folder, agency, calendar_dates, feed_info, routes, shapes, stop_times, stops, transfers, trips):
    """

    :param output_folder:
    :param agency:
    :param calendar_dates:
    :param feed_info:
    :param routes:
    :param shapes:
    :param stop_times:
    :param stops:
    :param transfers:
    :param trips:
    :return:
    """
    print('\n.. saving output files')
    create_if_needed(output_folder)

    agency.to_csv(os.path.join(output_folder, 'agency.txt'), index=False)
    calendar_dates.to_csv(os.path.join(output_folder, 'calendar_dates.txt'), index=False)
    feed_info.to_csv(os.path.join(output_folder, 'feed_info.txt'), index=False)
    routes.to_csv(os.path.join(output_folder, 'routes.txt'), index=False)
    shapes.to_csv(os.path.join(output_folder, 'shapes.txt'), index=False)
    stop_times.to_csv(os.path.join(output_folder, 'stop_times.txt'), index=False)
    stops.to_csv(os.path.join(output_folder, 'stops.txt'), index=False)
    transfers.to_csv(os.path.join(output_folder, 'transfers.txt'), index=False)
    trips.to_csv(os.path.join(output_folder, 'trips.txt'), index=False)