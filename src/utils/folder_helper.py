import os


def build_storage_folder(output_folder, indi_or_aggr, time_txt, network_focus, gtfs_ext, headway, layover,
                         expo_variance_at_spawn):

    if gtfs_ext is not None:
        gtfs_ext_txt = gtfs_ext
        prefix = ''
    else:
        assert headway is not None
        assert layover is not None
        assert expo_variance_at_spawn is not None
        gtfs_ext_txt = 'no_schedule'
        prefix = 'h{}_l{}_e{}_'.format(headway, layover, expo_variance_at_spawn)

    output_folder_ad_hoc = os.path.join(output_folder, '{}_sim_{}_{}_{}'.format(indi_or_aggr, time_txt, network_focus,
                                                                                gtfs_ext_txt))

    storage_folder = os.path.join(output_folder_ad_hoc, '{}'.format(prefix))

    return storage_folder, output_folder_ad_hoc
