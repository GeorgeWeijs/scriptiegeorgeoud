from src.utils import utilities
from src.settings import PREPROCESSED_DATA
import copy


def find_start_section(lrv_schedule):
    """
    Function that transforms a lrv schedule into a section start location
    """
    # load inverted gtfs stop map ( to transform gtfs stops to sections)
    inverted_gtfs_stop_map = utilities.load_json('inverted_gtfs_stop_map', PREPROCESSED_DATA)

    first_gtfs_stop_id = lrv_schedule[0]['stop_id']

    # the gtfs schedule specifies the departure_section
    departure_section = inverted_gtfs_stop_map[first_gtfs_stop_id][0]

    return departure_section


def find_trips(lrv_schedules):
    """
    Find all unique trips in all schedules together
    """

    lrv_trips = set()
    for block_id, schedule_data in lrv_schedules.items():
        for stop in schedule_data['lrv_sched']:
            lrv_trips.update([stop['trip_id']])

    return lrv_trips


def calculated_missed_meters_and_remove_trip_stops(train_object):
    """
    Function that lets a train, after it completed in current_trip,
    'shadow travel' to the end of its trip, and calculate the missed kilometers in between
    So it find the next section, add the mileage, and once it hits the next stop in the schedule
    remove it until all stops from the schedule for this trip are completed.
    We made the assumption that each trip ends with an arriving schedule stop, which will be counted
    after which no more stops for this trip are included. The first assert is to check this assumption
    and make sure that it cannot happen that the next stop is the same stop and would be counted double
    """

    current_section = train_object.current_sec_id  # last section that kilometers were done
    current_trip = train_object.current_trip
    train_schedule = copy.deepcopy(train_object.schedule)  # to avoid removing things from the schedule of the train
    nw_sections = train_object.nw_sections
    previous_stop_scheduled = ''
    missed_meters = 0

    # if the current section is a station, and it has not been ended, this means
    # it was not the last stop. The meters have already been counted, as this is done
    # the resources are free. (Assumption that station m's are added once arrival on dwell section)
    # so we remove this arrival stop.
    # If a trip is ended after the completion of the dwell time (and possible after waiting negative dwell time)
    # the section that was just done (station) has already been removed from the schedule.
    # this is checked by making sure that the current section gtfs map of the current section (station) is within
    # the first entry of the station, if so delete, otherwise it has already been deleted
    if nw_sections[current_section]['station_ind']:
        gtfs_maps = nw_sections[current_section]['gtfs_map']
        if train_schedule[0]['stop_id'] in gtfs_maps:
            train_schedule = train_schedule[1:]

    while len(train_schedule) > 0 and train_schedule[0]['trip_id'] == current_trip:
        # find a possible next section
        next_section = nw_sections[current_section]['subsequent_sections'][0]
        # add the kilometers
        missed_meters += nw_sections[next_section]['section_length']
        if nw_sections[next_section]['station_ind']:
            # make sure that it cannot happen that a schedule with multiple entries at a stop are counted twice
            assert previous_stop_scheduled != train_schedule[0]['stop_id']
            # if we hit a station, do check and remove the entry from the schedule
            gtfs_maps = nw_sections[next_section]['gtfs_map']
            assert train_schedule[0]['stop_id'] in gtfs_maps
            previous_stop_scheduled = train_schedule[0]['stop_id']
            train_schedule = train_schedule[1:]
        # iterate next section until all entries of this trip are processed
        current_section = next_section

    return missed_meters

