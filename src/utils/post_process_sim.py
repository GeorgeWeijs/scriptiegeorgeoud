import pandas as pd
import numpy as np
import os


def aggr_section_runtime(input_folder, input_file, nr_sims):
    colsort = ['train_name', 'timestamp']

    df_output = pd.DataFrame()
    for i in range(nr_sims):
        inputfileloc = os.path.join(input_folder, str(i), input_file)
        df_temp = pd.read_csv(inputfileloc, delimiter=',')

        # calculate time difference between dep-arr or arr-dep
        # but only within the same trip
        df_temp.sort_values(colsort, inplace=True)
        df_temp['wait_bc_early'] = df_temp['wait_bc_early'].fillna(0)
        df_temp['wait_resource_time'] = df_temp['wait_resource_time'].fillna(0)
        df_temp['diffTime'] = df_temp['timestamp'] - df_temp['timestamp'].shift(1) - \
                              df_temp['wait_bc_early'] - df_temp['wait_resource_time']
        cond_difflrv = (df_temp['train_name'] != df_temp['train_name'].shift(1))
        df_temp.loc[cond_difflrv, 'diffTime'] = np.NaN

        # allocate section run time and dwell+wait time
        df_temp.loc[df_temp['arrival'] == 'arrival', 'timeSectionRun'] = df_temp['diffTime']
        df_temp.loc[df_temp['arrival'] == 'departure', 'timeDwellWait'] = df_temp['diffTime']
        df_temp.drop('diffTime', axis=1, inplace=True)

        # assign simulation number for id
        df_temp['simNo'] = i

        # append to output
        df_output = df_output.append(df_temp)
        del df_temp, cond_difflrv  # save RAM

    df_output.drop('Unnamed: 0', axis=1, inplace=True)
    return df_output
