import numpy as np

AVG_SPAWN_INTERVAL = 60*7


def get_exp_wait_time_rounded(avg_interval):
    """
    Exponential distribution, rounded
    """
    wait_time = np.random.exponential(avg_interval)
    return round(wait_time)


def get_norm_time_cutoff(mean, stdev, nr_of_std_cutoff=1):
    """
    Calculate time given a normal distribution with mu and std, and cutoff at 'nr_of_std_cutoff' times the std
    """
    section_time = np.random.normal(mean, stdev)
    upper_limit = mean + nr_of_std_cutoff * stdev
    lower_limit = mean - nr_of_std_cutoff * stdev
    section_time_trunc = max(min(section_time, upper_limit), lower_limit)

    return section_time_trunc


def get_gamma_time_orig(mean, stdev):
    """
    Gamma distribution with mean and std as inputted
    These parameters will be converted to scale and rate and then stochastic drawing takes place
    """
    # see https://www.rocscience.com/help/swedge/webhelp/swedge/Gamma_Distribution.htm
    # note that b here, is the scale parameter (theta on wiki), not the beta from wiki which is the rate
    # or inverse scale(which is 1/theta)
    g_shape = (mean / stdev) ** 2
    g_scale = (stdev ** 2) / mean

    section_time = np.random.gamma(g_shape, g_scale)
    return section_time


def get_gamma_time_lowerbound(mean, stdev, lower_bound=0):
    """
    Gamma distribution with mean and std and lower bound as inputted
    These parameters will be converted to scale and rate and then stochastic drawing takes place
    Note that the drawing is redone if it is below the lower_bound
    which effectively means that the tail below minimum is spread over the other part
    """
    # see https://www.rocscience.com/help/swedge/webhelp/swedge/Gamma_Distribution.htm
    # note that b here, is the scale parameter (theta on wiki), not the beta from wiki which is the rate
    # or inverse scale(which is 1/theta)
    g_shape = (mean / stdev) ** 2
    g_scale = (stdev ** 2) / mean

    while True:
        section_time = np.random.gamma(g_shape, g_scale)
        if section_time > lower_bound:
            break
    return section_time


def get_gamma_time_fixed_minimum(mean, stdev, fixed_minimum):
    """
    Gamma distribution with mean and std and fixed minimum as inputted
    These parameters will be converted to scale and rate and then stochastic drawing takes place
    The minimum cannot be exceeded negatively, and will be subtracted first and added later again from the mean
    """
    # see https://www.rocscience.com/help/swedge/webhelp/swedge/Gamma_Distribution.htm
    # note that b here, is the scale parameter (theta on wiki), not the beta from wiki which is the rate
    # or inverse scale(which is 1/theta)
    if (mean ==None) or (stdev ==None):
        mean = 100
        stdev = 10
        fixed_minimum = 30
    mean = mean-fixed_minimum
    g_shape = ((mean / stdev) ** 2)
    g_scale = ((stdev ** 2) / mean)
    section_time = np.random.gamma(g_shape, g_scale)
    section_time += fixed_minimum
    return section_time


def get_lognormal_time(mean, stdev):
    """
    lognormal distribution with mean and std as inputted
    see https://blogs.sas.com/content/iml/2014/06/04/simulate-lognormal-data-with-specified-mean-and-variance.html
    """
    phi = np.sqrt(stdev**2 + mean**2)
    mu = np.log(mean**2/phi)          # mean of log(Y)
    sigma = np.sqrt(np.log(phi**2/mean**2))  # std dev of log(Y)

    section_time = np.random.lognormal(mu, sigma)
    return section_time


if __name__ == '__main__':

    print('test')
