import folium
from folium import plugins
import pandas as pd
import os
from src.settings import RAW_GTFS_DATA, OUTPUT_DATA


def create_basemap(centre, zoomstart=12):

    map_attribution = '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, &copy; <a href="http://cartodb.com/attributions">CartoDB</a>, icon by &copy; <a href="https://www.flaticon.com/authors/simpleicon" title="SimpleIcon">SimpleIcon</a> (licensed by <a href="http://creativecommons.org/licenses/by/3.0/" title="Creative Commons BY 3.0" target="_blank">CC 3.0 BY</a>), powered by &copy <a href="http://lynxx.eu">Lynxx </a>'
    # tiles = 'https://{s}.basemaps.cartocdn.com/light_nolabels/{z}/{x}/{y}.png'

    map = folium.Map(location=centre, zoom_start=zoomstart, tiles=None, attr=map_attribution)

    return map, map_attribution


def add_map_tiles(map, attribution):
    """

    :param map:
    :return:
    """

    # add a bunch of new tile sets
    tiles = {'CartoDB positron': {'name': 'Light layer'},
             'Stamen Terrain': {'name': 'Terrain layer'},
             'Open Street Map': {'name': 'Open Street Map'}
             }
    for tileLayer, tileLayer_data in tiles.items():
        folium.TileLayer(tileLayer, attr=attribution, name=tileLayer_data['name'], min_zoom=12, max_zoom=18).add_to(map)

    return map


def add_map_functionalities(map, fullscreen=True, control=True, drawing=True, ruler=True):
    """

    :param map:
    :param fullscreen:
    :param control:
    :param drawing:
    :param ruler:
    :return:
    """

    if fullscreen:
        # full screen enabled
        plugins.Fullscreen(
            position='topright',
            title='Enter full screen',
            title_cancel='Exit full screen',
            force_separate_button=True).add_to(map)

    if control:
        # layer control enabled
        folium.LayerControl(position='bottomright').add_to(map)

    if drawing:
        # drawing enabled
        plugins.Draw(export=True).add_to(map)

    if ruler:
        # ruler enabled
        plugins.MeasureControl(position='bottomleft').add_to(map)

    return map


def add_shape_to_map(map, gtfs_folder, shape_id):
    """

    :param map:
    :param gtfs_folder:
    :param shape_id:
    :return:
    """

    # load all available shapes
    shapes = pd.read_csv(os.path.join(gtfs_folder, 'shapes.txt'), dtype=str)
    trips = pd.read_csv(os.path.join(gtfs_folder, 'trips.txt'), dtype=str)
    stop_times = pd.read_csv(os.path.join(gtfs_folder, 'stop_times.txt'), dtype=str)
    stops = pd.read_csv(os.path.join(gtfs_folder, 'stops.txt'), dtype=str)

    # filtering
    shapes = shapes[shapes['shape_id'] == shape_id]
    trips = trips[trips['shape_id'].isin(shapes['shape_id'])]
    stop_times = stop_times[stop_times['trip_id'].isin(trips['trip_id'])]
    stops = stops[stops['stop_id'].isin(stop_times['stop_id'])]

    # some changing
    shapes['shape_pt_sequence'] = shapes['shape_pt_sequence'].astype(int)
    shapes = shapes.sort_values(by='shape_pt_sequence')

    # remove tiny errors
    bug_list = [965, 510, 928, 929]

    # route segmenten
    segments = {'Haarlem': {'start': 1, 'end': 194, 'color': '#d53e4f'},
                'Zuidtangent': {'start': 194, 'end': 709, 'color': '#f46d43'},
                'Abdijtunnel': {'start': 709, 'end': 733, 'color': '#fdae61'},
                'Schiphol': {'start': 733, 'end': 870, 'color': '#fee08b'},
                'Buitenvelderttunnel': {'start': 870, 'end': 890, 'color': '#e6f598'},
                'Zuidtangent Schiphol Noord': {'start': 890, 'end': 1040, 'color': '#abdda4'},
                'A9': {'start': 1040, 'end': 1250, 'color': '#66c2a5'},
                'Amstelveen / Bijlmer': {'start': 1250, 'end': 1514, 'color': '#3288bd'},
                }

    for segment_name, segment_data in segments.items():
        # create the polyline sequentially from the locations in the shape file
        locations = []
        for index, row in shapes.iterrows():
            if segment_data['start'] <= row['shape_pt_sequence'] <= segment_data['end']:
                if row['shape_pt_sequence'] not in bug_list:
                    point = (float(row['shape_pt_lat']), float(row['shape_pt_lon']))
                    locations.append(point)

        # add the polyline to the map
        folium.PolyLine(locations=locations, popup=segment_name, color=segment_data['color'], weight=4).add_to(map, name=segment_name)

    # add the stops to the map
    for index, row in stops.iterrows():
        folium.CircleMarker(
            radius=5, color='grey', fill_color='white', fill_opacity=1,
            location=(float(row['stop_lat']), float(row['stop_lon'])),
            popup=row['stop_name']).add_to(map)

    # add the shape_points to the map
    # for index, row in shapes.iterrows():
    #     folium.CircleMarker(
    #         radius=1, color='blue', fill_color='blue', fill_opacity=1,
    #         location=(float(row['shape_pt_lat']), float(row['shape_pt_lon'])),
    #         popup=str(row['shape_pt_sequence'])).add_to(map)

    return map


def add_stops_to_map_name_based(map, gtfs_folder):
    """

    :param map:
    :param gtfs_folder:
    :return:
    """

    # load all available shapes

    stops = pd.read_csv(os.path.join(gtfs_folder, 'stops.txt'), dtype=str)

    # loop through all the stops
    added_stop_names_list = []
    for index, row in stops.iterrows():
        # we do want to add multiple platforms of the same station the map
        if row['stop_name'] not in added_stop_names_list:

            folium.CircleMarker(
                radius=4, color='green', fill_color='white', fill_opacity=1,
                location=(float(row['stop_lat']), float(row['stop_lon'])),
                popup=row['stop_name']).add_to(map)

            added_stop_names_list.append(row['stop_name'])

    return map