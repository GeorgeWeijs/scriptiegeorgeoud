import statistics
import pandas as pd
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
from matplotlib import dates
import os
from src.settings import INPUT_DATA, OUTPUT_DATA

def plot_bus_travelling(sections: dict, trn_acts: dict ,plot_sub_folder: str)-> None:
    """
    :param sections: dictionary containing all the sections
    :param trn_acts: dictionary containing all the logged messages of the simulation
    :param plot_sub_folder: the output folder of the plot
    :return:
    """
    #create dataframe from dictionary trn_acts
    trn_acts = pd.DataFrame(trn_acts)

    # extract current trip from message
    trn_acts.current_trip = trn_acts['message'].str.split('id', expand=True)[1].str.split(',', expand=True)[0]
    trn_acts.current_trip = trn_acts.current_trip.replace(" ", '')
    trn_acts = trn_acts[trn_acts.message.str.contains("WAITING")==False]
    # Transform None section to correct section
    for section, index in zip(trn_acts['current_trip'], trn_acts.index):
        if not section:
            if ("WAITING" not in trn_acts.loc[index, 'message']) and ("TERMINATED" not in trn_acts.loc[index, 'message']):
                section_start = trn_acts.loc[index, 'message'].split("COMMENCE, now on section ")[1].split(", with delay +0")[0]
                trn_acts.loc[index, 'current_trip'] = section_start
    trn_acts['type_message'] = trn_acts['message'].str.split('sequence', expand=True)[0]

    # make figure with NetworkSim Logo
    fig = plt.figure(figsize=(40, 20), dpi=100)
    ax = fig.add_subplot(111)

    from PIL import Image
    logo = Image.open(os.path.join(INPUT_DATA, 'NWS_logo.png'))
    basewidth = 1200
    wpercent = (basewidth / float(logo.size[0]))
    hsize = int((float(logo.size[1]) * float(wpercent)))
    logo = logo.resize((basewidth, hsize), Image.ANTIALIAS)

    # put image in the plot
    logo = np.asarray(logo)
    fig.figimage(logo, xo=fig.bbox.xmax - logo.shape[1] - 10, yo=fig.bbox.ymax - logo.shape[0] - 10, alpha=0.4, zorder=1)

    # create locator and formatter for plots
    hours = dates.MinuteLocator(interval=30)
    h_fmt = dates.DateFormatter('%H:%M')
    plot_type = False

    # Make seperate line for each driving bus
    for i in range(0,22):
        if plot_type == False:
            #if i%2 ==0:
            converted_time = dates.datestr2num(trn_acts.loc[trn_acts['train_name'] == "BUS_" + str(i)].timestamp_hr)
            x_axis = (converted_time)
            trip_nr = trn_acts.loc[trn_acts['train_name'] == "BUS_" + str(i)].current_trip
            trip_nr = pd.to_numeric(trip_nr,errors = 'coerce')
            trip_nr.loc[(trip_nr > 83) & (trip_nr<86)]= 1
            trip_nr.loc[(trip_nr > 85)] = 0
            trip_nr.loc[trip_nr>42] =  84- trip_nr.loc[trip_nr>42]

            y_axis = trip_nr.astype(str)
            ax.plot(y_axis, x_axis, '-')
        else:
            if i < 10:
                if i<7:
                    #only take even busses, these have the same start direction
                    if i%2 ==0:
                        converted_time = dates.datestr2num(trn_acts.loc[trn_acts['train_name'] == "BUS_" + str(i)].timestamp_hr)
                        x_axis = (converted_time)

                        trip_nr = trn_acts.loc[trn_acts['train_name'] == "BUS_" + str(i)].current_trip
                        trip_nr = pd.to_numeric(trip_nr, errors='coerce')
                        trip_nr.loc[(trip_nr > 83) & (trip_nr<86)]= 1
                        trip_nr.loc[(trip_nr > 85)] = 0
                        trip_nr.loc[trip_nr > 42] =  84 - trip_nr.loc[trip_nr > 42]
                        y_axis = trip_nr.astype(str)
                        ax.plot(y_axis, x_axis, '-')
            if i>10:
                if i>16:
                    if i % 2 == 1:
                        converted_time = dates.datestr2num(trn_acts.loc[trn_acts['train_name'] == "BUS_" + str(i)].timestamp_hr)
                        x_axis = (converted_time)
                        trip_nr = trn_acts.loc[trn_acts['train_name'] == "BUS_" + str(i)].current_trip
                        trip_nr = pd.to_numeric(trip_nr, errors='coerce')
                        trip_nr.loc[(trip_nr > 83) & (trip_nr<86)]= 1
                        trip_nr.loc[(trip_nr > 85)] = 0
                        trip_nr.loc[trip_nr > 42] = 84 - trip_nr.loc[trip_nr > 42]
                        y_axis = trip_nr.astype(str)
                        ax.plot(y_axis, x_axis, '-')

    # set formatter and locator for y_axis
    ax.yaxis.set_major_locator(hours)
    plt.grid(axis='x', alpha =0.8)
    fig.autofmt_xdate()
    ax.yaxis.set_major_formatter(h_fmt)

    # only show the time period given
    ax.set_ylim(dates.datestr2num("07:00"), dates.datestr2num("10:00"))

    # create ticks and labels at stations and not at roadways between stations
    ticks = [i for i in range(0,44,2)]
    labels = [sections[str(station)]["section_name"] for station in range(0,44,2)]
    ax.set_xticks(ticks)
    ax.tick_params(axis='both', which='major', labelsize=17)
    ax.set_xticklabels(labels,fontsize=17)
    plt.xticks(rotation=45)
    ax.yaxis.label.set_size(fontsize = 25)
    plot_name = 'plot_time_haltes.png'
    plt.savefig(os.path.join(OUTPUT_DATA, plot_name))


def calculate_bus_performance_kpis(sections: dict, trn_acts: dict) -> (pd.DataFrame, pd.DataFrame):
    # set desired regelmaat interval and factors for kpi's
    interval = 7*60
    wait_assesment_factor = 2* 60
    bunching_factor = 3*60

    sections_df = pd.DataFrame.from_dict(sections, orient="index")
    trn_acts = pd.DataFrame(trn_acts)

    # extract current trips from message
    trn_acts.current_trip = trn_acts['message'].str.split('id', expand=True)[1].str.split(',', expand=True)[0]
    trn_acts.current_trip = trn_acts.current_trip.replace(" ", '')

    # extract type of message from message
    trn_acts['type_message'] = trn_acts['message'].str.split('sequence', expand=True)[0].str.replace(' ','')
    trn_acts['current_trip'] = trn_acts['current_trip'].str.replace(" ", "")

    # add the haltes and sections columns to the trn_acts
    df = pd.merge(trn_acts, sections_df[['id', "halte","section_name","direction"]], left_on='current_trip', right_on='id')
    df_haltes = df[df['halte']]
    df_leaving_haltes = df_haltes.loc[df_haltes['type_message']=="COMPLETED"]
    df_haltes = df_haltes.sort_values('timestamp')

    # create dataframe with the kpi's for all station and direction combination
    kpi_per_station = pd.DataFrame(columns = ['station','direction','wait_assesment',"Excess_waiting_time","Bunching"])
    counter =0
    for direction in df_leaving_haltes.direction.unique():
        for station in df_leaving_haltes.section_name.unique():
            counter += 1
            # continue when halte direction combination does not exist
            if len(df_leaving_haltes.loc[(df_leaving_haltes.section_name == station) & (df_leaving_haltes.direction == direction)]) == 0:
                continue

            kpi_per_station.loc[counter,'station'] = station
            kpi_per_station.loc[counter,'direction'] = direction

            # create column with time between busses at a halte
            df_leaving_haltes.loc[(df_leaving_haltes.section_name == station) &
                                  (df_leaving_haltes.direction == direction),'time_after_last_bus'] =\
                df_leaving_haltes.loc[(df_leaving_haltes.section_name == station) &
                                      (df_leaving_haltes.direction == direction),'timestamp'].diff(periods = 1)

            # derive number of observations where wait assesment factor has been exceeded, excess waiting times, an bunching
            total_exceeded_wait_threshold = sum(df_leaving_haltes.loc[
                                                    (df_leaving_haltes.section_name == station) &
                                                    (df_leaving_haltes.direction == direction), 'time_after_last_bus']
                                                > interval + wait_assesment_factor)
            excess_waiting_time = df_leaving_haltes.loc[(df_leaving_haltes.section_name == station) & (df_leaving_haltes.direction == direction), 'time_after_last_bus']/2
            bunching = sum(df_leaving_haltes.loc[(df_leaving_haltes.section_name == station) & (df_leaving_haltes.direction == direction),'time_after_last_bus']<bunching_factor)/len(df_leaving_haltes.loc[
                    (df_leaving_haltes.section_name == station) & (df_leaving_haltes.direction == direction)])

            kpi_per_station.loc[(kpi_per_station.station == station) & (kpi_per_station.direction == direction) ,"Excess_waiting_time"] = excess_waiting_time.mean()
            kpi_per_station.loc[(kpi_per_station.station == station) & (kpi_per_station.direction == direction), "Bunching"] = bunching
            kpi_per_station.loc[(kpi_per_station.station == station) & (kpi_per_station.direction == direction),"wait_assesment"] \
                = total_exceeded_wait_threshold/len(df_leaving_haltes.loc[(df_leaving_haltes.section_name == station) &
                                                                          (df_leaving_haltes.direction == direction)] )

    # create dataframe with travel time of a trip (Richting Haarlem, or Richting Bijlmer) for each specific bus
    travel_time =pd.DataFrame()
    for bus in df_haltes.train_name.unique():
        route = 0
        for time, halte in zip(df_haltes.loc[df_haltes.train_name==bus,"timestamp"],df_haltes.loc[df_haltes.train_name==bus,"current_trip"]):
            if halte == "2":
                time_left_haarlem = time
            elif halte =="42":
                arrived_bijlmer = True
                time_arrived_bijlmer = time
                travel_time.loc[bus,"to_bijlmer_"+str(route)] = time_arrived_bijlmer-time_left_haarlem
            elif halte =="44":
                time_left_bijlmer = time
            elif halte =="86":
                time_arrived_haarlem = time
                travel_time.loc[bus, "to_haarlem_" + str(route)] = time_arrived_haarlem - time_left_bijlmer
                route+=1

    return kpi_per_station, travel_time

def calculate_performance_kpis(ended_trip_details, stat_acts):
    """
    Function that calculates all the performance KPI's (official and useful)
    for the output of 1 simulation (random seed)
    """

    df_stat_acts = pd.DataFrame(stat_acts)
    df_ended_trip_details = pd.DataFrame(ended_trip_details)

    # filter on uncancelled trips
    df_etd_uncancelled = df_ended_trip_details[df_ended_trip_details['is_cancelled'] == 'No']

    # nr of uncancelled trips
    nr_of_uncancelled_trips = len(df_etd_uncancelled)

    # nr of cancelled trips
    nr_of_cancelled_trips = len(df_ended_trip_details) - len(df_etd_uncancelled)

    # non official kpi's
    # avg_wait_resource_time, only uncancelled
    avg_wait_resource_time_uncancel = statistics.mean(df_etd_uncancelled['trips_wait_resource_time'])

    # avg_behind_schedule_time, only uncancelled
    avg_behind_schedule_time_uncancel = statistics.mean(df_etd_uncancelled['trips_behind_schedule'])

    # avg_total_run_time, only uncancelled
    avg_total_run_time_uncancel = statistics.mean(df_etd_uncancelled['trips_total_run_time'])

    # official kpi's
    # filter train activities in departures for TFLBS and AESH kpi's later
    df_stat_acts_dep = df_stat_acts[df_stat_acts['arrival'] == 'departure']
    # AESH also needs arrivals for termini headways
    df_stat_acts_arr = df_stat_acts[df_stat_acts['arrival'] == 'arrival']

    # Service Availability
    # Total Actual Service Kilometres as a percentage of Planned Service Kilometres
    # note cancelled trips are included for the total planned kilometres
    # filter on peak trips and non peak
    df_etd_peak = df_ended_trip_details[df_ended_trip_details['is_peak'] == 'Yes']
    df_etd_nonpeak = df_ended_trip_details[df_ended_trip_details['is_peak'] == 'No']
    actual_service_km_run = sum(1.5 * df_etd_peak['trip_service_meters']) + sum(df_etd_nonpeak['trip_service_meters'])
    planned_service_km_run = sum(1.5 * df_etd_peak['trip_planned_meters']) + sum(df_etd_nonpeak['trip_planned_meters'])
    perc_service_km_run = actual_service_km_run / planned_service_km_run

    # Timely First and Last Base Service
    # A First of Last Base Service must leave the Originating Stop no earlier and less than 2 minutes later than
    # the planned departure time.
    nr_of_not_timely_base_first_last_service = 0
    # first services from originating stops (between 05:00 and 07:00, departure)
    df_stat_acts_dep_em_westmead = df_stat_acts_dep[df_stat_acts_dep['planned_timestamp'].between(18000, 25200,
                                                                                                  inclusive=True) &
                                                    df_stat_acts_dep['section_id'].isin(['3', '8', '10'])]  # westmead
    if df_stat_acts_dep_em_westmead['behind_schedule'].iloc[0] < 0 or \
            df_stat_acts_dep_em_westmead['behind_schedule'].iloc[0] > 120 or len(df_stat_acts_dep_em_westmead) == 0:
        nr_of_not_timely_base_first_last_service += 1

    df_stat_acts_dep_em_carlingf = df_stat_acts_dep[df_stat_acts_dep['planned_timestamp'].between(18000, 25200,
                                                                                                  inclusive=True) &
                                                    df_stat_acts_dep['section_id'].isin(['1065', '1066'])]  # carlingford
    if df_stat_acts_dep_em_carlingf['behind_schedule'].iloc[0] < 0 or \
            df_stat_acts_dep_em_carlingf['behind_schedule'].iloc[0] > 120 or len(df_stat_acts_dep_em_carlingf) == 0:
        nr_of_not_timely_base_first_last_service += 1
    # last services from originating stops (between 23:00 and 01:00, departure)
    df_stat_acts_dep_ni_westmead = df_stat_acts_dep[df_stat_acts_dep['planned_timestamp'].between(82800, 90000,
                                                                                                  inclusive=True) &
                                                    df_stat_acts_dep['section_id'].isin(['3', '8', '10'])]  # westmead
    if df_stat_acts_dep_ni_westmead['behind_schedule'].iloc[-1] < 0 or \
            df_stat_acts_dep_ni_westmead['behind_schedule'].iloc[0] > 120 or len(df_stat_acts_dep_ni_westmead) == 0:
        nr_of_not_timely_base_first_last_service += 1
    df_stat_acts_dep_ni_carlingf = df_stat_acts_dep[df_stat_acts_dep['planned_timestamp'].between(82800, 90000,
                                                                                                  inclusive=True) &
                                                    df_stat_acts_dep['section_id'].isin(['1065', '1066'])]  # carlingford
    if df_stat_acts_dep_ni_carlingf['behind_schedule'].iloc[-1] < 0 or \
            df_stat_acts_dep_ni_carlingf['behind_schedule'].iloc[0] > 120 or len(df_stat_acts_dep_ni_carlingf) == 0:
        nr_of_not_timely_base_first_last_service += 1

    # Average Excess Service Headway
    # Actual Headway less the Planned Headway (both measured in seconds at each Measuring Stop)
    # timing stops are:
    # carlingford, westmead, camelia, parramatta square

    df_sa_dep_clf_dep = df_stat_acts_dep[df_stat_acts_dep['section_id'].isin(['1065', '1066'])]
    df_sa_dep_clf_arr = df_stat_acts_arr[df_stat_acts_arr['section_id'].isin(['1065', '1066'])]
    df_sa_dep_wmd_dep = df_stat_acts_dep[df_stat_acts_dep['section_id'].isin(['3', '8', '10'])]
    df_sa_dep_wmd_arr = df_stat_acts_arr[df_stat_acts_arr['section_id'].isin(['3', '8', '10'])]
    df_sa_dep_cam_east = df_stat_acts_dep[df_stat_acts_dep['section_id'].isin(['967'])]
    df_sa_dep_cam_west = df_stat_acts_dep[df_stat_acts_dep['section_id'].isin(['1164'])]
    df_sa_dep_par_east = df_stat_acts_dep[df_stat_acts_dep['section_id'].isin(['926'])]
    df_sa_dep_par_west = df_stat_acts_dep[df_stat_acts_dep['section_id'].isin(['1205'])]

    list_of_abs_headways = []
    for df_case_dep in [df_sa_dep_clf_dep, df_sa_dep_clf_arr, df_sa_dep_wmd_dep, df_sa_dep_wmd_arr, df_sa_dep_cam_east,
                        df_sa_dep_cam_west, df_sa_dep_par_east, df_sa_dep_par_west]:

        headway_column = 'planned_departure_headway'
        if df_case_dep is df_sa_dep_clf_arr or df_case_dep is df_sa_dep_wmd_arr:
            headway_column = 'planned_arrival_headway'
        # for each of the filtered dataframes (for different locations/direction if needed), we calculate the
        # excess headway.
        previous_timestamp = df_case_dep['timestamp'].iloc[0]
        for row_id in range(len(df_case_dep)):
            # skip first row as this will not have a headway (missing previous)
            if row_id > 0:
                actual_headway = df_case_dep['timestamp'].iloc[row_id] - previous_timestamp
                absolute_diff = abs(actual_headway - df_case_dep[headway_column].iloc[row_id])
                if df_case_dep['is_peak'].iloc[row_id]:
                    absolute_diff *= 1.5
                list_of_abs_headways.append(absolute_diff)
                # set current value to previous for next row
                previous_timestamp = df_case_dep['timestamp'].iloc[row_id]
    average_excess_headway = sum(list_of_abs_headways)/len(list_of_abs_headways)
    df_leaving_haltes[df_leaving_haltes.section_name == station]['timestamp'].diff(periods=1)

    # Excess Service Journey
    # Actual Service Journey Time less the Planned Service Journey Time as a percentage of Completed Services
    exces_sjt_list = []
    for i in range(len(df_etd_uncancelled)):
        excess_sjt = max(df_etd_uncancelled['trips_total_run_time'].iloc[i] -
                         df_etd_uncancelled['trips_planned_total_run_time'].iloc[i], 0)
        if df_etd_uncancelled['is_peak'].iloc[i] == 'Yes':
            excess_sjt *= 1.5
        exces_sjt_list.append(excess_sjt)
    average_excess_sjt = sum(exces_sjt_list) / len(exces_sjt_list)

    return_dict = dict()
    # kpis
    return_dict['perc_service_km_run'] = perc_service_km_run
    return_dict['nr_not_timely_flbs'] = nr_of_not_timely_base_first_last_service
    return_dict['avg_excess_headway'] = average_excess_headway
    return_dict['avg_excess_sjt'] = average_excess_sjt
    # figures to aggregate averages appropriately later
    return_dict['nr_of_uncancelled_trips'] = nr_of_uncancelled_trips
    return_dict['nr_of_measured_headway'] = len(list_of_abs_headways)
    return_dict['actual_service_km_run'] = actual_service_km_run
    return_dict['planned_service_km_run'] = planned_service_km_run

    # sanity check KPI's
    return_dict['nr_of_cancelled_trips'] = nr_of_cancelled_trips
    return_dict['avg_wait_resource_time_uncancel'] = avg_wait_resource_time_uncancel
    return_dict['avg_behind_schedule_time_uncancel'] = avg_behind_schedule_time_uncancel
    return_dict['avg_total_run_time_uncancel'] = avg_total_run_time_uncancel

    return return_dict


def aggr_performance_kpis(simulator_result_df):
    """
    Function that takes the dataframe result of the simulator (a data frame with KPI's for each simulation (seed))
    and transforms this into aggregated KPI's for the whole case (gtfs schedule, or headway param etc)
    This is used to compare different scenarios and is an estimate of an average day
    """
    # easy avg
    total_avg_nr_not_timely_flbs = sum(simulator_result_df['nr_not_timely_flbs']) / \
                                   len(simulator_result_df['nr_not_timely_flbs'])
    total_avg_nr_of_cancelled_trips = sum(simulator_result_df['nr_of_cancelled_trips']) / \
                                      len(simulator_result_df['nr_of_cancelled_trips'])
    total_avg_nr_of_uncancelled_trips = sum(simulator_result_df['nr_of_uncancelled_trips']) / \
                                        len(simulator_result_df['nr_of_uncancelled_trips'])
    total_avg_nr_of_measured_headway = sum(simulator_result_df['nr_of_measured_headway']) / \
                                       len(simulator_result_df['nr_of_measured_headway'])

    # weighted sum for nr of trips (uncancel)
    total_avg_behind_schedule_uncancel = sum(simulator_result_df['avg_behind_schedule_time_uncancel'] *
                                             simulator_result_df['nr_of_uncancelled_trips']) / \
                                         sum(simulator_result_df['nr_of_uncancelled_trips'])
    total_avg_total_run_time_uncancel = sum(simulator_result_df['avg_total_run_time_uncancel'] *
                                            simulator_result_df['nr_of_uncancelled_trips']) / \
                                        sum(simulator_result_df['nr_of_uncancelled_trips'])
    total_avg_wait_resource_time_uncancel = sum(simulator_result_df['avg_wait_resource_time_uncancel'] *
                                                simulator_result_df['nr_of_uncancelled_trips']) / \
                                            sum(simulator_result_df['nr_of_uncancelled_trips'])
    total_avg_excess_sjt = sum(simulator_result_df['avg_excess_sjt'] *
                               simulator_result_df['nr_of_uncancelled_trips']) / \
                           sum(simulator_result_df['nr_of_uncancelled_trips'])

    # weighted sum for measured headways
    total_avg_excess_headway = sum(simulator_result_df['avg_excess_headway'] *
                                   simulator_result_df['nr_of_measured_headway']) / \
                               sum(simulator_result_df['nr_of_measured_headway'])

    # fraction of easy sums
    total_actual_service_km_run = sum(simulator_result_df['actual_service_km_run'])
    total_planned_service_km_run = sum(simulator_result_df['planned_service_km_run'])
    total_perc_service_km_run = total_actual_service_km_run/total_planned_service_km_run

    aggr_return_dict = dict()
    aggr_return_dict['total_avg_nr_not_timely_flbs'] = total_avg_nr_not_timely_flbs
    aggr_return_dict['total_avg_nr_of_cancelled_trips'] = total_avg_nr_of_cancelled_trips
    aggr_return_dict['total_avg_nr_of_uncancelled_trips'] = total_avg_nr_of_uncancelled_trips
    aggr_return_dict['total_avg_nr_of_measured_headway'] = total_avg_nr_of_measured_headway
    aggr_return_dict['total_actual_service_km_run'] = total_actual_service_km_run
    aggr_return_dict['total_planned_service_km_run'] = total_planned_service_km_run
    aggr_return_dict['total_avg_behind_schedule_uncancel'] = total_avg_behind_schedule_uncancel
    aggr_return_dict['total_avg_total_run_time_uncancel'] = total_avg_total_run_time_uncancel
    aggr_return_dict['total_avg_wait_resource_time_uncancel'] = total_avg_wait_resource_time_uncancel
    aggr_return_dict['total_avg_excess_sjt'] = total_avg_excess_sjt
    aggr_return_dict['total_avg_excess_headway'] = total_avg_excess_headway
    aggr_return_dict['total_perc_service_km_run'] = total_perc_service_km_run

    return aggr_return_dict


def add_paymech_results(df_input):
    """
    Function that transforms the output of a run into financial implications based on the paymech mechanics
    """

    var_avgDaysPerMonth = 30

    ''' 1. Calculation of the Service Availability Deduction '''
    var_ASA_D_cap = 900000
    var_SA_T = 0.995  # 99.5%
    var_LSP_dollar = 30000

    SA_D_cap = var_ASA_D_cap / 12  # annual -> monthly
    # TPSK_perc = df_input['total_actual_service_km_run'] / df_input['total_planned_service_km_run']
    TPSK_perc = df_input['total_perc_service_km_run']
    LSP_perc = np.maximum(0, var_SA_T - TPSK_perc) * 100  # avg => *30 gives the same results

    df_input['SA_D_noIF'] = np.minimum(SA_D_cap, LSP_perc * 100 * var_LSP_dollar)

    ''' 2. Calculation of the Timely First & Last Base Services Departure Deduction '''
    var_ATFLBSD_D_cap = 225000
    var_TFLBSD_dollar = 2000

    TFLBSD_D_cap = var_ATFLBSD_D_cap / 12  # annual -> monthly
    TFLBSD_n = df_input['total_avg_nr_not_timely_flbs'] * var_avgDaysPerMonth  # monthly

    df_input['TFLBSD_D_noIF'] = np.minimum(TFLBSD_D_cap, TFLBSD_n * var_TFLBSD_dollar)

    ''' 3. Calculation of the Average Excess Service Headway Deduction '''
    var_AAESH_D_cap = 562500
    var_AESH_T = 60  # 1 minute
    var_AESH_dollar = 100000

    AESH_D_cap = var_AAESH_D_cap / 12  # annual -> monthly
    AESH_minutes = np.maximum(0, df_input['total_avg_excess_headway'] - var_AESH_T) / 60  # avg => *30 gives the same results

    df_input['AESH_D_noIF'] = np.minimum(AESH_D_cap, AESH_minutes * var_AESH_dollar)

    ''' 4. Calculation of the Excess Service Journey Time Deduction '''
    var_AESJT_D_cap = 562500
    var_ESJT_T = 120  # 2 minutes
    var_ESJT_dollar = 25000

    ESJT_D_cap = var_AESJT_D_cap / 12  # annual -> monthly
    ESJT_minutes = np.maximum(0, df_input['total_avg_excess_sjt'] - var_ESJT_T) / 60  # avg => *30 gives the same results

    df_input['ESJT_D_noIF'] = np.minimum(ESJT_D_cap, ESJT_minutes * var_ESJT_dollar)

    ''' 5. Calculation of the Permanent Performance Incentive Threshold Components Deduction '''
    var_PPITC_perc = 0.20  # 20%

    df_input.loc[df_input['SA_D_noIF'] + df_input['TFLBSD_D_noIF'] + df_input['AESH_D_noIF']
                 + df_input['ESJT_D_noIF'] > 0, 'PPITC_perc'] = var_PPITC_perc
    df_input['PPITC_perc'].fillna(0, inplace=True)

    return df_input


def get_mean_and_std_of_aggregated_results(sim_folders: list()) -> (pd.DataFrame, pd.DataFrame):
    """
    :param sim_folders: list of to be analyzed folders
    :return: average travel time and kpi agregated results
    """

    kpi_busses = []
    all_rules = pd.DataFrame()

    kpi_aggr_sim = pd.DataFrame()
    travel_time_aggr_sim = pd.DataFrame()
    travel_time_rules = pd.DataFrame(columns=["rule", "mean_travel_time_haarlem", "std_travel_time_bijlmer"])

    counter = 0
    print(sim_folders)
    for scenario in sim_folders:
        print(scenario)
        travel_time_rules.loc[counter, 'rule'] = scenario
        path = os.path.join(OUTPUT_DATA, scenario)
        for sim in range(0, 1):
            kpi_busses = pd.read_csv(os.path.join(path, "h240_l300_evariance_off_/" + str(counter) + "/kpi_busses.csv"))
            kpi_busses['simulation'] = sim
            kpi_aggr_sim = pd.concat([kpi_busses, kpi_aggr_sim])

            travel_time = pd.read_csv(
                os.path.join(path, "h240_l300_evariance_off_/" + str(counter) + "/travel_time_busses.csv"))
            travel_time_aggr_sim = pd.concat([travel_time, travel_time_aggr_sim])

        travel_time_aggr_sim = travel_time_aggr_sim.dropna(axis=1)
        travel_times = pd.DataFrame(travel_time_aggr_sim.mean())

        travel_times['route'] = travel_times.index
        kpi_aggr_sim = kpi_aggr_sim.replace({"Richtig Haarlem": "Richting Haarlem"})
        kpi_aggr_sim['rule'] = scenario
        all_rules = pd.concat([all_rules, kpi_aggr_sim])
        aggregated_kpi = \
            pd.DataFrame(kpi_aggr_sim.groupby(['station', 'direction']).mean())[
                ['wait_assesment', 'Excess_waiting_time', 'Bunching']]

        travel_time_rules.loc[travel_time_rules['rule'] == scenario, 'mean_travel_time_haarlem'] = \
            travel_times[travel_times['route'].apply(lambda x: "haarlem" in x)].mean()[0]
        travel_time_rules.loc[travel_time_rules['rule'] == scenario, 'std_travel_time_haarlem'] = \
            travel_times[travel_times['route'].apply(lambda x: "haarlem" in x)].std()[0]
        travel_time_rules.loc[travel_time_rules['rule'] == scenario, 'mean_travel_time_bijlmer'] = \
            travel_times[travel_times['route'].apply(lambda x: "bijlmer" in x)].mean()[0]
        travel_time_rules.loc[travel_time_rules['rule'] == scenario, 'std_travel_time_bijlmer'] = \
            travel_times[travel_times['route'].apply(lambda x: "bijlmer" in x)].std()[0]

        return travel_time_rules, kpi_aggr_sim

    if __name__ == '__main__':
        # demonstration case
        x, y = get_mean_and_std_of_aggregated_results(["indi_sim_1559309990_all_no_schedule"])
        print(x, y)
        #result_df = pd.read_csv('demonstrate_results.csv')
        #add_paymech_results(result_df)